# nfcMMO

This is a hard fork of [mcMMO](https://github.com/mcMMO-Dev/mcMMO) 1.1.14 for use with [New Frontier Craft](https://newfrontiercraft.net/). This project is being used under the terms of the GPLv3 license.

nfcMMO Goals:
- Revive and maintain an NFC-compatible version of mcMMO
- Revive and maintain all the dependencies needed to build and run it.
- Massively clean up the codebase over time
- Potentially introduce new features, however this is secondary to cleanup and compatibility.



We also maintain a fork of the version of Spout compatible with NFC, [nfcSpout](https://gitlab.com/bushtail/nfcSpout)

## Assisting the Project

Want to help? The 2011craft team only updates localization in US English. We appreciate any merge requests for international languages,
as long as they have been tested beforehand. We will still test your localization, but please make sure it is working before sending it in.
