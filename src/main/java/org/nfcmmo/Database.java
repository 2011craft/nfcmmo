// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import java.util.ArrayList;
import java.util.HashMap;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import org.nfcmmo.config.Config;

public class Database
{
    private nfcMMO plugin;
    private String connectionString;
    
    public Database(final nfcMMO instance) {
        this.plugin = instance;
        this.connectionString = "jdbc:mysql://" + Config.MySQLserverName + ":" + Config.MySQLport + "/" + Config.MySQLdbName + "?user=" + Config.MySQLuserName + "&password=" + Config.MySQLdbPass;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            DriverManager.getConnection(this.connectionString);
        }
        catch (ClassNotFoundException e) {
            this.plugin.getServer().getLogger().warning(e.getLocalizedMessage());
        }
        catch (SQLException e2) {
            this.plugin.getServer().getLogger().warning(e2.getLocalizedMessage());
            System.out.println("SQLException: " + e2.getMessage());
            System.out.println("SQLState: " + e2.getSQLState());
            System.out.println("VendorError: " + e2.getErrorCode());
        }
    }
    
    public void createStructure() {
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "huds` (`user_id` int(10) unsigned NOT NULL," + "`hudtype` varchar(50) NOT NULL DEFAULT ''," + "PRIMARY KEY (`user_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "users` (`id` int(10) unsigned NOT NULL AUTO_INCREMENT," + "`user` varchar(40) NOT NULL," + "`lastlogin` int(32) unsigned NOT NULL," + "`party` varchar(100) NOT NULL DEFAULT ''," + "PRIMARY KEY (`id`)," + "UNIQUE KEY `user` (`user`)) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;");
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "cooldowns` (`user_id` int(10) unsigned NOT NULL," + "`taming` int(32) unsigned NOT NULL DEFAULT '0'," + "`mining` int(32) unsigned NOT NULL DEFAULT '0'," + "`woodcutting` int(32) unsigned NOT NULL DEFAULT '0'," + "`repair` int(32) unsigned NOT NULL DEFAULT '0'," + "`unarmed` int(32) unsigned NOT NULL DEFAULT '0'," + "`herbalism` int(32) unsigned NOT NULL DEFAULT '0'," + "`excavation` int(32) unsigned NOT NULL DEFAULT '0'," + "`archery` int(32) unsigned NOT NULL DEFAULT '0'," + "`swords` int(32) unsigned NOT NULL DEFAULT '0'," + "`axes` int(32) unsigned NOT NULL DEFAULT '0'," + "`acrobatics` int(32) unsigned NOT NULL DEFAULT '0'," + "PRIMARY KEY (`user_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "skills` (`user_id` int(10) unsigned NOT NULL," + "`taming` int(10) unsigned NOT NULL DEFAULT '0'," + "`mining` int(10) unsigned NOT NULL DEFAULT '0'," + "`woodcutting` int(10) unsigned NOT NULL DEFAULT '0'," + "`repair` int(10) unsigned NOT NULL DEFAULT '0'," + "`unarmed` int(10) unsigned NOT NULL DEFAULT '0'," + "`herbalism` int(10) unsigned NOT NULL DEFAULT '0'," + "`excavation` int(10) unsigned NOT NULL DEFAULT '0'," + "`archery` int(10) unsigned NOT NULL DEFAULT '0'," + "`swords` int(10) unsigned NOT NULL DEFAULT '0'," + "`axes` int(10) unsigned NOT NULL DEFAULT '0'," + "`acrobatics` int(10) unsigned NOT NULL DEFAULT '0'," + "PRIMARY KEY (`user_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "experience` (`user_id` int(10) unsigned NOT NULL," + "`taming` int(10) unsigned NOT NULL DEFAULT '0'," + "`mining` int(10) unsigned NOT NULL DEFAULT '0'," + "`woodcutting` int(10) unsigned NOT NULL DEFAULT '0'," + "`repair` int(10) unsigned NOT NULL DEFAULT '0'," + "`unarmed` int(10) unsigned NOT NULL DEFAULT '0'," + "`herbalism` int(10) unsigned NOT NULL DEFAULT '0'," + "`excavation` int(10) unsigned NOT NULL DEFAULT '0'," + "`archery` int(10) unsigned NOT NULL DEFAULT '0'," + "`swords` int(10) unsigned NOT NULL DEFAULT '0'," + "`axes` int(10) unsigned NOT NULL DEFAULT '0'," + "`acrobatics` int(10) unsigned NOT NULL DEFAULT '0'," + "PRIMARY KEY (`user_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
        this.Write("CREATE TABLE IF NOT EXISTS `" + Config.MySQLtablePrefix + "spawn` (`user_id` int(10) NOT NULL," + "`x` int(64) NOT NULL DEFAULT '0'," + "`y` int(64) NOT NULL DEFAULT '0'," + "`z` int(64) NOT NULL DEFAULT '0'," + "`world` varchar(50) NOT NULL DEFAULT ''," + "PRIMARY KEY (`user_id`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;");
    }
    
    public boolean Write(final String sql) {
        try {
            final Connection conn = DriverManager.getConnection(this.connectionString);
            final PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.executeUpdate();
            conn.close();
            return true;
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
            return false;
        }
    }
    
    public Integer GetInt(final String sql) {
        ResultSet rs = null;
        Integer result = 0;
        try {
            final Connection conn = DriverManager.getConnection(this.connectionString);
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt = conn.prepareStatement(sql);
            if (stmt.executeQuery() != null) {
                stmt.executeQuery();
                rs = stmt.getResultSet();
                if (rs.next()) {
                    result = rs.getInt(1);
                }
                else {
                    result = 0;
                }
            }
            conn.close();
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return result;
    }
    
    public HashMap<Integer, ArrayList<String>> Read(final String sql) {
        ResultSet rs = null;
        final HashMap<Integer, ArrayList<String>> Rows = new HashMap<Integer, ArrayList<String>>();
        try {
            final Connection conn = DriverManager.getConnection(this.connectionString);
            final PreparedStatement stmt = conn.prepareStatement(sql);
            if (stmt.executeQuery() != null) {
                stmt.executeQuery();
                rs = stmt.getResultSet();
                while (rs.next()) {
                    final ArrayList<String> Col = new ArrayList<String>();
                    for (int i = 1; i <= rs.getMetaData().getColumnCount(); ++i) {
                        Col.add(rs.getString(i));
                    }
                    Rows.put(rs.getRow(), Col);
                }
            }
            conn.close();
        }
        catch (SQLException ex) {
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        return Rows;
    }
}
