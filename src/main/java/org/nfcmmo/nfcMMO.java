// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import java.io.FileNotFoundException;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.BufferedWriter;
import java.io.FileWriter;
import org.nfcmmo.command.Commands;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import java.util.logging.Level;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import org.nfcmmo.skills.Skills;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.getspout.spoutapi.player.FileManager;
import org.bukkit.plugin.PluginManager;
import org.blockface.bukkitstats.CallHome;
import org.bukkit.Bukkit;
import java.util.Collection;
import org.getspout.spoutapi.SpoutManager;
import org.nfcmmo.spout.SpoutStuff;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import org.bukkit.event.Event;
import org.nfcmmo.party.Party;
import org.nfcmmo.runnables.mcTimer;
import org.nfcmmo.config.Config;
import org.nfcmmo.config.Misc;
import com.nijikokun.bukkit.Permissions.Permissions;
import org.nfcmmo.listeners.mcEntityListener;
import org.nfcmmo.listeners.mcBlockListener;
import org.nfcmmo.listeners.mcPlayerListener;
import java.util.logging.Logger;
import java.io.File;
import org.bukkit.plugin.java.JavaPlugin;

public class nfcMMO extends JavaPlugin
{
    public static String maindirectory;
    File file;
    static File versionFile;
    public static final Logger log;
    private final mcPlayerListener playerListener;
    private final mcBlockListener blockListener;
    private final mcEntityListener entityListener;
    public static mcPermissions permissionHandler;
    private Permissions permissions;
    private Runnable nfcMMO_Timer;
    public static Database database;
    public Misc misc;
    Config config;
    public static File nfcmmo;
    
    static {
        nfcMMO.maindirectory = "plugins" + File.separator + "nfcMMO";
        nfcMMO.versionFile = new File(String.valueOf(nfcMMO.maindirectory) + File.separator + "VERSION");
        log = Logger.getLogger("Minecraft");
        nfcMMO.permissionHandler = new mcPermissions();
        nfcMMO.database = null;
    }
    
    public nfcMMO() {
        this.file = new File(String.valueOf(nfcMMO.maindirectory) + File.separator + "config.yml");
        this.playerListener = new mcPlayerListener(this);
        this.blockListener = new mcBlockListener(this);
        this.entityListener = new mcEntityListener(this);
        this.nfcMMO_Timer = new mcTimer(this);
        this.misc = new Misc(this);
        this.config = new Config();
    }
    
    public void onEnable() {
        nfcMMO.nfcmmo = this.getFile();
        new File(nfcMMO.maindirectory).mkdir();
        if (!nfcMMO.versionFile.exists()) {
            this.updateVersion();
        }
        else {
            final String vnum = this.readVersion();
            if (vnum.equalsIgnoreCase("1.0.48")) {
                this.updateFrom(1);
            }
            else if (!vnum.equalsIgnoreCase(this.getDescription().getVersion())) {
                this.updateFrom(-1);
            }
        }
        mcPermissions.initialize(this.getServer());
        this.config.configCheck();
        Party.getInstance().loadParties();
        new Party(this);
        if (!Config.useMySQL) {
            Users.getInstance().loadUsers();
        }
        final PluginManager pm = this.getServer().getPluginManager();
        if (pm.getPlugin("Spout") != null) {
            Config.spoutEnabled = true;
        }
        else {
            Config.spoutEnabled = false;
        }
        pm.registerEvent(Event.Type.PLAYER_QUIT, (Listener)this.playerListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_JOIN, (Listener)this.playerListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_LOGIN, (Listener)this.playerListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_CHAT, (Listener)this.playerListener, Event.Priority.Lowest, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_INTERACT, (Listener)this.playerListener, Event.Priority.Monitor, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_RESPAWN, (Listener)this.playerListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.PLAYER_PICKUP_ITEM, (Listener)this.playerListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.BLOCK_DAMAGE, (Listener)this.blockListener, Event.Priority.Highest, (Plugin)this);
        pm.registerEvent(Event.Type.BLOCK_BREAK, (Listener)this.blockListener, Event.Priority.Monitor, (Plugin)this);
        pm.registerEvent(Event.Type.BLOCK_FROMTO, (Listener)this.blockListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.BLOCK_PLACE, (Listener)this.blockListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.ENTITY_DEATH, (Listener)this.entityListener, Event.Priority.Normal, (Plugin)this);
        pm.registerEvent(Event.Type.ENTITY_DAMAGE, (Listener)this.entityListener, Event.Priority.Monitor, (Plugin)this);
        pm.registerEvent(Event.Type.CREATURE_SPAWN, (Listener)this.entityListener, Event.Priority.Normal, (Plugin)this);
        if (Config.spoutEnabled) {
            SpoutStuff.setupSpoutConfigs();
            SpoutStuff.registerCustomEvent();
            SpoutStuff.extractFiles();
            final FileManager FM = SpoutManager.getFileManager();
            FM.addToPreLoginCache((Plugin)this, (Collection)SpoutStuff.getFiles());
        }
        final PluginDescriptionFile pdfFile = this.getDescription();
        mcPermissions.initialize(this.getServer());
        if (Config.useMySQL) {
            (nfcMMO.database = new Database(this)).createStructure();
        }
        else {
            Leaderboard.makeLeaderboards();
        }
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = this.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player player = onlinePlayers[i];
            Users.addUser(player);
        }
        System.out.println(String.valueOf(pdfFile.getName()) + " version " + pdfFile.getVersion() + " is enabled!");
        Bukkit.getServer().getScheduler().scheduleAsyncRepeatingTask((Plugin)this, this.nfcMMO_Timer, 0L, 20L);
        CallHome.load((Plugin)this);
    }
    
    public PlayerProfile getPlayerProfile(final Player player) {
        return Users.getProfile(player);
    }
    
    public void checkXp(final Player player, final SkillType skillType) {
        if (skillType == SkillType.ALL) {
            Skills.XpCheckAll(player);
        }
        else {
            Skills.XpCheckSkill(skillType, player);
        }
    }
    
    public boolean inSameParty(final Player playera, final Player playerb) {
        return Users.getProfile(playera).inParty() && Users.getProfile(playerb).inParty() && Users.getProfile(playera).getParty().equals(Users.getProfile(playerb).getParty());
    }
    
    public ArrayList<String> getParties() {
        final String location = "plugins/nfcMMO/nfcmmo.users";
        final ArrayList<String> parties = new ArrayList<String>();
        try {
            final FileReader file = new FileReader(location);
            final BufferedReader in = new BufferedReader(file);
            String line = "";
            while ((line = in.readLine()) != null) {
                final String[] character = line.split(":");
                String theparty = null;
                if (character.length > 3) {
                    theparty = character[3];
                }
                if (!parties.contains(theparty)) {
                    parties.add(theparty);
                }
            }
            in.close();
        }
        catch (Exception e) {
            nfcMMO.log.log(Level.SEVERE, "Exception while reading " + location + " (Are you sure you formatted it correctly?)", e);
        }
        return parties;
    }
    
    public static String getPartyName(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        return PP.getParty();
    }
    
    public static boolean inParty(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        return PP.inParty();
    }
    
    public Permissions getPermissions() {
        return this.permissions;
    }
    
    public void onDisable() {
        Bukkit.getServer().getScheduler().cancelTasks((Plugin)this);
        System.out.println("nfcMMO was disabled.");
    }
    
    public boolean onCommand(final CommandSender sender, final Command command, final String label, final String[] args) {
        return Commands.processCommands(sender, command, label, args);
    }
    
    public void updateFrom(final int age) {
        if (age == -1) {
            this.updateVersion();
            return;
        }
        this.updateVersion();
    }
    
    public void updateVersion() {
        try {
            nfcMMO.versionFile.createNewFile();
            final BufferedWriter vout = new BufferedWriter(new FileWriter(nfcMMO.versionFile));
            vout.write(this.getDescription().getVersion());
            vout.close();
        }
        catch (IOException ex) {
            ex.printStackTrace();
        }
        catch (SecurityException ex2) {
            ex2.printStackTrace();
        }
    }
    
    public String readVersion() {
        final byte[] buffer = new byte[(int)nfcMMO.versionFile.length()];
        BufferedInputStream f = null;
        try {
            f = new BufferedInputStream(new FileInputStream(nfcMMO.versionFile));
            f.read(buffer);
        }
        catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        catch (IOException ex2) {
            ex2.printStackTrace();
        }
        finally {
            if (f != null) {
                try {
                    f.close();
                }
                catch (IOException ex3) {}
            }
        }
        if (f != null) {
            try {
                f.close();
            }
            catch (IOException ex4) {}
        }
        return new String(buffer);
    }
}
