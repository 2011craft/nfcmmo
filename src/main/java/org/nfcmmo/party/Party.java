// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.party;

import java.io.ObjectOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.EOFException;
import java.io.FileNotFoundException;
import java.io.ObjectInputStream;
import java.io.FileInputStream;

import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.spout.util.ArrayListString;
import java.util.ArrayList;
import org.nfcmmo.locale.mcLocale;
import org.bukkit.Bukkit;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;
import java.io.File;
import org.nfcmmo.nfcMMO;

import java.util.HashMap;

public class Party
{
    public static String partyPlayersFile;
    public static String partyLocksFile;
    public static String partyPasswordsFile;
    HashMap<String, HashMap<String, Boolean>> partyPlayers;
    HashMap<String, Boolean> partyLocks;
    HashMap<String, String> partyPasswords;
    private static nfcMMO plugin;
    private static volatile Party instance;
    
    static {
        Party.partyPlayersFile = String.valueOf(nfcMMO.maindirectory) + File.separator + "FlatFileStuff" + File.separator + "partyPlayers";
        Party.partyLocksFile = String.valueOf(nfcMMO.maindirectory) + File.separator + "FlatFileStuff" + File.separator + "partyLocks";
        Party.partyPasswordsFile = String.valueOf(nfcMMO.maindirectory) + File.separator + "FlatFileStuff" + File.separator + "partyPasswords";
    }
    
    public Party(final nfcMMO instance) {
        this.partyPlayers = new HashMap<String, HashMap<String, Boolean>>();
        this.partyLocks = new HashMap<String, Boolean>();
        this.partyPasswords = new HashMap<String, String>();
        new File(String.valueOf(nfcMMO.maindirectory) + File.separator + "FlatFileStuff").mkdir();
        Party.plugin = instance;
    }
    
    public static Party getInstance() {
        if (Party.instance == null) {
            Party.instance = new Party(Party.plugin);
        }
        return Party.instance;
    }
    
    public boolean inSameParty(final Player playera, final Player playerb) {
        return Users.getProfile(playera).inParty() && Users.getProfile(playerb).inParty() && Users.getProfile(playera).getParty().equals(Users.getProfile(playerb).getParty());
    }
    
    public int partyCount(final Player player, final Player[] players) {
        int x = 0;
        for (final Player hurrdurr : players) {
            if (player != null && hurrdurr != null && Users.getProfile(player).getParty().equals(Users.getProfile(hurrdurr).getParty())) {
                ++x;
            }
        }
        return x;
    }
    
    public void informPartyMembers(final Player player) {
        this.informPartyMembers(player, Bukkit.getServer().getOnlinePlayers());
    }
    
    public void informPartyMembers(final Player player, final Player[] players) {
        int x = 0;
        for (final Player p : players) {
            if (player != null && p != null && this.inSameParty(player, p) && !p.getName().equals(player.getName())) {
                p.sendMessage(mcLocale.getString("Party.InformedOnJoin", new Object[] { player.getName() }));
                ++x;
            }
        }
    }
    
    public ArrayList<Player> getPartyMembers(final Player player) {
        final ArrayList<Player> players = new ArrayList<Player>();
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player p = onlinePlayers[i];
            if (p.isOnline() && player != null && p != null && this.inSameParty(player, p) && !p.getName().equals(player.getName())) {
                players.add(p);
            }
        }
        return players;
    }
    
    public ArrayListString getPartyMembersByName(final Player player) {
        final ArrayListString players = new ArrayListString();
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player p = onlinePlayers[i];
            if (p.isOnline() && this.inSameParty(player, p)) {
                players.add(p.getName());
            }
        }
        return players;
    }
    
    public void informPartyMembersOwnerChange(final String newOwner) {
        final Player newOwnerPlayer = Party.plugin.getServer().getPlayer(newOwner);
        this.informPartyMembersOwnerChange(newOwnerPlayer, Bukkit.getServer().getOnlinePlayers());
    }
    
    public void informPartyMembersOwnerChange(final Player newOwner, final Player[] players) {
        int x = 0;
        for (final Player p : players) {
            if (newOwner != null && p != null && this.inSameParty(newOwner, p)) {
                p.sendMessage(String.valueOf(newOwner.getName()) + " is the new party owner.");
                ++x;
            }
        }
    }
    
    public void informPartyMembersQuit(final Player player) {
        this.informPartyMembersQuit(player, Bukkit.getServer().getOnlinePlayers());
    }
    
    public void informPartyMembersQuit(final Player player, final Player[] players) {
        int x = 0;
        for (final Player p : players) {
            if (player != null && p != null && this.inSameParty(player, p) && !p.getName().equals(player.getName())) {
                p.sendMessage(mcLocale.getString("Party.InformedOnQuit", new Object[] { player.getName() }));
                ++x;
            }
        }
    }
    
    public void removeFromParty(final Player player, final PlayerProfile PP) {
        if (!this.isParty(PP.getParty()) || !this.isInParty(player, PP)) {
            this.addToParty(player, PP, PP.getParty(), false);
        }
        this.informPartyMembersQuit(player);
        final String party = PP.getParty();
        if (this.isPartyLeader(player, party) && this.isPartyLocked(party)) {
            this.unlockParty(party);
        }
        this.partyPlayers.get(party).remove(player.getName());
        if (this.isPartyEmpty(party)) {
            this.deleteParty(party);
        }
        PP.removeParty();
        this.savePartyPlayers();
    }
    
    public void addToParty(final Player player, final PlayerProfile PP, String newParty, final Boolean invite) {
        newParty = newParty.replace(":", ".");
        this.addToParty(player, PP, newParty, invite, null);
    }
    
    public void addToParty(final Player player, final PlayerProfile PP, String newParty, final Boolean invite, final String password) {
        newParty = newParty.replace(":", ".");
        if (!invite) {
            if (this.isPartyLocked(newParty)) {
                if (!this.isPartyPasswordProtected(newParty)) {
                    player.sendMessage("Party is locked.");
                    return;
                }
                if (password == null) {
                    player.sendMessage("This party requires a password. Use " + Config.party + " <party> <password> to join it.");
                    return;
                }
                if (!password.equalsIgnoreCase(this.getPartyPassword(newParty))) {
                    player.sendMessage("Party password incorrect.");
                    return;
                }
            }
        }
        else {
            PP.acceptInvite();
        }
        if (!this.isParty(newParty)) {
            putNestedEntry(this.partyPlayers, newParty, player.getName(), true);
            this.partyLocks.put(newParty, false);
            this.partyPasswords.put(newParty, null);
            this.saveParties();
        }
        else {
            putNestedEntry(this.partyPlayers, newParty, player.getName(), false);
            this.savePartyPlayers();
        }
        PP.setParty(newParty);
        this.informPartyMembers(player);
        if (!invite) {
            player.sendMessage(mcLocale.getString("mcPlayerListener.JoinedParty", new Object[] { newParty }));
        }
        else {
            player.sendMessage(mcLocale.getString("mcPlayerListener.InviteAccepted", new Object[] { PP.getParty() }));
        }
    }
    
    private static <U, V, W> W putNestedEntry(final HashMap<U, HashMap<V, W>> nest, final U nestKey, final V nestedKey, final W nestedValue) {
        HashMap<V, W> nested = nest.get(nestKey);
        if (nested == null) {
            nested = new HashMap<V, W>();
            nest.put(nestKey, nested);
        }
        return nested.put(nestedKey, nestedValue);
    }
    
    public void dump(final Player player) {
        player.sendMessage(this.partyPlayers.toString());
        player.sendMessage(this.partyLocks.toString());
        player.sendMessage(this.partyPasswords.toString());
        for (final String nestkey : this.partyPlayers.keySet()) {
            player.sendMessage(nestkey);
            for (final String nestedkey : this.partyPlayers.get(nestkey).keySet()) {
                player.sendMessage("." + nestedkey);
                if (this.partyPlayers.get(nestkey).get(nestedkey)) {
                    player.sendMessage("..True");
                }
                else {
                    player.sendMessage("..False");
                }
            }
        }
    }
    
    public void lockParty(final String partyName) {
        this.partyLocks.put(partyName, true);
        this.savePartyLocks();
    }
    
    public void unlockParty(final String partyName) {
        this.partyLocks.put(partyName, false);
        this.savePartyLocks();
    }
    
    public void deleteParty(final String partyName) {
        this.partyPlayers.remove(partyName);
        this.partyLocks.remove(partyName);
        this.partyPasswords.remove(partyName);
        this.saveParties();
    }
    
    public void setPartyPassword(final String partyName, String password) {
        if (password.equalsIgnoreCase("\"\"")) {
            password = null;
        }
        this.partyPasswords.put(partyName, password);
        this.savePartyPasswords();
    }
    
    public void setPartyLeader(final String partyName, final String playerName) {
        for (final String playerKey : this.partyPlayers.get(partyName).keySet()) {
            if (playerKey.equalsIgnoreCase(playerName)) {
                this.partyPlayers.get(partyName).put(playerName, true);
                this.informPartyMembersOwnerChange(playerName);
                Party.plugin.getServer().getPlayer(playerName).sendMessage("You are now the party owner.");
            }
            else {
                if (!this.partyPlayers.get(partyName).get(playerKey)) {
                    continue;
                }
                Party.plugin.getServer().getPlayer(playerKey).sendMessage("You are no longer party owner.");
                this.partyPlayers.get(partyName).put(playerKey, false);
            }
        }
    }
    
    public String getPartyPassword(final String partyName) {
        return this.partyPasswords.get(partyName);
    }
    
    public boolean canInvite(final Player player, final PlayerProfile PP) {
        return !this.isPartyLocked(PP.getParty()) || this.isPartyLeader(player, PP.getParty());
    }
    
    public boolean isParty(final String partyName) {
        return this.partyPlayers.containsKey(partyName);
    }
    
    public boolean isPartyEmpty(final String partyName) {
        return this.partyPlayers.get(partyName).isEmpty();
    }
    
    public boolean isPartyLeader(final Player player, final String partyName) {
        return this.partyPlayers.get(partyName) != null && this.partyPlayers.get(partyName).get(player.getName()) != null && this.partyPlayers.get(partyName).get(player.getName());
    }
    
    public boolean isPartyLocked(final String partyName) {
        return this.partyLocks.get(partyName) != null && this.partyLocks.get(partyName);
    }
    
    public boolean isPartyPasswordProtected(final String partyName) {
        return this.partyPasswords.get(partyName) != null;
    }
    
    public boolean isInParty(final Player player, final PlayerProfile PP) {
        return this.partyPlayers.get(PP.getParty()).containsKey(player.getName());
    }
    
    public void loadParties() {
        if (new File(Party.partyPlayersFile).exists()) {
            try {
                final ObjectInputStream obj = new ObjectInputStream(new FileInputStream(Party.partyPlayersFile));
                this.partyPlayers = (HashMap<String, HashMap<String, Boolean>>)obj.readObject();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (EOFException e4) {
                nfcMMO.log.info("partyPlayersFile empty.");
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            catch (ClassNotFoundException e3) {
                e3.printStackTrace();
            }
        }
        if (new File(Party.partyLocksFile).exists()) {
            try {
                final ObjectInputStream obj = new ObjectInputStream(new FileInputStream(Party.partyLocksFile));
                this.partyLocks = (HashMap<String, Boolean>)obj.readObject();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (EOFException e4) {
                nfcMMO.log.info("partyLocksFile empty.");
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            catch (ClassNotFoundException e3) {
                e3.printStackTrace();
            }
        }
        if (new File(Party.partyPasswordsFile).exists()) {
            try {
                final ObjectInputStream obj = new ObjectInputStream(new FileInputStream(Party.partyPasswordsFile));
                this.partyPasswords = (HashMap<String, String>)obj.readObject();
            }
            catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            catch (EOFException e4) {
                nfcMMO.log.info("partyPasswordsFile empty.");
            }
            catch (IOException e2) {
                e2.printStackTrace();
            }
            catch (ClassNotFoundException e3) {
                e3.printStackTrace();
            }
        }
    }
    
    public void saveParties() {
        this.savePartyPlayers();
        this.savePartyLocks();
        this.savePartyPasswords();
    }
    
    public void savePartyPlayers() {
        try {
            new File(Party.partyPlayersFile).createNewFile();
            final ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(Party.partyPlayersFile));
            obj.writeObject(this.partyPlayers);
            obj.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public void savePartyLocks() {
        try {
            new File(Party.partyLocksFile).createNewFile();
            final ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(Party.partyLocksFile));
            obj.writeObject(this.partyLocks);
            obj.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public void savePartyPasswords() {
        try {
            new File(Party.partyPasswordsFile).createNewFile();
            final ObjectOutputStream obj = new ObjectOutputStream(new FileOutputStream(Party.partyPasswordsFile));
            obj.writeObject(this.partyPasswords);
            obj.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
