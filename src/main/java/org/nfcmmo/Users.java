// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import java.io.FileWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileInputStream;
import java.util.Properties;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.entity.Player;
import java.util.HashMap;
import java.util.logging.Logger;

public class Users
{
    private static volatile Users instance;
    protected static final Logger log;
    String location;
    String directory;
    String directoryb;
    public static HashMap<Player, PlayerProfile> players;
    private Properties properties;
    
    static {
        log = Logger.getLogger("Minecraft");
        Users.players = new HashMap<Player, PlayerProfile>();
    }
    
    public Users() {
        this.location = "plugins/nfcMMO/FlatFileStuff/nfcmmo.users";
        this.directory = "plugins/nfcMMO/FlatFileStuff/";
        this.directoryb = "plugins/nfcMMO/FlatFileStuff/Leaderboards/";
        this.properties = new Properties();
    }
    
    public void load() throws IOException {
        this.properties.load(new FileInputStream(this.location));
    }
    
    public void save() {
        try {
            this.properties.store(new FileOutputStream(this.location), null);
        }
        catch (IOException ex) {}
    }
    
    public void loadUsers() {
        new File(this.directory).mkdir();
        new File(this.directoryb).mkdir();
        final File theDir = new File(this.location);
        if (!theDir.exists()) {
            try {
                final FileWriter writer = new FileWriter(theDir);
                writer.close();
            }
            catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    
    public static void addUser(final Player player) {
        Users.players.put(player, new PlayerProfile(player));
    }
    
    public static void clearUsers() {
        Users.players.clear();
    }
    
    public static HashMap<Player, PlayerProfile> getProfiles() {
        return Users.players;
    }
    
    public static void removeUser(final Player player) {
        final PlayerProfile PP = getProfile(player);
        if (PP != null) {
            PP.save();
            if (Users.players.containsKey(player)) {
                Users.players.remove(player);
            }
        }
    }
    
    public static PlayerProfile getProfile(final Player player) {
        if (Users.players.get(player) != null) {
            return Users.players.get(player);
        }
        Users.players.put(player, new PlayerProfile(player));
        return Users.players.get(player);
    }
    
    public static Users getInstance() {
        if (Users.instance == null) {
            Users.instance = new Users();
        }
        return Users.instance;
    }
}
