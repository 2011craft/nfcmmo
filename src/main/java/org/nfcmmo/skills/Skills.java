// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.nfcmmo.nfcMMO;
import org.bukkit.entity.Entity;
import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.Leaderboard;
import org.nfcmmo.datatypes.PlayerStat;
import org.nfcmmo.datatypes.SkillType;
import org.bukkit.ChatColor;
import org.nfcmmo.m;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.Users;
import org.bukkit.inventory.ItemStack;
import org.bukkit.entity.Player;

import java.util.logging.Logger;

public class Skills
{
    protected static final Logger log;
    
    static {
        log = Logger.getLogger("Minecraft");
    }
    
    public void updateSQLfromFile(final Player player) {
    }
    
    public static boolean cooldownOver(final Player player, final long oldTime, final int cooldown) {
        final long currentTime = System.currentTimeMillis();
        return currentTime - oldTime >= cooldown * 1000;
    }
    
    public boolean hasArrows(final Player player) {
        ItemStack[] contents;
        for (int length = (contents = player.getInventory().getContents()).length, i = 0; i < length; ++i) {
            final ItemStack x = contents[i];
            if (x.getTypeId() == 262) {
                return true;
            }
        }
        return false;
    }
    
    public void addArrows(final Player player) {
        ItemStack[] contents;
        for (int length = (contents = player.getInventory().getContents()).length, i = 0; i < length; ++i) {
            final ItemStack x = contents[i];
            if (x.getTypeId() == 262) {
                x.setAmount(x.getAmount() + 1);
                return;
            }
        }
    }
    
    public static int calculateTimeLeft(final Player player, final long deactivatedTimeStamp, final int cooldown) {
        return (int)((deactivatedTimeStamp + cooldown * 1000 - System.currentTimeMillis()) / 1000L);
    }
    
    public static void watchCooldowns(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (!PP.getGreenTerraInformed() && System.currentTimeMillis() - PP.getGreenTerraDeactivatedTimeStamp() * 1000L >= Config.greenTerraCooldown * 1000) {
            PP.setGreenTerraInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourGreenTerra"));
        }
        if (!PP.getTreeFellerInformed() && System.currentTimeMillis() - PP.getTreeFellerDeactivatedTimeStamp() * 1000L >= Config.greenTerraCooldown * 1000) {
            PP.setTreeFellerInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourTreeFeller"));
        }
        if (!PP.getSuperBreakerInformed() && System.currentTimeMillis() - PP.getSuperBreakerDeactivatedTimeStamp() * 1000L >= Config.superBreakerCooldown * 1000) {
            PP.setSuperBreakerInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourSuperBreaker"));
        }
        if (!PP.getSerratedStrikesInformed() && System.currentTimeMillis() - PP.getSerratedStrikesDeactivatedTimeStamp() * 1000L >= Config.serratedStrikeCooldown * 1000) {
            PP.setSerratedStrikesInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourSerratedStrikes"));
        }
        if (!PP.getBerserkInformed() && System.currentTimeMillis() - PP.getBerserkDeactivatedTimeStamp() * 1000L >= Config.berserkCooldown * 1000) {
            PP.setBerserkInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourBerserk"));
        }
        if (!PP.getSkullSplitterInformed() && System.currentTimeMillis() - PP.getSkullSplitterDeactivatedTimeStamp() * 1000L >= Config.skullSplitterCooldown * 1000) {
            PP.setSkullSplitterInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourSkullSplitter"));
        }
        if (!PP.getGigaDrillBreakerInformed() && System.currentTimeMillis() - PP.getGigaDrillBreakerDeactivatedTimeStamp() * 1000L >= Config.gigaDrillBreakerCooldown * 1000) {
            PP.setGigaDrillBreakerInformed(true);
            player.sendMessage(mcLocale.getString("Skills.YourGigaDrillBreaker"));
        }
    }
    
    public static void hoeReadinessCheck(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (mcPermissions.getInstance().herbalismAbility(player) && m.isHoe(player.getItemInHand()) && !PP.getHoePreparationMode()) {
            if (!PP.getGreenTerraMode() && !cooldownOver(player, PP.getGreenTerraDeactivatedTimeStamp() * 1000L, Config.greenTerraCooldown)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + calculateTimeLeft(player, PP.getGreenTerraDeactivatedTimeStamp() * 1000L, Config.greenTerraCooldown) + "s)");
                return;
            }
            player.sendMessage(mcLocale.getString("Skills.ReadyHoe"));
            PP.setHoePreparationATS(System.currentTimeMillis());
            PP.setHoePreparationMode(true);
        }
    }
    
    public static void monitorSkills(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (PP != null) {
            if (PP.getHoePreparationMode() && System.currentTimeMillis() - PP.getHoePreparationATS() * 1000L >= 4000L) {
                PP.setHoePreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerHoe"));
            }
            if (PP.getAxePreparationMode() && System.currentTimeMillis() - PP.getAxePreparationATS() * 1000L >= 4000L) {
                PP.setAxePreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerAxe"));
            }
            if (PP.getPickaxePreparationMode() && System.currentTimeMillis() - PP.getPickaxePreparationATS() * 1000L >= 4000L) {
                PP.setPickaxePreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerPickAxe"));
            }
            if (PP.getSwordsPreparationMode() && System.currentTimeMillis() - PP.getSwordsPreparationATS() * 1000L >= 4000L) {
                PP.setSwordsPreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerSword"));
            }
            if (PP.getFistsPreparationMode() && System.currentTimeMillis() - PP.getFistsPreparationATS() * 1000L >= 4000L) {
                PP.setFistsPreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerFists"));
            }
            if (PP.getShovelPreparationMode() && System.currentTimeMillis() - PP.getShovelPreparationATS() * 1000L >= 4000L) {
                PP.setShovelPreparationMode(false);
                player.sendMessage(mcLocale.getString("Skills.LowerShovel"));
            }
            if (mcPermissions.getInstance().herbalismAbility(player) && PP.getGreenTerraMode() && PP.getGreenTerraDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setGreenTerraMode(false);
                PP.setGreenTerraInformed(false);
                player.sendMessage(mcLocale.getString("Skills.GreenTerraOff"));
            }
            if (mcPermissions.getInstance().axesAbility(player) && PP.getSkullSplitterMode() && PP.getSkullSplitterDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setSkullSplitterMode(false);
                PP.setSkullSplitterInformed(false);
                player.sendMessage(mcLocale.getString("Skills.SkullSplitterOff"));
            }
            if (mcPermissions.getInstance().woodCuttingAbility(player) && PP.getTreeFellerMode() && PP.getTreeFellerDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setTreeFellerMode(false);
                PP.setTreeFellerInformed(false);
                player.sendMessage(mcLocale.getString("Skills.TreeFellerOff"));
            }
            if (mcPermissions.getInstance().miningAbility(player) && PP.getSuperBreakerMode() && PP.getSuperBreakerDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setSuperBreakerMode(false);
                PP.setSuperBreakerInformed(false);
                player.sendMessage(mcLocale.getString("Skills.SuperBreakerOff"));
            }
            if (mcPermissions.getInstance().excavationAbility(player) && PP.getGigaDrillBreakerMode() && PP.getGigaDrillBreakerDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setGigaDrillBreakerMode(false);
                PP.setGigaDrillBreakerInformed(false);
                player.sendMessage(mcLocale.getString("Skills.GigaDrillBreakerOff"));
            }
            if (mcPermissions.getInstance().swordsAbility(player) && PP.getSerratedStrikesMode() && PP.getSerratedStrikesDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setSerratedStrikesMode(false);
                PP.setSerratedStrikesInformed(false);
                player.sendMessage(mcLocale.getString("Skills.SerratedStrikesOff"));
            }
            if (mcPermissions.getInstance().unarmedAbility(player) && PP.getBerserkMode() && PP.getBerserkDeactivatedTimeStamp() * 1000L <= System.currentTimeMillis()) {
                PP.setBerserkMode(false);
                PP.setBerserkInformed(false);
                player.sendMessage(mcLocale.getString("Skills.BerserkOff"));
            }
        }
    }
    
    public static void abilityActivationCheck(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (PP != null) {
            if (!PP.getAbilityUse()) {
                return;
            }
            if (mcPermissions.getInstance().miningAbility(player) && m.isMiningPick(player.getItemInHand()) && !PP.getPickaxePreparationMode()) {
                if (!PP.getSuperBreakerMode() && !cooldownOver(player, PP.getSuperBreakerDeactivatedTimeStamp() * 1000L, Config.superBreakerCooldown)) {
                    player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + calculateTimeLeft(player, PP.getSuperBreakerDeactivatedTimeStamp() * 1000L, Config.superBreakerCooldown) + "s)");
                    return;
                }
                player.sendMessage(mcLocale.getString("Skills.ReadyPickAxe"));
                PP.setPickaxePreparationATS(System.currentTimeMillis());
                PP.setPickaxePreparationMode(true);
            }
            if (mcPermissions.getInstance().excavationAbility(player) && m.isShovel(player.getItemInHand()) && !PP.getShovelPreparationMode()) {
                if (!PP.getGigaDrillBreakerMode() && !cooldownOver(player, PP.getGigaDrillBreakerDeactivatedTimeStamp() * 1000L, Config.gigaDrillBreakerCooldown)) {
                    player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + calculateTimeLeft(player, PP.getGigaDrillBreakerDeactivatedTimeStamp() * 1000L, Config.gigaDrillBreakerCooldown) + "s)");
                    return;
                }
                player.sendMessage(mcLocale.getString("Skills.ReadyShovel"));
                PP.setShovelPreparationATS(System.currentTimeMillis());
                PP.setShovelPreparationMode(true);
            }
            if (mcPermissions.getInstance().swordsAbility(player) && m.isSwords(player.getItemInHand()) && !PP.getSwordsPreparationMode()) {
                if (!PP.getSerratedStrikesMode() && !cooldownOver(player, PP.getSerratedStrikesDeactivatedTimeStamp() * 1000L, Config.serratedStrikeCooldown)) {
                    player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + calculateTimeLeft(player, PP.getSerratedStrikesDeactivatedTimeStamp() * 1000L, Config.serratedStrikeCooldown) + "s)");
                    return;
                }
                player.sendMessage(mcLocale.getString("Skills.ReadySword"));
                PP.setSwordsPreparationATS(System.currentTimeMillis());
                PP.setSwordsPreparationMode(true);
            }
            if (mcPermissions.getInstance().unarmedAbility(player) && player.getItemInHand().getTypeId() == 0 && !PP.getFistsPreparationMode()) {
                if (!PP.getBerserkMode() && !cooldownOver(player, PP.getBerserkDeactivatedTimeStamp() * 1000L, Config.berserkCooldown)) {
                    player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + calculateTimeLeft(player, PP.getBerserkDeactivatedTimeStamp() * 1000L, Config.berserkCooldown) + "s)");
                    return;
                }
                player.sendMessage(mcLocale.getString("Skills.ReadyFists"));
                PP.setFistsPreparationATS(System.currentTimeMillis());
                PP.setFistsPreparationMode(true);
            }
            if ((mcPermissions.getInstance().axesAbility(player) || mcPermissions.getInstance().woodCuttingAbility(player)) && !PP.getAxePreparationMode() && m.isAxes(player.getItemInHand())) {
                player.sendMessage(mcLocale.getString("Skills.ReadyAxe"));
                PP.setAxePreparationATS(System.currentTimeMillis());
                PP.setAxePreparationMode(true);
            }
        }
    }
    
    public static void ProcessLeaderboardUpdate(final SkillType skillType, final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        final PlayerStat ps = new PlayerStat();
        if (skillType != SkillType.ALL) {
            ps.statVal = PP.getSkillLevel(skillType);
        }
        else {
            ps.statVal = m.getPowerLevel(player);
        }
        ps.name = player.getName();
        Leaderboard.updateLeaderboard(ps, skillType);
    }
    
    public static void XpCheckSkill(final SkillType skillType, final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (PP.getSkillXpLevel(skillType) >= PP.getXpToLevel(skillType)) {
            int skillups = 0;
            while (PP.getSkillXpLevel(skillType) >= PP.getXpToLevel(skillType)) {
                ++skillups;
                PP.removeXP(skillType, PP.getXpToLevel(skillType));
                PP.skillUp(skillType, 1);
            }
            if (!Config.useMySQL) {
                ProcessLeaderboardUpdate(skillType, player);
                ProcessLeaderboardUpdate(SkillType.ALL, player);
            }
            final String capitalized = m.getCapitalized(skillType.toString());
            if (Config.spoutEnabled && player instanceof SpoutPlayer) {
                final SpoutPlayer sPlayer = SpoutManager.getPlayer(player);
                if (sPlayer.isSpoutCraftEnabled()) {
                    SpoutStuff.levelUpNotification(skillType, sPlayer);
                }
                else {
                    player.sendMessage(mcLocale.getString("Skills." + capitalized + "Up", new Object[] { String.valueOf(skillups), PP.getSkillLevel(skillType) }));
                }
            }
            else {
                player.sendMessage(mcLocale.getString("Skills." + capitalized + "Up", new Object[] { String.valueOf(skillups), PP.getSkillLevel(skillType) }));
            }
        }
        if (Config.xpbar && Config.spoutEnabled) {
            final SpoutPlayer sPlayer2 = SpoutManager.getPlayer(player);
            if (sPlayer2.isSpoutCraftEnabled()) {
                SpoutStuff.updateXpBar((Player)sPlayer2);
            }
        }
    }
    
    public static void XpCheckAll(final Player player) {
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType x = values[i];
            if (x != SkillType.ALL) {
                XpCheckSkill(x, player);
            }
        }
    }
    
    public static SkillType getSkillType(final String skillName) {
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType x = values[i];
            if (x.toString().equals(skillName.toUpperCase())) {
                return x;
            }
        }
        return null;
    }
    
    public static boolean isSkill(String skillname) {
        skillname = skillname.toUpperCase();
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType x = values[i];
            if (x.toString().equals(skillname)) {
                return true;
            }
        }
        return false;
    }
    
    public static void arrowRetrievalCheck(final Entity entity, final nfcMMO plugin) {
        if (plugin.misc.arrowTracker.containsKey(entity)) {
            for (Integer x = 0; x < plugin.misc.arrowTracker.get(entity); ++x) {
                m.mcDropItem(entity.getLocation(), 262);
            }
        }
        plugin.misc.arrowTracker.remove(entity);
    }
    
    public static String getSkillStats(final String skillname, final Integer level, final Integer XP, final Integer XPToLevel) {
        final ChatColor parColor = ChatColor.DARK_AQUA;
        final ChatColor xpColor = ChatColor.GRAY;
        final ChatColor LvlColor = ChatColor.GREEN;
        final ChatColor skillColor = ChatColor.YELLOW;
        return skillColor + skillname + LvlColor + level + parColor + " XP" + "(" + xpColor + XP + parColor + "/" + xpColor + XPToLevel + parColor + ")";
    }
    
    public static boolean hasCombatSkills(final Player player) {
        return mcPermissions.getInstance().axes(player) || mcPermissions.getInstance().archery(player) || mcPermissions.getInstance().swords(player) || mcPermissions.getInstance().taming(player) || mcPermissions.getInstance().unarmed(player);
    }
    
    public static boolean hasGatheringSkills(final Player player) {
        return mcPermissions.getInstance().excavation(player) || mcPermissions.getInstance().herbalism(player) || mcPermissions.getInstance().mining(player) || mcPermissions.getInstance().woodcutting(player);
    }
    
    public static boolean hasMiscSkills(final Player player) {
        return mcPermissions.getInstance().acrobatics(player) || mcPermissions.getInstance().repair(player);
    }
}
