// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.ChatColor;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.m;
import org.nfcmmo.spout.SpoutStuff;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.Users;
import org.bukkit.block.Block;
import org.bukkit.inventory.ItemStack;
import org.bukkit.entity.Player;
import org.nfcmmo.config.Config;

public class Repair
{
    private static int rGold;
    private static String nGold;
    private static int rStone;
    private static String nStone;
    private static int rWood;
    private static String nWood;
    private static int rDiamond;
    private static String nDiamond;
    private static int rIron;
    private static String nIron;
    
    static {
        Repair.rGold = Config.rGold;
        Repair.nGold = Config.nGold;
        Repair.rStone = Config.rStone;
        Repair.nStone = Config.nStone;
        Repair.rWood = Config.rWood;
        Repair.nWood = Config.nWood;
        Repair.rDiamond = Config.rDiamond;
        Repair.nDiamond = Config.nDiamond;
        Repair.rIron = Config.rIron;
        Repair.nIron = Config.nIron;
    }
    
    public static void repairCheck(final Player player, final ItemStack is, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        final short durabilityBefore = player.getItemInHand().getDurability();
        short durabilityAfter = 0;
        short dif = 0;
        if (block != null && mcPermissions.getInstance().repair(player)) {
            if (player.getItemInHand().getDurability() > 0 && player.getItemInHand().getAmount() < 2) {
                if (isArmor(is)) {
                    if (isDiamondArmor(is) && hasItem(player, Repair.rDiamond) && PP.getSkillLevel(SkillType.REPAIR) >= Config.repairdiamondlevel) {
                        removeItem(player, Repair.rDiamond);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        dif *= 6;
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else if (isIronArmor(is) && hasItem(player, Repair.rIron)) {
                        removeItem(player, Repair.rIron);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        dif *= 2;
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else if (isGoldArmor(is) && hasItem(player, Repair.rGold)) {
                        removeItem(player, Repair.rGold);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        dif *= 4;
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else {
                        needMoreVespeneGas(is, player);
                    }
                }
                if (isTools(is)) {
                    if (isStoneTools(is) && hasItem(player, Repair.rStone)) {
                        removeItem(player, Repair.rStone);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        if (m.isShovel(is)) {
                            dif /= 3;
                        }
                        if (m.isSwords(is)) {
                            dif /= 2;
                        }
                        if (m.isHoe(is)) {
                            dif /= 2;
                        }
                        dif /= 2;
                        PP.addXP(SkillType.REPAIR, dif * 10);
                    }
                    else if (isWoodTools(is) && hasItem(player, Repair.rWood)) {
                        removeItem(player, Repair.rWood);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        if (m.isShovel(is)) {
                            dif /= 3;
                        }
                        if (m.isSwords(is)) {
                            dif /= 2;
                        }
                        if (m.isHoe(is)) {
                            dif /= 2;
                        }
                        dif /= 2;
                        PP.addXP(SkillType.REPAIR, dif * 10);
                    }
                    else if (isIronTools(is) && hasItem(player, Repair.rIron)) {
                        removeItem(player, Repair.rIron);
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        if (m.isShovel(is)) {
                            dif /= 3;
                        }
                        if (m.isSwords(is)) {
                            dif /= 2;
                        }
                        if (m.isHoe(is)) {
                            dif /= 2;
                        }
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else if (isDiamondTools(is) && hasItem(player, Repair.rDiamond) && PP.getSkillLevel(SkillType.REPAIR) >= Config.repairdiamondlevel) {
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        removeItem(player, Repair.rDiamond);
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        if (m.isShovel(is)) {
                            dif /= 3;
                        }
                        if (m.isSwords(is)) {
                            dif /= 2;
                        }
                        if (m.isHoe(is)) {
                            dif /= 2;
                        }
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else if (isGoldTools(is) && hasItem(player, Repair.rGold)) {
                        player.getItemInHand().setDurability(getRepairAmount(is, player));
                        removeItem(player, Repair.rGold);
                        durabilityAfter = player.getItemInHand().getDurability();
                        dif = (short)(durabilityBefore - durabilityAfter);
                        dif *= (short)7.6;
                        if (m.isShovel(is)) {
                            dif /= 3;
                        }
                        if (m.isSwords(is)) {
                            dif /= 2;
                        }
                        if (m.isHoe(is)) {
                            dif /= 2;
                        }
                        PP.addXP(SkillType.REPAIR, dif * 10);
                        if (Config.spoutEnabled) {
                            SpoutStuff.playRepairNoise(player);
                        }
                    }
                    else {
                        needMoreVespeneGas(is, player);
                    }
                }
            }
            else {
                player.sendMessage(mcLocale.getString("Skills.FullDurability"));
            }
            player.updateInventory();
            Skills.XpCheckSkill(SkillType.REPAIR, player);
        }
    }
    
    public static boolean isArmor(final ItemStack is) {
        return is.getTypeId() == 306 || is.getTypeId() == 307 || is.getTypeId() == 308 || is.getTypeId() == 309 || is.getTypeId() == 310 || is.getTypeId() == 311 || is.getTypeId() == 312 || is.getTypeId() == 313 || is.getTypeId() == 314 || is.getTypeId() == 315 || is.getTypeId() == 316 || is.getTypeId() == 317;
    }
    
    public static boolean isGoldArmor(final ItemStack is) {
        return is.getTypeId() == 314 || is.getTypeId() == 315 || is.getTypeId() == 316 || is.getTypeId() == 317;
    }
    
    public static boolean isIronArmor(final ItemStack is) {
        return is.getTypeId() == 306 || is.getTypeId() == 307 || is.getTypeId() == 308 || is.getTypeId() == 309;
    }
    
    public static boolean isDiamondArmor(final ItemStack is) {
        return is.getTypeId() == 310 || is.getTypeId() == 311 || is.getTypeId() == 312 || is.getTypeId() == 313;
    }
    
    public static boolean isTools(final ItemStack is) {
        return is.getTypeId() == 359 || is.getTypeId() == 256 || is.getTypeId() == 257 || is.getTypeId() == 258 || is.getTypeId() == 267 || is.getTypeId() == 292 || is.getTypeId() == 276 || is.getTypeId() == 277 || is.getTypeId() == 278 || is.getTypeId() == 279 || is.getTypeId() == 293 || is.getTypeId() == 283 || is.getTypeId() == 285 || is.getTypeId() == 286 || is.getTypeId() == 284 || is.getTypeId() == 268 || is.getTypeId() == 269 || is.getTypeId() == 270 || is.getTypeId() == 271 || is.getTypeId() == 290 || is.getTypeId() == 272 || is.getTypeId() == 273 || is.getTypeId() == 274 || is.getTypeId() == 275 || is.getTypeId() == 291;
    }
    
    public static boolean isStoneTools(final ItemStack is) {
        return is.getTypeId() == 272 || is.getTypeId() == 273 || is.getTypeId() == 274 || is.getTypeId() == 275 || is.getTypeId() == 291;
    }
    
    public static boolean isWoodTools(final ItemStack is) {
        return is.getTypeId() == 268 || is.getTypeId() == 269 || is.getTypeId() == 270 || is.getTypeId() == 271 || is.getTypeId() == 290;
    }
    
    public static boolean isGoldTools(final ItemStack is) {
        return is.getTypeId() == 283 || is.getTypeId() == 285 || is.getTypeId() == 286 || is.getTypeId() == 284 || is.getTypeId() == 294;
    }
    
    public static boolean isIronTools(final ItemStack is) {
        return is.getTypeId() == 359 || is.getTypeId() == 256 || is.getTypeId() == 257 || is.getTypeId() == 258 || is.getTypeId() == 267 || is.getTypeId() == 292;
    }
    
    public static boolean isDiamondTools(final ItemStack is) {
        return is.getTypeId() == 276 || is.getTypeId() == 277 || is.getTypeId() == 278 || is.getTypeId() == 279 || is.getTypeId() == 293;
    }
    
    public static void removeItem(final Player player, final int typeid) {
        final ItemStack[] inventory = player.getInventory().getContents();
        ItemStack[] array;
        for (int length = (array = inventory).length, i = 0; i < length; ++i) {
            final ItemStack x = array[i];
            if (x != null && x.getTypeId() == typeid) {
                if (x.getAmount() == 1) {
                    x.setTypeId(0);
                    x.setAmount(0);
                    player.getInventory().setContents(inventory);
                }
                else {
                    x.setAmount(x.getAmount() - 1);
                    player.getInventory().setContents(inventory);
                }
                return;
            }
        }
    }
    
    public static boolean hasItem(final Player player, final int typeid) {
        final ItemStack[] inventory = player.getInventory().getContents();
        ItemStack[] array;
        for (int length = (array = inventory).length, i = 0; i < length; ++i) {
            final ItemStack x = array[i];
            if (x != null && x.getTypeId() == typeid) {
                return true;
            }
        }
        return false;
    }
    
    public static short repairCalculate(final Player player, short durability, short ramt) {
        final PlayerProfile PP = Users.getProfile(player);
        float bonus = (float)(PP.getSkillLevel(SkillType.REPAIR) / 500);
        bonus *= ramt;
        ramt = (ramt += (short)bonus);
        if (checkPlayerProcRepair(player)) {
            ramt *= 2;
        }
        durability -= ramt;
        if (durability < 0) {
            durability = 0;
        }
        return durability;
    }
    
    public static short getRepairAmount(final ItemStack is, final Player player) {
        final short durability = is.getDurability();
        short ramt = 0;
        switch (is.getTypeId()) {
            case 359: {
                ramt = 119;
                break;
            }
            case 268: {
                ramt = 30;
                break;
            }
            case 269: {
                ramt = 60;
                break;
            }
            case 270: {
                ramt = 20;
                break;
            }
            case 271: {
                ramt = 20;
                break;
            }
            case 290: {
                ramt = 30;
                break;
            }
            case 272: {
                ramt = 66;
                break;
            }
            case 273: {
                ramt = 132;
                break;
            }
            case 274: {
                ramt = 44;
                break;
            }
            case 275: {
                ramt = 44;
                break;
            }
            case 291: {
                ramt = 66;
                break;
            }
            case 284: {
                ramt = 33;
                break;
            }
            case 256: {
                ramt = 251;
                break;
            }
            case 277: {
                ramt = 1562;
                break;
            }
            case 257: {
                ramt = 84;
                break;
            }
            case 258: {
                ramt = 84;
                break;
            }
            case 267: {
                ramt = 126;
                break;
            }
            case 292: {
                ramt = 126;
                break;
            }
            case 276: {
                ramt = 781;
                break;
            }
            case 278: {
                ramt = 521;
                break;
            }
            case 279: {
                ramt = 521;
                break;
            }
            case 293: {
                ramt = 781;
                break;
            }
            case 283: {
                ramt = 17;
                break;
            }
            case 285: {
                ramt = 11;
                break;
            }
            case 286: {
                ramt = 11;
                break;
            }
            case 294: {
                ramt = 17;
                break;
            }
            case 306: {
                ramt = 27;
                break;
            }
            case 310: {
                ramt = 55;
                break;
            }
            case 307: {
                ramt = 24;
                break;
            }
            case 311: {
                ramt = 48;
                break;
            }
            case 308: {
                ramt = 27;
                break;
            }
            case 312: {
                ramt = 53;
                break;
            }
            case 309: {
                ramt = 40;
                break;
            }
            case 313: {
                ramt = 80;
                break;
            }
            case 314: {
                ramt = 13;
                break;
            }
            case 315: {
                ramt = 12;
                break;
            }
            case 316: {
                ramt = 14;
                break;
            }
            case 317: {
                ramt = 20;
                break;
            }
        }
        return repairCalculate(player, durability, ramt);
    }
    
    public static void needMoreVespeneGas(final ItemStack is, final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if ((isDiamondTools(is) || isDiamondArmor(is)) && PP.getSkillLevel(SkillType.REPAIR) < Config.repairdiamondlevel) {
            player.sendMessage(mcLocale.getString("Skills.AdeptDiamond"));
        }
        else if ((isDiamondTools(is) && !hasItem(player, Repair.rDiamond)) || (isIronTools(is) && !hasItem(player, Repair.rIron)) || (isGoldTools(is) && !hasItem(player, Repair.rGold))) {
            if (isDiamondTools(is) && !hasItem(player, Repair.rDiamond)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.BLUE + Repair.nDiamond);
            }
            if (isIronTools(is) && !hasItem(player, Repair.rIron)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.GRAY + Repair.nIron);
            }
            if (isGoldTools(is) && !hasItem(player, Repair.rGold)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.GOLD + Repair.nGold);
            }
            if (isWoodTools(is) && !hasItem(player, Repair.rWood)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.DARK_GREEN + Repair.nWood);
            }
            if (isStoneTools(is) && !hasItem(player, Repair.rStone)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.GRAY + Repair.nStone);
            }
        }
        else if (isDiamondArmor(is) && !hasItem(player, Repair.rDiamond)) {
            player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.BLUE + Repair.nDiamond);
        }
        else if (isIronArmor(is) && !hasItem(player, Repair.rIron)) {
            player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.GRAY + Repair.nIron);
        }
        else if (isGoldArmor(is) && !hasItem(player, Repair.rGold)) {
            player.sendMessage(String.valueOf(mcLocale.getString("Skills.NeedMore")) + " " + ChatColor.GOLD + Repair.nGold);
        }
        else if (is.getAmount() > 1) {
            player.sendMessage(mcLocale.getString("Skills.StackedItems"));
        }
    }
    
    public static boolean checkPlayerProcRepair(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.REPAIR)) {
            player.sendMessage(mcLocale.getString("Skills.FeltEasy"));
            return true;
        }
        return false;
    }
}
