// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.AnimalTamer;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Wolf;

public class Taming
{
    public static boolean ownerOnline(final Wolf theWolf, final Plugin pluginx) {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = pluginx.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player x = onlinePlayers[i];
            if (x instanceof AnimalTamer) {
                final AnimalTamer tamer = (AnimalTamer)x;
                if (theWolf.getOwner() == tamer) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public static Player getOwner(final Entity wolf, final Plugin pluginx) {
        if (wolf instanceof Wolf) {
            final Wolf theWolf = (Wolf)wolf;
            Player[] onlinePlayers;
            for (int length = (onlinePlayers = pluginx.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
                final Player x = onlinePlayers[i];
                if (x instanceof AnimalTamer) {
                    final AnimalTamer tamer = (AnimalTamer)x;
                    if (theWolf.getOwner() == tamer) {
                        return x;
                    }
                }
            }
        }
        return null;
    }
    
    public static String getOwnerName(final Wolf theWolf) {
        final Player owner = (Player)theWolf.getOwner();
        if (owner != null) {
            return owner.getName();
        }
        return "Offline Master";
    }
}
