// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.ChatColor;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.Users;
import org.nfcmmo.mcPermissions;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.entity.Player;

public class Acrobatics
{
    public static void acrobaticsCheck(final Player player, final EntityDamageEvent event) {
        if (player != null && mcPermissions.getInstance().acrobatics(player)) {
            final PlayerProfile PP = Users.getProfile(player);
            int acrovar = PP.getSkillLevel(SkillType.ACROBATICS);
            if (player.isSneaking()) {
                acrovar *= 2;
            }
            if (Math.random() * 1000.0 <= acrovar && !event.isCancelled()) {
                int threshold = 7;
                if (player.isSneaking()) {
                    threshold = 14;
                }
                int newDamage = event.getDamage() - threshold;
                if (newDamage < 0) {
                    newDamage = 0;
                }
                if (player.getHealth() - newDamage >= 1) {
                    if (!event.isCancelled()) {
                        PP.addXP(SkillType.ACROBATICS, event.getDamage() * 8 * 10);
                    }
                    Skills.XpCheckSkill(SkillType.ACROBATICS, player);
                    event.setDamage(newDamage);
                    if (event.getDamage() <= 0) {
                        event.setCancelled(true);
                    }
                    if (player.isSneaking()) {
                        player.sendMessage(ChatColor.GREEN + "**GRACEFUL ROLL**");
                    }
                    else {
                        player.sendMessage("**ROLL**");
                    }
                }
            }
            else if (!event.isCancelled() && player.getHealth() - event.getDamage() >= 1) {
                PP.addXP(SkillType.ACROBATICS, event.getDamage() * 12 * 10);
                Skills.XpCheckSkill(SkillType.ACROBATICS, player);
            }
        }
    }
    
    public static void dodgeChecks(final EntityDamageByEntityEvent event) {
        final Player defender = (Player)event.getEntity();
        final PlayerProfile PPd = Users.getProfile(defender);
        if (mcPermissions.getInstance().acrobatics(defender)) {
            if (PPd.getSkillLevel(SkillType.ACROBATICS) <= 800) {
                if (Math.random() * 4000.0 <= PPd.getSkillLevel(SkillType.ACROBATICS)) {
                    defender.sendMessage(ChatColor.GREEN + "**DODGE**");
                    if (System.currentTimeMillis() >= 5000L + PPd.getRespawnATS() && defender.getHealth() >= 1) {
                        PPd.addXP(SkillType.ACROBATICS, event.getDamage() * 12 * 1);
                        Skills.XpCheckSkill(SkillType.ACROBATICS, defender);
                    }
                    event.setDamage(event.getDamage() / 2);
                    if (event.getDamage() <= 0) {
                        event.setDamage(1);
                    }
                }
            }
            else if (Math.random() * 4000.0 <= 800.0) {
                defender.sendMessage(ChatColor.GREEN + "**DODGE**");
                if (System.currentTimeMillis() >= 5000L + PPd.getRespawnATS() && defender.getHealth() >= 1) {
                    PPd.addXP(SkillType.ACROBATICS, event.getDamage() * 12 * 10);
                    Skills.XpCheckSkill(SkillType.ACROBATICS, defender);
                }
                event.setDamage(event.getDamage() / 2);
                if (event.getDamage() <= 0) {
                    event.setDamage(1);
                }
            }
        }
    }
}
