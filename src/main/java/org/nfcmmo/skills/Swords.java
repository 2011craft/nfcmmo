// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.nfcmmo.Combat;
import org.bukkit.entity.Arrow;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.ChatColor;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.party.Party;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Wolf;
import org.nfcmmo.nfcMMO;
import org.bukkit.entity.LivingEntity;
import java.util.Iterator;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.m;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;

public class Swords
{
    public static void serratedStrikesActivationCheck(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isSwords(player.getItemInHand())) {
            if (PP.getSwordsPreparationMode()) {
                PP.setSwordsPreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.SWORDS); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getSerratedStrikesMode() && PP.getSerratedStrikesDeactivatedTimeStamp() < System.currentTimeMillis()) {
                player.sendMessage(mcLocale.getString("Skills.SerratedStrikesOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.SerratedStrikesPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setSerratedStrikesActivatedTimeStamp(System.currentTimeMillis());
                PP.setSerratedStrikesDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setSerratedStrikesMode(true);
            }
        }
    }
    
    public static void bleedCheck(final Player attacker, final LivingEntity x, final nfcMMO pluginx) {
        final PlayerProfile PPa = Users.getProfile(attacker);
        if (x instanceof Wolf) {
            final Wolf wolf = (Wolf)x;
            if (Taming.getOwner((Entity)wolf, (Plugin)pluginx) != null) {
                if (Taming.getOwner((Entity)wolf, (Plugin)pluginx) == attacker) {
                    return;
                }
                if (Party.getInstance().inSameParty(attacker, Taming.getOwner((Entity)wolf, (Plugin)pluginx))) {
                    return;
                }
            }
        }
        if (mcPermissions.getInstance().swords(attacker) && m.isSwords(attacker.getItemInHand())) {
            if (PPa.getSkillLevel(SkillType.SWORDS) >= 750) {
                if (Math.random() * 1000.0 >= 750.0) {
                    if (!(x instanceof Player)) {
                        pluginx.misc.addToBleedQue(x);
                    }
                    if (x instanceof Player) {
                        final Player target = (Player)x;
                        Users.getProfile(target).addBleedTicks(3);
                    }
                    attacker.sendMessage(ChatColor.GREEN + "**ENEMY BLEEDING**");
                }
            }
            else if (Math.random() * 1000.0 <= PPa.getSkillLevel(SkillType.SWORDS)) {
                if (!(x instanceof Player)) {
                    pluginx.misc.addToBleedQue(x);
                }
                if (x instanceof Player) {
                    final Player target = (Player)x;
                    Users.getProfile(target).addBleedTicks(2);
                }
                attacker.sendMessage(ChatColor.GREEN + "**ENEMY BLEEDING**");
            }
        }
    }
    
    public static void applySerratedStrikes(final Player attacker, final EntityDamageByEntityEvent event, final nfcMMO pluginx) {
        int targets = 0;
        if (event.getEntity() instanceof LivingEntity) {
            final LivingEntity x = (LivingEntity)event.getEntity();
            targets = m.getTier(attacker);
            for (final Entity derp : x.getWorld().getEntities()) {
                if (m.getDistance(x.getLocation(), derp.getLocation()) < 5.0) {
                    if (derp instanceof Wolf) {
                        final Wolf hurrDurr = (Wolf)derp;
                        if (Taming.getOwner((Entity)hurrDurr, (Plugin)pluginx) == attacker) {
                            continue;
                        }
                        if (Party.getInstance().inSameParty(attacker, Taming.getOwner((Entity)hurrDurr, (Plugin)pluginx))) {
                            continue;
                        }
                    }
                    if (!(derp instanceof LivingEntity) || targets < 1) {
                        continue;
                    }
                    if (derp instanceof Player) {
                        final Player target = (Player)derp;
                        if (target.getName().equals(attacker.getName())) {
                            continue;
                        }
                        if (Users.getProfile(target).getGodMode()) {
                            continue;
                        }
                        if (Party.getInstance().inSameParty(attacker, target)) {
                            continue;
                        }
                        if (targets < 1 || !derp.getWorld().getPVP()) {
                            continue;
                        }
                        target.damage(event.getDamage() / 4);
                        target.sendMessage(ChatColor.DARK_RED + "Struck by Serrated Strikes!");
                        Users.getProfile(target).addBleedTicks(5);
                        --targets;
                    }
                    else {
                        if (!pluginx.misc.bleedTracker.contains(derp)) {
                            pluginx.misc.addToBleedQue((LivingEntity)derp);
                        }
                        final LivingEntity target2 = (LivingEntity)derp;
                        target2.damage(event.getDamage() / 4);
                        --targets;
                    }
                }
            }
        }
    }
    
    public static void counterAttackChecks(final EntityDamageByEntityEvent event) {
        if (event.getDamager() instanceof Arrow) {
            return;
        }
        if (event instanceof EntityDamageByEntityEvent) {
            final Entity f = event.getDamager();
            if (event.getEntity() instanceof Player) {
                final Player defender = (Player)event.getEntity();
                final PlayerProfile PPd = Users.getProfile(defender);
                if (m.isSwords(defender.getItemInHand()) && mcPermissions.getInstance().swords(defender)) {
                    if (PPd.getSkillLevel(SkillType.SWORDS) >= 600) {
                        if (Math.random() * 2000.0 <= 600.0) {
                            Combat.dealDamage(f, event.getDamage() / 2);
                            defender.sendMessage(ChatColor.GREEN + "**COUNTER-ATTACKED**");
                            if (f instanceof Player) {
                                ((Player)f).sendMessage(ChatColor.DARK_RED + "Hit with counterattack!");
                            }
                        }
                    }
                    else if (Math.random() * 2000.0 <= PPd.getSkillLevel(SkillType.SWORDS)) {
                        Combat.dealDamage(f, event.getDamage() / 2);
                        defender.sendMessage(ChatColor.GREEN + "**COUNTER-ATTACKED**");
                        if (f instanceof Player) {
                            ((Player)f).sendMessage(ChatColor.DARK_RED + "Hit with counterattack!");
                        }
                    }
                }
            }
        }
    }
    
    public static void bleedSimulate(final nfcMMO plugin) {
        LivingEntity[] bleedQue;
        for (int length = (bleedQue = plugin.misc.bleedQue).length, i = 0; i < length; ++i) {
            final LivingEntity x = bleedQue[i];
            plugin.misc.bleedTracker.add(x);
        }
        plugin.misc.bleedQue = new LivingEntity[plugin.misc.bleedQue.length];
        plugin.misc.bleedQuePos = 0;
        LivingEntity[] bleedRemovalQue;
        for (int length2 = (bleedRemovalQue = plugin.misc.bleedRemovalQue).length, j = 0; j < length2; ++j) {
            final LivingEntity x = bleedRemovalQue[j];
            plugin.misc.bleedTracker.remove(x);
        }
        plugin.misc.bleedRemovalQue = new LivingEntity[plugin.misc.bleedRemovalQue.length];
        plugin.misc.bleedRemovalQuePos = 0;
        final Iterator<LivingEntity> iterator = plugin.misc.bleedTracker.iterator();
        while (iterator.hasNext()) {
            final LivingEntity x = iterator.next();
            if (x == null) {
                continue;
            }
            if (x.getHealth() <= 0) {
                plugin.misc.addToBleedRemovalQue(x);
            }
            else {
                x.damage(2);
            }
        }
    }
}
