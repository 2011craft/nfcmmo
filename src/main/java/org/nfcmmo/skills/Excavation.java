// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.inventory.ItemStack;
import java.util.ArrayList;
import org.nfcmmo.config.Config;
import org.bukkit.Location;
import org.bukkit.Material;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.m;
import org.nfcmmo.Users;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Excavation
{
    public static void gigaDrillBreakerActivationCheck(final Player player, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isShovel(player.getItemInHand())) {
            if (block != null && !m.abilityBlockCheck(block)) {
                return;
            }
            if (PP.getShovelPreparationMode()) {
                PP.setShovelPreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.EXCAVATION); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getGigaDrillBreakerMode() && PP.getGigaDrillBreakerDeactivatedTimeStamp() < System.currentTimeMillis()) {
                player.sendMessage(mcLocale.getString("Skills.GigaDrillBreakerOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.GigaDrillBreakerPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setGigaDrillBreakerActivatedTimeStamp(System.currentTimeMillis());
                PP.setGigaDrillBreakerDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setGigaDrillBreakerMode(true);
            }
        }
    }
    
    public static boolean canBeGigaDrillBroken(final Block block) {
        return block.getType() == Material.DIRT || block.getType() == Material.GRASS || block.getType() == Material.SAND || block.getType() == Material.GRAVEL || block.getType() == Material.CLAY;
    }
    
    public static void excavationProcCheck(final byte data, final Material type, final Location loc, final Player player) {
        if (Config.excavationRequiresShovel && !m.isShovel(player.getItemInHand())) {
            return;
        }
        final PlayerProfile PP = Users.getProfile(player);
        final ArrayList<ItemStack> is = new ArrayList<ItemStack>();
        int xp = 0;
        switch (type) {
            case GRASS: {
                if (PP.getSkillLevel(SkillType.EXCAVATION) < 250) {
                    break;
                }
                if (Config.eggs && Math.random() * 100.0 > 99.0) {
                    xp += Config.meggs;
                    is.add(new ItemStack(Material.EGG, 1, (short)0, (byte)0));
                }
                if (Config.apples && Math.random() * 100.0 > 99.0) {
                    xp += Config.mapple;
                    is.add(new ItemStack(Material.APPLE, 1, (short)0, (byte)0));
                    break;
                }
                break;
            }
            case GRAVEL: {
                if (Config.netherrack && PP.getSkillLevel(SkillType.EXCAVATION) >= 850 && Math.random() * 200.0 > 199.0) {
                    xp += Config.mnetherrack;
                    is.add(new ItemStack(Material.NETHERRACK, 1, (short)0, (byte)0));
                }
                if (Config.sulphur && PP.getSkillLevel(SkillType.EXCAVATION) >= 75 && Math.random() * 10.0 > 9.0) {
                    xp += Config.msulphur;
                    is.add(new ItemStack(Material.SULPHUR, 1, (short)0, (byte)0));
                }
                if (Config.bones && PP.getSkillLevel(SkillType.EXCAVATION) >= 175 && Math.random() * 10.0 > 9.0) {
                    xp += Config.mbones;
                    is.add(new ItemStack(Material.BONE, 1, (short)0, (byte)0));
                    break;
                }
                break;
            }
            case SAND: {
                if (Config.glowstone && PP.getSkillLevel(SkillType.EXCAVATION) >= 50 && Math.random() * 100.0 > 95.0) {
                    xp += Config.mglowstone2;
                    is.add(new ItemStack(Material.GLOWSTONE_DUST, 1, (short)0, (byte)0));
                }
                if (Config.slowsand && PP.getSkillLevel(SkillType.EXCAVATION) >= 650 && Math.random() * 200.0 > 199.0) {
                    xp += Config.mslowsand;
                    is.add(new ItemStack(Material.SOUL_SAND, 1, (short)0, (byte)0));
                    break;
                }
                break;
            }
            case CLAY: {
                if (Config.slimeballs && PP.getSkillLevel(SkillType.EXCAVATION) >= 50 && Math.random() * 20.0 > 19.0) {
                    xp += Config.mslimeballs;
                    is.add(new ItemStack(Material.SLIME_BALL, 1, (short)0, (byte)0));
                }
                if (Config.string && PP.getSkillLevel(SkillType.EXCAVATION) >= 250 && Math.random() * 20.0 > 19.0) {
                    xp += Config.mstring;
                    is.add(new ItemStack(Material.STRING, 1, (short)0, (byte)0));
                }
                if (Config.watch && PP.getSkillLevel(SkillType.EXCAVATION) >= 500 && Math.random() * 100.0 > 99.0) {
                    xp += Config.mwatch;
                    is.add(new ItemStack(Material.WATCH, 1, (short)0));
                }
                if (Config.bucket && PP.getSkillLevel(SkillType.EXCAVATION) >= 500 && Math.random() * 100.0 > 99.0) {
                    xp += Config.mbucket;
                    is.add(new ItemStack(Material.BUCKET, 1, (short)0, (byte)0));
                }
                if (Config.web && PP.getSkillLevel(SkillType.EXCAVATION) >= 750 && Math.random() * 20.0 > 19.0) {
                    xp += Config.mweb;
                    is.add(new ItemStack(Material.WEB, 1, (short)0, (byte)0));
                    break;
                }
                break;
            }
        }
        if (type == Material.GRASS || type == Material.DIRT || type == Material.GRAVEL || type == Material.SAND || type == Material.CLAY) {
            xp += Config.mbase;
            if (PP.getSkillLevel(SkillType.EXCAVATION) >= 750 && Config.cake && Math.random() * 2000.0 > 1999.0) {
                xp += Config.mcake;
                is.add(new ItemStack(Material.CAKE, 1, (short)0, (byte)0));
            }
            if (PP.getSkillLevel(SkillType.EXCAVATION) >= 350 && Config.diamond && Math.random() * 750.0 > 749.0) {
                xp += Config.mdiamond2;
                is.add(new ItemStack(Material.DIAMOND, 1, (short)0, (byte)0));
            }
            if (PP.getSkillLevel(SkillType.EXCAVATION) >= 250 && Config.music && Math.random() * 2000.0 > 1999.0) {
                xp += Config.mmusic;
                is.add(new ItemStack(Material.GOLD_RECORD, 1, (short)0, (byte)0));
            }
            if (PP.getSkillLevel(SkillType.EXCAVATION) >= 350 && Config.music && Math.random() * 2000.0 > 1999.0) {
                xp += Config.mmusic;
                is.add(new ItemStack(Material.GREEN_RECORD, 1, (short)0, (byte) 0));
            }
        }
        if (type == Material.DIRT || type == Material.GRASS) {
            if (PP.getSkillLevel(SkillType.EXCAVATION) >= 50 && Config.cocoabeans && Math.random() * 75.0 > 74.0) {
                xp += Config.mcocoa;
                is.add(new ItemStack(Material.getMaterial(351), 1, (short)0, (byte)3));
            }
            if (Config.mushrooms && PP.getSkillLevel(SkillType.EXCAVATION) >= 500 && Math.random() * 200.0 > 199.0) {
                xp += Config.mmushroom2;
                switch ((int)Math.random() * 1) {
                    case 0: {
                        is.add(new ItemStack(Material.BROWN_MUSHROOM, 1, (short)0, (byte)0));
                        break;
                    }
                    case 1: {
                        is.add(new ItemStack(Material.RED_MUSHROOM, 1, (short)0, (byte)0));
                        break;
                    }
                }
            }
            if (Config.glowstone && PP.getSkillLevel(SkillType.EXCAVATION) >= 25 && Math.random() * 100.0 > 95.0) {
                xp += Config.mglowstone2;
                is.add(new ItemStack(Material.GLOWSTONE_DUST, 1, (short)0, (byte)0));
            }
        }
        for (final ItemStack x : is) {
            if (x != null) {
                loc.getWorld().dropItemNaturally(loc, x);
            }
        }
        PP.addXP(SkillType.EXCAVATION, xp);
        Skills.XpCheckSkill(SkillType.EXCAVATION, player);
    }
}
