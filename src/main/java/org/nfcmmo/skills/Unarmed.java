// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.inventory.ItemStack;
import org.bukkit.Location;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.m;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;

public class Unarmed
{
    public static void berserkActivationCheck(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (player.getItemInHand().getTypeId() == 0) {
            if (PP.getFistsPreparationMode()) {
                PP.setFistsPreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.UNARMED); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getBerserkMode() && Skills.cooldownOver(player, PP.getBerserkDeactivatedTimeStamp(), Config.berserkCooldown)) {
                player.sendMessage(mcLocale.getString("Skills.BerserkOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.BerserkPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setBerserkActivatedTimeStamp(System.currentTimeMillis());
                PP.setBerserkDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setBerserkMode(true);
            }
        }
    }
    
    public static void unarmedBonus(final Player attacker, final EntityDamageByEntityEvent event) {
        final PlayerProfile PPa = Users.getProfile(attacker);
        int bonus = 0;
        if (PPa.getSkillLevel(SkillType.UNARMED) >= 250) {
            bonus += 2;
        }
        if (PPa.getSkillLevel(SkillType.UNARMED) >= 500) {
            bonus += 4;
        }
        event.setDamage(event.getDamage() + bonus);
    }
    
    public static void disarmProcCheck(final Player attacker, final Player defender) {
        final PlayerProfile PP = Users.getProfile(attacker);
        if (attacker.getItemInHand().getTypeId() == 0) {
            if (PP.getSkillLevel(SkillType.UNARMED) >= 1000) {
                if (Math.random() * 4000.0 <= 1000.0) {
                    final Location loc = defender.getLocation();
                    if (defender.getItemInHand() != null && defender.getItemInHand().getTypeId() != 0) {
                        defender.sendMessage(mcLocale.getString("Skills.Disarmed"));
                        final ItemStack item = defender.getItemInHand();
                        if (item != null) {
                            loc.getWorld().dropItemNaturally(loc, item);
                            final ItemStack itemx = null;
                            defender.setItemInHand(itemx);
                        }
                    }
                }
            }
            else if (Math.random() * 4000.0 <= PP.getSkillLevel(SkillType.UNARMED)) {
                final Location loc = defender.getLocation();
                if (defender.getItemInHand() != null && defender.getItemInHand().getTypeId() != 0) {
                    defender.sendMessage(mcLocale.getString("Skills.Disarmed"));
                    final ItemStack item = defender.getItemInHand();
                    if (item != null) {
                        loc.getWorld().dropItemNaturally(loc, item);
                        final ItemStack itemx = null;
                        defender.setItemInHand(itemx);
                    }
                }
            }
        }
    }
}
