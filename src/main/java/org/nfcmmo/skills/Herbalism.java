// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.World;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;
import org.nfcmmo.nfcMMO;
import org.bukkit.event.block.BlockBreakEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.m;
import org.nfcmmo.Users;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Herbalism
{
    public static void greenTerraCheck(final Player player, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isHoe(player.getItemInHand())) {
            if (block != null && !m.abilityBlockCheck(block)) {
                return;
            }
            if (PP.getHoePreparationMode()) {
                PP.setHoePreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.HERBALISM); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getGreenTerraMode() && Skills.cooldownOver(player, PP.getGreenTerraDeactivatedTimeStamp(), Config.greenTerraCooldown)) {
                player.sendMessage(mcLocale.getString("Skills.GreenTerraOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.GreenTerraPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setGreenTerraActivatedTimeStamp(System.currentTimeMillis());
                PP.setGreenTerraDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setGreenTerraMode(true);
            }
        }
    }
    
    public static void greenTerraWheat(final Player player, final Block block, final BlockBreakEvent event, final nfcMMO plugin) {
        if (block.getType() == Material.WHEAT && block.getData() == 7) {
            event.setCancelled(true);
            final PlayerProfile PP = Users.getProfile(player);
            Material mat = Material.getMaterial(296);
            final Location loc = block.getLocation();
            ItemStack is = new ItemStack(mat, 1, (short)0, (byte)0);
            PP.addXP(SkillType.HERBALISM, Config.mwheat);
            loc.getWorld().dropItemNaturally(loc, is);
            mat = Material.SEEDS;
            is = new ItemStack(mat, 1, (short)0, (byte)0);
            loc.getWorld().dropItemNaturally(loc, is);
            herbalismProcCheck(block, player, event, plugin);
            herbalismProcCheck(block, player, event, plugin);
            block.setData((byte)3);
        }
    }
    
    public static void greenTerra(final Player player, final Block block) {
        if (block.getType() == Material.COBBLESTONE || block.getType() == Material.DIRT) {
            if (!hasSeeds(player)) {
                player.sendMessage("You need more seeds to spread Green Terra");
            }
            if (hasSeeds(player) && block.getType() != Material.WHEAT) {
                removeSeeds(player);
                if (block.getType() == Material.DIRT) {
                    block.setType(Material.GRASS);
                }
                if (Config.enableCobbleToMossy && block.getType() == Material.COBBLESTONE) {
                    block.setType(Material.MOSSY_COBBLESTONE);
                }
            }
        }
    }
    
    public static Boolean canBeGreenTerra(final Block block) {
        final int t = block.getTypeId();
        if (t == 4 || t == 3 || t == 59 || t == 81 || t == 83 || t == 91 || t == 86 || t == 39 || t == 46 || t == 37 || t == 38) {
            return true;
        }
        return false;
    }
    
    public static boolean hasSeeds(final Player player) {
        final ItemStack[] inventory = player.getInventory().getContents();
        ItemStack[] array;
        for (int length = (array = inventory).length, i = 0; i < length; ++i) {
            final ItemStack x = array[i];
            if (x != null && x.getTypeId() == 295) {
                return true;
            }
        }
        return false;
    }
    
    public static void removeSeeds(final Player player) {
        final ItemStack[] inventory = player.getInventory().getContents();
        ItemStack[] array;
        for (int length = (array = inventory).length, i = 0; i < length; ++i) {
            final ItemStack x = array[i];
            if (x != null && x.getTypeId() == 295) {
                if (x.getAmount() == 1) {
                    x.setTypeId(0);
                    x.setAmount(0);
                    player.getInventory().setContents(inventory);
                }
                else {
                    x.setAmount(x.getAmount() - 1);
                    player.getInventory().setContents(inventory);
                }
                return;
            }
        }
    }
    
    public static void herbalismProcCheck(final Block block, final Player player, final BlockBreakEvent event, final nfcMMO plugin) {
        final PlayerProfile PP = Users.getProfile(player);
        final int type = block.getTypeId();
        final Location loc = block.getLocation();
        ItemStack is = null;
        Material mat = null;
        if (plugin.misc.blockWatchList.contains(block)) {
            return;
        }
        if (type == 59 && block.getData() == 7) {
            mat = Material.getMaterial(296);
            is = new ItemStack(mat, 1, (short)0, (byte)0);
            PP.addXP(SkillType.HERBALISM, Config.mwheat);
            if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                loc.getWorld().dropItemNaturally(loc, is);
            }
            if (Math.random() * 1500.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                event.setCancelled(true);
                loc.getWorld().dropItemNaturally(loc, is);
                mat = Material.SEEDS;
                is = new ItemStack(mat, 1, (short)0, (byte)0);
                loc.getWorld().dropItemNaturally(loc, is);
                block.setData((byte)1);
                int bonus = 0;
                if (PP.getSkillLevel(SkillType.HERBALISM) >= 200) {
                    ++bonus;
                }
                if (PP.getSkillLevel(SkillType.HERBALISM) >= 400) {
                    ++bonus;
                }
                if (PP.getSkillLevel(SkillType.HERBALISM) >= 600) {
                    ++bonus;
                }
                if (bonus > 0) {
                    is = new ItemStack(mat, bonus);
                    loc.getWorld().dropItemNaturally(loc, is);
                }
            }
        }
        if (block.getData() != 5) {
            if (type == 81) {
                final World world = block.getWorld();
                final Block[] blockArray = { block, world.getBlockAt(block.getX(), block.getY() + 1, block.getZ()), world.getBlockAt(block.getX(), block.getY() + 2, block.getZ()) };
                final Material[] materialArray = { blockArray[0].getType(), blockArray[1].getType(), blockArray[2].getType() };
                final byte[] byteArray = { blockArray[0].getData(), blockArray[0].getData(), blockArray[0].getData() };
                int x = 0;
                Block[] array;
                for (int length = (array = blockArray).length, i = 0; i < length; ++i) {
                    final Block target = array[i];
                    if (materialArray[x] == Material.CACTUS) {
                        is = new ItemStack(Material.CACTUS, 1, (short)0, (byte)0);
                        if (byteArray[x] != 5) {
                            if (Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                                loc.getWorld().dropItemNaturally(target.getLocation(), is);
                            }
                            PP.addXP(SkillType.HERBALISM, Config.mcactus);
                        }
                    }
                    ++x;
                }
            }
            if (type == 83) {
                final World world = block.getWorld();
                final Block[] blockArray = { block, world.getBlockAt(block.getX(), block.getY() + 1, block.getZ()), world.getBlockAt(block.getX(), block.getY() + 2, block.getZ()) };
                final Material[] materialArray = { blockArray[0].getType(), blockArray[1].getType(), blockArray[2].getType() };
                final byte[] byteArray = { blockArray[0].getData(), blockArray[0].getData(), blockArray[0].getData() };
                int x = 0;
                Block[] array2;
                for (int length2 = (array2 = blockArray).length, j = 0; j < length2; ++j) {
                    final Block target = array2[j];
                    if (materialArray[x] == Material.SUGAR_CANE_BLOCK) {
                        is = new ItemStack(Material.SUGAR_CANE, 1, (short)0, (byte)0);
                        if (byteArray[x] != 5) {
                            if (Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                                loc.getWorld().dropItemNaturally(target.getLocation(), is);
                            }
                            PP.addXP(SkillType.HERBALISM, Config.msugar);
                        }
                    }
                    ++x;
                }
            }
            if (type == 91 || type == 86) {
                mat = Material.getMaterial(block.getTypeId());
                is = new ItemStack(mat, 1, (short)0, (byte)0);
                if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                    loc.getWorld().dropItemNaturally(loc, is);
                }
                PP.addXP(SkillType.HERBALISM, Config.mpumpkin);
            }
            if (type == 39 || type == 40) {
                mat = Material.getMaterial(block.getTypeId());
                is = new ItemStack(mat, 1, (short)0, (byte)0);
                if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                    loc.getWorld().dropItemNaturally(loc, is);
                }
                PP.addXP(SkillType.HERBALISM, Config.mmushroom);
            }
            if (type == 37 || type == 38) {
                mat = Material.getMaterial(block.getTypeId());
                is = new ItemStack(mat, 1, (short)0, (byte)0);
                if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                    loc.getWorld().dropItemNaturally(loc, is);
                }
                PP.addXP(SkillType.HERBALISM, Config.mflower);
            }
        }
        Skills.XpCheckSkill(SkillType.HERBALISM, player);
    }
}
