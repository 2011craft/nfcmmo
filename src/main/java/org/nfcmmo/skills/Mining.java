// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.sound.SoundEffect;
import org.bukkit.Statistic;
import org.nfcmmo.nfcMMO;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.Material;

import java.util.HashMap;

import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.m;
import org.nfcmmo.Users;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class Mining
{
    public static void superBreakerCheck(final Player player, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isMiningPick(player.getItemInHand())) {
            if (block != null && !m.abilityBlockCheck(block)) {
                return;
            }
            if (PP.getPickaxePreparationMode()) {
                PP.setPickaxePreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.MINING); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getSuperBreakerMode() && Skills.cooldownOver(player, PP.getSuperBreakerDeactivatedTimeStamp(), Config.superBreakerCooldown)) {
                player.sendMessage(mcLocale.getString("Skills.SuperBreakerOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.SuperBreakerPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setSuperBreakerActivatedTimeStamp(System.currentTimeMillis());
                PP.setSuperBreakerDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setSuperBreakerMode(true);
            }
        }
    }
    
    public static void blockProcSimulate(final Block block) {
        final Location loc = block.getLocation();
        MiningNode n = MiningNode.forId(block.getTypeId());
        loc.getWorld().dropItemNaturally(loc, n.getDrop());
    }

    public static void blockProcCheck(final Block block, final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.MINING)) {
            blockProcSimulate(block);
            player.sendMessage(mcLocale.getString("SkillProcs.Mining"));
        }
    }

    public static void miningBlockCheck(final Player player, final Block block, final nfcMMO plugin) {
        final PlayerProfile PP = Users.getProfile(player);
        if (plugin.misc.blockWatchList.contains(block) || block.getData() == 5) {
            return;
        }
        int xp = 0;
        MiningNode reward = MiningNode.forId(block.getTypeId());
        if (reward != null) {
            xp += reward.getExperience();
            blockProcCheck(block, player);
            PP.addXP(SkillType.MINING, xp);
            Skills.XpCheckSkill(SkillType.MINING, player);
        }
    }

    public static Boolean canBeSuperBroken(final Block block) {
        final int t = block.getTypeId();
        return MiningNode.forId(t) != null;
    }

    public static void SuperBreakerBlockCheck(final Player player, final Block block, final nfcMMO plugin) {
        final PlayerProfile PP = Users.getProfile(player);
        final Location loc = block.getLocation();

        if (Config.toolsLoseDurabilityFromAbilities) {
            m.damageTool(player, (short) Config.abilityDurabilityLoss);
        }

        MiningNode node = MiningNode.forId(block.getTypeId());
        int tier = m.getTier(player);
        if (node == null || (tier != 0 && tier < node.tierRequirement)) {
            return;
        }

        if (!plugin.misc.blockWatchList.contains(block) && block.getData() != 5) {
            PP.addXP(SkillType.MINING, node.getExperience());
            blockProcCheck(block, player);
            blockProcCheck(block, player);
        }

        loc.getWorld().dropItemNaturally(loc, node.getDrop());
        player.incrementStatistic(Statistic.MINE_BLOCK, block.getType());
        block.setType(Material.AIR);

        if (Config.spoutEnabled) {
            SpoutStuff.playSoundForPlayer(SoundEffect.POP, player, block.getLocation());
        }
        Skills.XpCheckSkill(SkillType.MINING, player);
    }

    public enum MiningNode {
        STONE(new int[] {1, 24}, 0,30, "Stone") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(4), 1);
            }
        },
        COAL(new int[] {16}, 0, 100, "Coal") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(263), 1);
            }
        },
        IRON(new int[] {15}, 2, 250, "Iron"),
        GOLD(new int[] {14}, 2, 250, "Gold"),
        REDSTONE(new int[] {73, 74}, 2, 150, "Redstone") {
            @Override public ItemStack getDrop() {
                int amount = Math.random() > 0.5 ? 4 : 3;
                return new ItemStack(Material.getMaterial(331), amount);
            }
        },
        DIAMOND(new int[] {56}, 4, 750, "Diamond") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(264), 1);
            }
        },
        OBSIDIAN(new int[] {49}, 5, 150, "Obsidian"),
        NETHERRACK(new int[] {87}, 0, 30, "Netherrack"),
        GLOWSTONE(new int[] {89}, 0, 30, "Glowstone") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(348), 1);
            }
        },
        LAPIS(new int[] {21}, 2, 400, "lapis") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(351), 4);
            }
        },
        //NFC ORES
        ALUMINUM(new int[] {154}, 0, 125, "Aluminum"),
        COPPER(new int[] {155}, 0, 125, "Copper"),
        TIN(new int[] {156}, 0, 125, "Tin"),
        ZINC(new int[] {157}, 0, 125, "Zinc"),
        NICKEL(new int[] {158}, 0, 125, "Nickel"),
        BISMUTH(new int[] {159}, 0,  125, "Bismuth"),
        OSMIUM(new int[] {160}, 4, 125, "Osmium"),
        TUNGSTEN(new int[] {161}, 3,  125, "Tungsten"),
        MAGNETITE(new int[] {162}, 3, 125, "Magnetite"),
        SILVER(new int[] {163}, 1, 200, "Silver"),
        LEAD(new int[] {164}, 0, 200, "Lead"),
        SILICON(new int[] {165}, 2, 200, "Silicon"),
        CHROME(new int[] {166}, 2, 200, "Chrome"),
        ANTHRACITE(new int[] {167}, 0, 300, "Anthracite"),
        TITANIUM(new int[] {168}, 3, 300, "Titanium"),
        URANINITE(new int[] {169}, 5, 300, "Uraninite"),
        RUBY(new int[] {170}, 3, 450, "Ruby") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(472), 1);
            }
        },
        ONYX(new int[] {171}, 3, 450, "Onyx") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(706), 1);
            }
        },
        SAPPHIRE(new int[] {172}, 3, 450, "Sapphire") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(473), 1);
            }
        },
        EMERALD(new int[] {173}, 3, 450, "Emerald") {
            @Override public ItemStack getDrop() {
                return new ItemStack(Material.getMaterial(474), 1);
            }
        },
        BORON(new int[] {174}, 1, 200, "Boron"),
        PLATINUM(new int[] {175}, 1, 450, "Platinum")
        ;

        final int[] ids;
        final int defaultValue;
        final int tierRequirement;
        final String configToken;
        static HashMap<Integer, MiningNode> idMap = new HashMap<>();
        MiningNode(int[] ids, int tierRequirement, int defaultValue, String configToken) {
            this.ids = ids;
            this.defaultValue = defaultValue;
            this.configToken = configToken;
            this.tierRequirement = tierRequirement;
        }

        static {
            for (MiningNode reward : values()) {
                for (int id : reward.ids) {
                    idMap.put(id, reward);
                }
            }
        }

        public static MiningNode forId(int id) {
            return idMap.get(id);
        }

        public int getExperience() {
            return Config.readInteger("Experience.Mining." + this.configToken, this.defaultValue);
        }

        public ItemStack getDrop() {
            return new ItemStack(Material.getMaterial(this.ids[0]), 1);
        }
    }
}
