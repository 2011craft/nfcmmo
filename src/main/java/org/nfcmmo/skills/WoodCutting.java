// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.Location;
import java.util.ArrayList;
import org.nfcmmo.nfcMMO;
import org.bukkit.ChatColor;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.m;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.inventory.ItemStack;
import org.nfcmmo.datatypes.SkillType;
import org.bukkit.Material;
import org.nfcmmo.Users;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class WoodCutting
{
    static int w;
    private static boolean isdone;
    
    static {
        WoodCutting.w = 0;
        WoodCutting.isdone = false;
    }
    
    public static void woodCuttingProcCheck(final Player player, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        final byte type = block.getData();
        final Material mat = Material.getMaterial(block.getTypeId());
        if (player != null && Math.random() * 1000.0 <= PP.getSkillLevel(SkillType.WOODCUTTING)) {
            final ItemStack item = new ItemStack(mat, 1, (short)0, Byte.valueOf(type));
            block.getWorld().dropItemNaturally(block.getLocation(), item);
        }
    }
    
    public static void treeFellerCheck(final Player player, final Block block) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isAxes(player.getItemInHand())) {
            if (block != null && !m.abilityBlockCheck(block)) {
                return;
            }
            if (PP.getAxePreparationMode()) {
                PP.setAxePreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.WOODCUTTING); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getTreeFellerMode() && Skills.cooldownOver(player, PP.getTreeFellerDeactivatedTimeStamp() * 1000L, Config.treeFellerCooldown)) {
                player.sendMessage(mcLocale.getString("Skills.TreeFellerOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.TreeFellerPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setTreeFellerActivatedTimeStamp(System.currentTimeMillis());
                PP.setTreeFellerDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setTreeFellerMode(true);
            }
            if (!PP.getTreeFellerMode() && !Skills.cooldownOver(player, PP.getTreeFellerDeactivatedTimeStamp() * 1000L, Config.treeFellerCooldown)) {
                player.sendMessage(ChatColor.RED + "You are too tired to use that ability again." + ChatColor.YELLOW + " (" + Skills.calculateTimeLeft(player, PP.getTreeFellerDeactivatedTimeStamp() * 1000L, Config.treeFellerCooldown) + "s)");
            }
        }
    }
    
    public static void treeFeller(final Block block, final Player player, final nfcMMO plugin) {
        final PlayerProfile PP = Users.getProfile(player);
        int radius = 1;
        if (PP.getSkillLevel(SkillType.WOODCUTTING) >= 500) {
            ++radius;
        }
        if (PP.getSkillLevel(SkillType.WOODCUTTING) >= 950) {
            ++radius;
        }
        final ArrayList<Block> blocklist = new ArrayList<Block>();
        final ArrayList<Block> toAdd = new ArrayList<Block>();
        if (block != null) {
            blocklist.add(block);
        }
        while (!WoodCutting.isdone) {
            addBlocksToTreeFelling(blocklist, toAdd, radius);
        }
        WoodCutting.isdone = false;
        for (final Block x : toAdd) {
            if (!plugin.misc.treeFeller.contains(x)) {
                plugin.misc.treeFeller.add(x);
            }
        }
        toAdd.clear();
    }
    
    public static void addBlocksToTreeFelling(final ArrayList<Block> blocklist, final ArrayList<Block> toAdd, final Integer radius) {
        int u = 0;
        for (final Block x : blocklist) {
            ++u;
            if (toAdd.contains(x)) {
                continue;
            }
            WoodCutting.w = 0;
            final Location loc = x.getLocation();
            final int vx = x.getX();
            final int vy = x.getY();
            final int vz = x.getZ();
            for (int cx = -radius; cx <= radius; ++cx) {
                for (int cy = -radius; cy <= radius; ++cy) {
                    for (int cz = -radius; cz <= radius; ++cz) {
                        final Block blocktarget = loc.getWorld().getBlockAt(vx + cx, vy + cy, vz + cz);
                        if (!blocklist.contains(blocktarget) && !toAdd.contains(blocktarget) && (blocktarget.getTypeId() == 17 || blocktarget.getTypeId() == 18)) {
                            toAdd.add(blocktarget);
                            ++WoodCutting.w;
                        }
                    }
                }
            }
        }
        for (final Block xx : toAdd) {
            if (!blocklist.contains(xx)) {
                blocklist.add(xx);
            }
        }
        if (u >= blocklist.size()) {
            WoodCutting.isdone = true;
        }
        else {
            WoodCutting.isdone = false;
        }
    }
}
