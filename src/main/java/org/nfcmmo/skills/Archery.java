// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.Location;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.party.Party;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.entity.Entity;
import org.nfcmmo.nfcMMO;

public class Archery
{
    public static void trackArrows(final nfcMMO pluginx, final Entity x, final EntityDamageByEntityEvent event, final Player attacker) {
        final PlayerProfile PPa = Users.getProfile(attacker);
        if (!pluginx.misc.arrowTracker.containsKey(x) && event.getDamage() > 0) {
            pluginx.misc.arrowTracker.put(x, 0);
            if (attacker != null && Math.random() * 1000.0 <= PPa.getSkillLevel(SkillType.ARCHERY)) {
                pluginx.misc.arrowTracker.put(x, 1);
            }
        }
        else if (event.getDamage() > 0 && attacker != null && Math.random() * 1000.0 <= PPa.getSkillLevel(SkillType.ARCHERY)) {
            pluginx.misc.arrowTracker.put(x, 1);
        }
    }
    
    public static void ignitionCheck(final Entity x, final EntityDamageByEntityEvent event, final Player attacker) {
        final PlayerProfile PPa = Users.getProfile(attacker);
        if (Math.random() * 100.0 >= 75.0) {
            int ignition = 20 + (PPa.getSkillLevel(SkillType.ARCHERY) / 10);
            if (x instanceof Player) {
                final Player Defender = (Player)x;
                if (!Party.getInstance().inSameParty(attacker, Defender)) {
                    event.getEntity().setFireTicks(ignition);
                    attacker.sendMessage(mcLocale.getString("Combat.Ignition"));
                    Defender.sendMessage(mcLocale.getString("Combat.BurningArrowHit"));
                }
            }
            else {
                event.getEntity().setFireTicks(ignition);
                attacker.sendMessage(mcLocale.getString("Combat.Ignition"));
            }
        }
    }
    
    public static void dazeCheck(final Player defender, final Player attacker) {
        final PlayerProfile PPa = Users.getProfile(attacker);
        final Location loc = defender.getLocation();
        if (Math.random() * 10.0 > 5.0) {
            loc.setPitch(90.0f);
        }
        else {
            loc.setPitch(-90.0f);
        }
        if (PPa.getSkillLevel(SkillType.ARCHERY) >= 1000) {
            if (Math.random() * 1000.0 <= 500.0) {
                defender.teleport(loc);
                defender.sendMessage(mcLocale.getString("Combat.TouchedFuzzy"));
                attacker.sendMessage(mcLocale.getString("Combat.TargetDazed"));
            }
        }
        else if (Math.random() * 2000.0 <= PPa.getSkillLevel(SkillType.ARCHERY)) {
            defender.teleport(loc);
            defender.sendMessage(mcLocale.getString("Combat.TouchedFuzzy"));
            attacker.sendMessage(mcLocale.getString("Combat.TargetDazed"));
        }
    }
}
