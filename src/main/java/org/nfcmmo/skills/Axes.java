// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.skills;

import org.bukkit.entity.LivingEntity;
import org.nfcmmo.party.Party;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Wolf;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.ChatColor;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.config.Config;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.m;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;

public class Axes
{
    public static void skullSplitterCheck(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        if (m.isAxes(player.getItemInHand()) && mcPermissions.getInstance().axesAbility(player)) {
            if (PP.getAxePreparationMode()) {
                PP.setAxePreparationMode(false);
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.AXES); x >= 50; x -= 50, ++ticks) {}
            if (!PP.getSkullSplitterMode() && Skills.cooldownOver(player, PP.getSkullSplitterDeactivatedTimeStamp() * 1000L, Config.skullSplitterCooldown)) {
                player.sendMessage(mcLocale.getString("Skills.SkullSplitterOn"));
                for (final Player y : player.getWorld().getPlayers()) {
                    if (y != null && y != player && m.getDistance(player.getLocation(), y.getLocation()) < 10.0) {
                        y.sendMessage(mcLocale.getString("Skills.SkullSplitterPlayer", new Object[] { player.getName() }));
                    }
                }
                PP.setSkullSplitterActivatedTimeStamp(System.currentTimeMillis());
                PP.setSkullSplitterDeactivatedTimeStamp(System.currentTimeMillis() + ticks * 1000);
                PP.setSkullSplitterMode(true);
            }
            if (!PP.getSkullSplitterMode() && !Skills.cooldownOver(player, PP.getSkullSplitterDeactivatedTimeStamp() * 1000L, Config.skullSplitterCooldown)) {
                player.sendMessage(String.valueOf(mcLocale.getString("Skills.TooTired")) + ChatColor.YELLOW + " (" + Skills.calculateTimeLeft(player, PP.getSkullSplitterDeactivatedTimeStamp() * 1000L, Config.skullSplitterCooldown) + "s)");
            }
        }
    }
    
    public static void axeCriticalCheck(final Player attacker, final EntityDamageByEntityEvent event, final Plugin pluginx) {
        final Entity x = event.getEntity();
        if (x instanceof Wolf) {
            final Wolf wolf = (Wolf)x;
            if (Taming.getOwner((Entity)wolf, pluginx) != null) {
                if (Taming.getOwner((Entity)wolf, pluginx) == attacker) {
                    return;
                }
                if (Party.getInstance().inSameParty(attacker, Taming.getOwner((Entity)wolf, pluginx))) {
                    return;
                }
            }
        }
        final PlayerProfile PPa = Users.getProfile(attacker);
        if (m.isAxes(attacker.getItemInHand()) && mcPermissions.getInstance().axes(attacker)) {
            if (PPa.getSkillLevel(SkillType.AXES) >= 750) {
                if (Math.random() * 1000.0 <= 750.0) {
                    if (x instanceof Player) {
                        final Player player = (Player)x;
                        player.sendMessage(ChatColor.DARK_RED + "You were CRITICALLY hit!");
                    }
                    if (x instanceof Player) {
                        event.setDamage(event.getDamage() * 2 - event.getDamage() / 2);
                    }
                    else {
                        event.setDamage(event.getDamage() * 2);
                    }
                    attacker.sendMessage(ChatColor.RED + "CRITICAL HIT!");
                }
            }
            else if (Math.random() * 1000.0 <= PPa.getSkillLevel(SkillType.AXES)) {
                if (x instanceof Player) {
                    final Player player = (Player)x;
                    player.sendMessage(ChatColor.DARK_RED + "You were CRITICALLY hit!");
                }
                if (x instanceof Player) {
                    event.setDamage(event.getDamage() * 2 - event.getDamage() / 2);
                }
                else {
                    event.setDamage(event.getDamage() * 2);
                }
                attacker.sendMessage(ChatColor.RED + "CRITICAL HIT!");
            }
        }
    }
    
    public static void applyAoeDamage(final Player attacker, final EntityDamageByEntityEvent event, final Plugin pluginx) {
        int targets = 0;
        if (event.getEntity() instanceof LivingEntity) {
            final LivingEntity x = (LivingEntity)event.getEntity();
            targets = m.getTier(attacker);
            for (final Entity entity : x.getWorld().getEntities()) {
                if (m.getDistance(x.getLocation(), entity.getLocation()) < 5.0) {
                    if (entity instanceof Wolf) {
                        final Wolf wolf = (Wolf)entity;
                        if (Taming.getOwner((Entity)wolf, pluginx) == attacker) {
                            continue;
                        }
                        if (Party.getInstance().inSameParty(attacker, Taming.getOwner((Entity)wolf, pluginx))) {
                            continue;
                        }
                    }
                    if (!(entity instanceof LivingEntity) || targets < 1) {
                        continue;
                    }
                    if (entity instanceof Player) {
                        final Player target = (Player)entity;
                        if (Users.getProfile(target).getGodMode() || target.getName().equals(attacker.getName()) || Party.getInstance().inSameParty(attacker, target) || targets < 1 || !entity.getWorld().getPVP()) {
                            continue;
                        }
                        target.damage(event.getDamage() / 2);
                        target.sendMessage(ChatColor.DARK_RED + "Struck by CLEAVE!");
                        --targets;
                    }
                    else {
                        final LivingEntity target2 = (LivingEntity)entity;
                        target2.damage(event.getDamage() / 2);
                        --targets;
                    }
                }
            }
        }
    }
}
