// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

import org.nfcmmo.m;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.Color;
import org.nfcmmo.config.Config;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.bukkit.plugin.Plugin;
import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.SpoutManager;
import org.nfcmmo.Users;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.nfcmmo.nfcMMO;
import org.getspout.spoutapi.gui.GenericTexture;
import org.getspout.spoutapi.gui.GenericGradient;
import org.getspout.spoutapi.gui.Widget;

public class HUDmmo
{
    int center_x;
    int center_y;
    String playerName;
    Widget xpbar;
    GenericGradient xpfill;
    GenericGradient xpbg;
    GenericGradient xpicon_bg;
    GenericGradient xpicon_border;
    GenericTexture xpicon;
    nfcMMO plugin;
    
    public HUDmmo(final Player player) {
        this.center_x = 213;
        this.center_y = 120;
        this.playerName = null;
        this.xpbar = null;
        this.xpfill = null;
        this.xpbg = null;
        this.xpicon_bg = null;
        this.xpicon_border = null;
        this.xpicon = null;
        this.plugin = (nfcMMO)Bukkit.getServer().getPluginManager().getPlugin("nfcMMO");
        this.playerName = player.getName();
        this.initializeHUD(player);
    }
    
    public void initializeHUD(final Player player) {
        final HUDType type = Users.getProfile(player).getHUDType();
        switch (type) {
            case RETRO: {
                this.initializeXpBarDisplayRetro(SpoutManager.getPlayer(player));
                break;
            }
            case STANDARD: {
                this.initializeXpBarDisplayStandard(SpoutManager.getPlayer(player));
                break;
            }
            case SMALL: {
                this.initializeXpBarDisplaySmall(SpoutManager.getPlayer(player));
                break;
            }
        }
    }
    
    public void updateXpBarDisplay(final HUDType type, final Player player) {
        switch (type) {
            case RETRO: {
                this.updateXpBarRetro(player, Users.getProfile(player));
                break;
            }
            case STANDARD: {
                this.updateXpBarStandard(player, Users.getProfile(player));
                break;
            }
            case SMALL: {
                this.updateXpBarStandard(player, Users.getProfile(player));
                break;
            }
        }
    }
    
    public void resetHUD() {
        final SpoutPlayer sPlayer = SpoutStuff.getSpoutPlayer(this.playerName);
        if (sPlayer != null) {
            sPlayer.getMainScreen().removeWidgets((Plugin)this.plugin);
            this.xpbar = null;
            this.xpfill = null;
            this.xpbg = null;
            this.xpicon = null;
            sPlayer.getMainScreen().setDirty(true);
        }
    }
    
    private void initializeXpBarDisplayRetro(final SpoutPlayer sPlayer) {
        final Color border = new Color((float) Config.xpborder_r, (float) Config.xpborder_g, (float) Config.xpborder_b, 1.0f);
        final Color green = new Color(0.0f, 1.0f, 0.0f, 1.0f);
        final Color background = new Color((float) Config.xpbackground_r, (float) Config.xpbackground_g, (float) Config.xpbackground_b, 1.0f);
        final Color darkbg = new Color(0.2f, 0.2f, 0.2f, 1.0f);
        this.xpicon = new GenericTexture();
        this.xpbar = (Widget)new GenericGradient();
        this.xpfill = new GenericGradient();
        this.xpbg = new GenericGradient();
        this.xpicon_bg = new GenericGradient();
        this.xpicon_border = new GenericGradient();
        this.xpicon_bg.setBottomColor(darkbg).setTopColor(darkbg).setWidth(4).setHeight(4).setPriority(RenderPriority.High).setX(142).setY(10).setDirty(true);
        this.xpicon_border.setBottomColor(border).setTopColor(border).setWidth(6).setHeight(6).setPriority(RenderPriority.Highest).setX(141).setY(9).setDirty(true);
        this.xpicon.setWidth(6).setHeight(6).setX(141).setY(9).setPriority(RenderPriority.Normal).setDirty(true);
        this.xpicon.setUrl("Icon_r.png");
        this.xpbar.setWidth(128).setHeight(4).setX(149).setY(10);
        ((GenericGradient)this.xpbar).setBottomColor(border).setTopColor(border).setPriority(RenderPriority.Highest).setDirty(true);
        this.xpfill.setWidth(0).setHeight(2).setX(150).setY(11);
        this.xpfill.setBottomColor(green).setTopColor(green).setPriority(RenderPriority.Lowest).setDirty(true);
        this.xpbg.setWidth(126).setHeight(2).setX(150).setY(11);
        this.xpbg.setBottomColor(background).setTopColor(background).setPriority(RenderPriority.Low).setDirty(true);
        if (Config.xpbar) {
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpbar);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpfill);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpbg);
            if (Config.xpicon) {
                sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpicon);
                sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpicon_bg);
            }
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpicon_border);
        }
        sPlayer.getMainScreen().setDirty(true);
    }
    
    private void initializeXpBarDisplayStandard(final SpoutPlayer sPlayer) {
        this.xpbar = (Widget)new GenericTexture();
        if (Config.xpbar && Config.xpicon) {
            (this.xpicon = new GenericTexture()).setUrl("Icon.png");
            this.xpicon.setHeight(16).setWidth(32).setX(Config.xpicon_x).setY(Config.xpicon_y);
            this.xpicon.setDirty(true);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpicon);
        }
        if (Config.xpbar) {
            ((GenericTexture)this.xpbar).setUrl("xpbar_inc000.png");
            this.xpbar.setX(Config.xpbar_x).setY(Config.xpbar_y).setHeight(8).setWidth(256);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, this.xpbar);
        }
        sPlayer.getMainScreen().setDirty(true);
    }
    
    private void initializeXpBarDisplaySmall(final SpoutPlayer sPlayer) {
        this.xpbar = (Widget)new GenericTexture();
        if (Config.xpbar && Config.xpicon) {
            (this.xpicon = new GenericTexture()).setUrl("Icon.png");
            this.xpicon.setHeight(8).setWidth(16).setX(this.center_x - 72).setY(Config.xpicon_y + 2);
            this.xpicon.setDirty(true);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, (Widget)this.xpicon);
        }
        if (Config.xpbar) {
            ((GenericTexture)this.xpbar).setUrl("xpbar_inc000.png");
            this.xpbar.setX(this.center_x - 64).setY(Config.xpbar_y).setHeight(4).setWidth(128);
            sPlayer.getMainScreen().attachWidget((Plugin)this.plugin, this.xpbar);
        }
        sPlayer.getMainScreen().setDirty(true);
    }
    
    private void updateXpBarStandard(final Player player, final PlayerProfile PP) {
        if (!Config.xpbar) {
            return;
        }
        SkillType theType = null;
        if (PP.getXpBarLocked()) {
            theType = PP.getSkillLock();
        }
        else {
            theType = PP.getLastGained();
        }
        if (theType == null) {
            return;
        }
        this.xpicon.setUrl(String.valueOf(m.getCapitalized(theType.toString())) + ".png");
        this.xpicon.setDirty(true);
        ((GenericTexture)this.xpbar).setUrl(SpoutStuff.getUrlBar(SpoutStuff.getXpInc(PP.getSkillXpLevel(theType), PP.getXpToLevel(theType), HUDType.STANDARD)));
        this.xpbar.setDirty(true);
        SpoutManager.getPlayer(player).getMainScreen().setDirty(true);
    }
    
    private void updateXpBarRetro(final Player player, final PlayerProfile PP) {
        if (!Config.xpbar) {
            return;
        }
        SkillType theType = null;
        if (PP.getXpBarLocked() && PP.getSkillLock() != null) {
            theType = PP.getSkillLock();
        }
        else {
            theType = PP.getLastGained();
        }
        if (theType == null) {
            return;
        }
        final Color color = SpoutStuff.getRetroColor(theType);
        if (this.xpicon != null && theType != null) {
            this.xpicon.setUrl(String.valueOf(m.getCapitalized(theType.toString())) + "_r.png");
        }
        if (theType != null) {
            this.xpfill.setBottomColor(color).setTopColor(color).setWidth((int)SpoutStuff.getXpInc(PP.getSkillXpLevel(theType), PP.getXpToLevel(theType), HUDType.RETRO)).setDirty(true);
        }
        else {
            System.out.println("theType was null!");
        }
        SpoutManager.getPlayer(player).getMainScreen().setDirty(true);
    }
}
