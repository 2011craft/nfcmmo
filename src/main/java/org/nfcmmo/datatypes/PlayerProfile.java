// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

import org.bukkit.Location;
import org.bukkit.Bukkit;
import org.bukkit.World;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.logging.Level;
import org.nfcmmo.m;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import org.nfcmmo.nfcMMO;
import org.nfcmmo.config.Config;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.logging.Logger;

public class PlayerProfile
{
    protected final Logger log;
    private HUDType hud;
    private String party;
    private String myspawn;
    private String myspawnworld;
    private String invite;
    private boolean partyhud;
    private boolean spoutcraft;
    private boolean filling;
    private boolean xpbarlocked;
    private boolean placedAnvil;
    private boolean partyChatMode;
    private boolean adminChatMode;
    private boolean godMode;
    private boolean greenTerraMode;
    private boolean partyChatOnly;
    private boolean greenTerraInformed;
    private boolean berserkInformed;
    private boolean skullSplitterInformed;
    private boolean gigaDrillBreakerInformed;
    private boolean superBreakerInformed;
    private boolean serratedStrikesInformed;
    private boolean treeFellerInformed;
    private boolean dead;
    private boolean abilityuse;
    private boolean treeFellerMode;
    private boolean superBreakerMode;
    private boolean gigaDrillBreakerMode;
    private boolean serratedStrikesMode;
    private boolean hoePreparationMode;
    private boolean shovelPreparationMode;
    private boolean swordsPreparationMode;
    private boolean fistsPreparationMode;
    private boolean pickaxePreparationMode;
    private boolean axePreparationMode;
    private boolean skullSplitterMode;
    private boolean berserkMode;
    private int recentlyHurt;
    private int berserkATS;
    private int berserkDATS;
    private int gigaDrillBreakerATS;
    private int gigaDrillBreakerDATS;
    private int respawnATS;
    private int mySpawnATS;
    private int greenTerraATS;
    private int greenTerraDATS;
    private int superBreakerATS;
    private int superBreakerDATS;
    private int serratedStrikesATS;
    private int serratedStrikesDATS;
    private int treeFellerATS;
    private int treeFellerDATS;
    private int skullSplitterATS;
    private int skullSplitterDATS;
    private int hoePreparationATS;
    private int axePreparationATS;
    private int pickaxePreparationATS;
    private int fistsPreparationATS;
    private int shovelPreparationATS;
    private int swordsPreparationATS;
    private SkillType lastgained;
    private SkillType skillLock;
    private int xpbarinc;
    private int lastlogin;
    private int userid;
    private int bleedticks;
    private int mana;
    private int greenDyeCycleSel;
    private int greenDyeCycle;
    private int blueDyeCycle;
    private int blueDyeCycleSel;
    public boolean dyeChanged;
    private String playername;
    HashMap<SkillType, Integer> skills;
    HashMap<SkillType, Integer> skillsXp;
    String location;
    
    public PlayerProfile(final Player player) {
        this.log = Logger.getLogger("Minecraft");
        this.partyhud = true;
        this.spoutcraft = false;
        this.filling = false;
        this.xpbarlocked = false;
        this.placedAnvil = false;
        this.partyChatMode = false;
        this.adminChatMode = false;
        this.godMode = false;
        this.partyChatOnly = false;
        this.greenTerraInformed = true;
        this.berserkInformed = true;
        this.skullSplitterInformed = true;
        this.gigaDrillBreakerInformed = true;
        this.superBreakerInformed = true;
        this.serratedStrikesInformed = true;
        this.treeFellerInformed = true;
        this.abilityuse = true;
        this.hoePreparationMode = false;
        this.shovelPreparationMode = false;
        this.swordsPreparationMode = false;
        this.fistsPreparationMode = false;
        this.pickaxePreparationMode = false;
        this.axePreparationMode = false;
        this.recentlyHurt = 0;
        this.berserkATS = 0;
        this.berserkDATS = 0;
        this.gigaDrillBreakerATS = 0;
        this.gigaDrillBreakerDATS = 0;
        this.respawnATS = 0;
        this.mySpawnATS = 0;
        this.greenTerraATS = 0;
        this.greenTerraDATS = 0;
        this.superBreakerATS = 0;
        this.superBreakerDATS = 0;
        this.serratedStrikesATS = 0;
        this.serratedStrikesDATS = 0;
        this.treeFellerATS = 0;
        this.treeFellerDATS = 0;
        this.skullSplitterATS = 0;
        this.skullSplitterDATS = 0;
        this.hoePreparationATS = 0;
        this.axePreparationATS = 0;
        this.pickaxePreparationATS = 0;
        this.fistsPreparationATS = 0;
        this.shovelPreparationATS = 0;
        this.swordsPreparationATS = 0;
        this.lastgained = null;
        this.skillLock = null;
        this.xpbarinc = 0;
        this.lastlogin = 0;
        this.userid = 0;
        this.bleedticks = 0;
        this.mana = 0;
        this.greenDyeCycleSel = 0;
        this.greenDyeCycle = 0;
        this.blueDyeCycle = 0;
        this.blueDyeCycleSel = 0;
        this.dyeChanged = false;
        this.skills = new HashMap<SkillType, Integer>();
        this.skillsXp = new HashMap<SkillType, Integer>();
        this.location = "plugins/nfcMMO/FlatFileStuff/nfcmmo.users";
        this.hud = Config.defaulthud;
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType skillType = values[i];
            if (skillType != SkillType.ALL) {
                this.skills.put(skillType, 0);
                this.skillsXp.put(skillType, 0);
            }
        }
        this.playername = player.getName();
        if (Config.useMySQL) {
            if (!this.loadMySQL(player)) {
                this.addMySQLPlayer(player);
                this.loadMySQL(player);
            }
        }
        else if (!this.load()) {
            this.addPlayer();
        }
        this.lastlogin = (int) (System.currentTimeMillis() / 1000L);
    }
    
    public int getLastLogin() {
        return this.lastlogin;
    }
    
    public int getMySQLuserId() {
        return this.userid;
    }
    
    public boolean loadMySQL(final Player p) {
        Integer id = 0;
        id = nfcMMO.database.GetInt("SELECT id FROM " + Config.MySQLtablePrefix + "users WHERE user = '" + p.getName() + "'");
        if (id == 0) {
            return false;
        }
        this.userid = id;
        if (id > 0) {
            final HashMap<Integer, ArrayList<String>> huds = nfcMMO.database.Read("SELECT hudtype FROM " + Config.MySQLtablePrefix + "huds WHERE user_id = " + id);
            if (huds.get(1) == null) {
                nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "huds (user_id) VALUES (" + id + ")");
            }
            else if (huds.get(1).get(0) != null) {
                HUDType[] values;
                for (int length = (values = HUDType.values()).length, i = 0; i < length; ++i) {
                    final HUDType x = values[i];
                    if (x.toString().equals(huds.get(1).get(0))) {
                        this.hud = x;
                    }
                }
            }
            else {
                this.hud = Config.defaulthud;
            }
            final HashMap<Integer, ArrayList<String>> users = nfcMMO.database.Read("SELECT lastlogin, party FROM " + Config.MySQLtablePrefix + "users WHERE id = " + id);
            this.party = users.get(1).get(1);
            final HashMap<Integer, ArrayList<String>> spawn = nfcMMO.database.Read("SELECT world, x, y, z FROM " + Config.MySQLtablePrefix + "spawn WHERE user_id = " + id);
            this.myspawnworld = spawn.get(1).get(0);
            this.myspawn = String.valueOf(spawn.get(1).get(1)) + "," + spawn.get(1).get(2) + "," + spawn.get(1).get(3);
            final HashMap<Integer, ArrayList<String>> cooldowns = nfcMMO.database.Read("SELECT mining, woodcutting, unarmed, herbalism, excavation, swords, axes FROM " + Config.MySQLtablePrefix + "cooldowns WHERE user_id = " + id);
            if (cooldowns.get(1) == null) {
                nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "cooldowns (user_id) VALUES (" + id + ")");
            }
            else {
                this.superBreakerDATS = Integer.valueOf(cooldowns.get(1).get(0));
                this.treeFellerDATS = Integer.valueOf(cooldowns.get(1).get(1));
                this.berserkDATS = Integer.valueOf(cooldowns.get(1).get(2));
                this.greenTerraDATS = Integer.valueOf(cooldowns.get(1).get(3));
                this.gigaDrillBreakerDATS = Integer.valueOf(cooldowns.get(1).get(4));
                this.serratedStrikesDATS = Integer.valueOf(cooldowns.get(1).get(5));
                this.skullSplitterDATS = Integer.valueOf(cooldowns.get(1).get(6));
            }
            final HashMap<Integer, ArrayList<String>> stats = nfcMMO.database.Read("SELECT taming, mining, repair, woodcutting, unarmed, herbalism, excavation, archery, swords, axes, acrobatics FROM " + Config.MySQLtablePrefix + "skills WHERE user_id = " + id);
            this.skills.put(SkillType.TAMING, Integer.valueOf(stats.get(1).get(0)));
            this.skills.put(SkillType.MINING, Integer.valueOf(stats.get(1).get(1)));
            this.skills.put(SkillType.REPAIR, Integer.valueOf(stats.get(1).get(2)));
            this.skills.put(SkillType.WOODCUTTING, Integer.valueOf(stats.get(1).get(3)));
            this.skills.put(SkillType.UNARMED, Integer.valueOf(stats.get(1).get(4)));
            this.skills.put(SkillType.HERBALISM, Integer.valueOf(stats.get(1).get(5)));
            this.skills.put(SkillType.EXCAVATION, Integer.valueOf(stats.get(1).get(6)));
            this.skills.put(SkillType.ARCHERY, Integer.valueOf(stats.get(1).get(7)));
            this.skills.put(SkillType.SWORDS, Integer.valueOf(stats.get(1).get(8)));
            this.skills.put(SkillType.AXES, Integer.valueOf(stats.get(1).get(9)));
            this.skills.put(SkillType.ACROBATICS, Integer.valueOf(stats.get(1).get(10)));
            final HashMap<Integer, ArrayList<String>> experience = nfcMMO.database.Read("SELECT taming, mining, repair, woodcutting, unarmed, herbalism, excavation, archery, swords, axes, acrobatics FROM " + Config.MySQLtablePrefix + "experience WHERE user_id = " + id);
            this.skillsXp.put(SkillType.TAMING, Integer.valueOf(experience.get(1).get(0)));
            this.skillsXp.put(SkillType.MINING, Integer.valueOf(experience.get(1).get(1)));
            this.skillsXp.put(SkillType.REPAIR, Integer.valueOf(experience.get(1).get(2)));
            this.skillsXp.put(SkillType.WOODCUTTING, Integer.valueOf(experience.get(1).get(3)));
            this.skillsXp.put(SkillType.UNARMED, Integer.valueOf(experience.get(1).get(4)));
            this.skillsXp.put(SkillType.HERBALISM, Integer.valueOf(experience.get(1).get(5)));
            this.skillsXp.put(SkillType.EXCAVATION, Integer.valueOf(experience.get(1).get(6)));
            this.skillsXp.put(SkillType.ARCHERY, Integer.valueOf(experience.get(1).get(7)));
            this.skillsXp.put(SkillType.SWORDS, Integer.valueOf(experience.get(1).get(8)));
            this.skillsXp.put(SkillType.AXES, Integer.valueOf(experience.get(1).get(9)));
            this.skillsXp.put(SkillType.ACROBATICS, Integer.valueOf(experience.get(1).get(10)));
            return true;
        }
        return false;
    }
    
    public void addMySQLPlayer(final Player p) {
        Integer id = 0;
        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "users (user, lastlogin) VALUES ('" + p.getName() + "'," + System.currentTimeMillis() / 1000L + ")");
        id = nfcMMO.database.GetInt("SELECT id FROM " + Config.MySQLtablePrefix + "users WHERE user = '" + p.getName() + "'");
        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "cooldowns (user_id) VALUES (" + id + ")");
        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "spawn (user_id) VALUES (" + id + ")");
        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "skills (user_id) VALUES (" + id + ")");
        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "experience (user_id) VALUES (" + id + ")");
        this.userid = id;
    }
    
    public boolean load() {
        try {
            final FileReader file = new FileReader(this.location);
            final BufferedReader in = new BufferedReader(file);
            String line = "";
            while ((line = in.readLine()) != null) {
                final String[] character = line.split(":");
                if (!character[0].equals(this.playername)) {
                    continue;
                }
                if (character.length > 1 && m.isInt(character[1])) {
                    this.skills.put(SkillType.MINING, Integer.valueOf(character[1]));
                }
                if (character.length > 2) {
                    this.myspawn = character[2];
                }
                if (character.length > 3) {
                    this.party = character[3];
                }
                if (character.length > 4 && m.isInt(character[4])) {
                    this.skillsXp.put(SkillType.MINING, Integer.valueOf(character[4]));
                }
                if (character.length > 5 && m.isInt(character[5])) {
                    this.skills.put(SkillType.WOODCUTTING, Integer.valueOf(character[5]));
                }
                if (character.length > 6 && m.isInt(character[6])) {
                    this.skillsXp.put(SkillType.WOODCUTTING, Integer.valueOf(character[6]));
                }
                if (character.length > 7 && m.isInt(character[7])) {
                    this.skills.put(SkillType.REPAIR, Integer.valueOf(character[7]));
                }
                if (character.length > 8 && m.isInt(character[8])) {
                    this.skills.put(SkillType.UNARMED, Integer.valueOf(character[8]));
                }
                if (character.length > 9 && m.isInt(character[9])) {
                    this.skills.put(SkillType.HERBALISM, Integer.valueOf(character[9]));
                }
                if (character.length > 10 && m.isInt(character[10])) {
                    this.skills.put(SkillType.EXCAVATION, Integer.valueOf(character[10]));
                }
                if (character.length > 11 && m.isInt(character[11])) {
                    this.skills.put(SkillType.ARCHERY, Integer.valueOf(character[11]));
                }
                if (character.length > 12 && m.isInt(character[12])) {
                    this.skills.put(SkillType.SWORDS, Integer.valueOf(character[12]));
                }
                if (character.length > 13 && m.isInt(character[13])) {
                    this.skills.put(SkillType.AXES, Integer.valueOf(character[13]));
                }
                if (character.length > 14 && m.isInt(character[14])) {
                    this.skills.put(SkillType.ACROBATICS, Integer.valueOf(character[14]));
                }
                if (character.length > 15 && m.isInt(character[15])) {
                    this.skillsXp.put(SkillType.REPAIR, Integer.valueOf(character[15]));
                }
                if (character.length > 16 && m.isInt(character[16])) {
                    this.skillsXp.put(SkillType.UNARMED, Integer.valueOf(character[16]));
                }
                if (character.length > 17 && m.isInt(character[17])) {
                    this.skillsXp.put(SkillType.HERBALISM, Integer.valueOf(character[17]));
                }
                if (character.length > 18 && m.isInt(character[18])) {
                    this.skillsXp.put(SkillType.EXCAVATION, Integer.valueOf(character[18]));
                }
                if (character.length > 19 && m.isInt(character[19])) {
                    this.skillsXp.put(SkillType.ARCHERY, Integer.valueOf(character[19]));
                }
                if (character.length > 20 && m.isInt(character[20])) {
                    this.skillsXp.put(SkillType.SWORDS, Integer.valueOf(character[20]));
                }
                if (character.length > 21 && m.isInt(character[21])) {
                    this.skillsXp.put(SkillType.AXES, Integer.valueOf(character[21]));
                }
                if (character.length > 22 && m.isInt(character[22])) {
                    this.skillsXp.put(SkillType.ACROBATICS, Integer.valueOf(character[22]));
                }
                if (character.length > 23 && m.isInt(character[23])) {
                    this.myspawnworld = character[23];
                }
                if (character.length > 24 && m.isInt(character[24])) {
                    this.skills.put(SkillType.TAMING, Integer.valueOf(character[24]));
                }
                if (character.length > 25 && m.isInt(character[25])) {
                    this.skillsXp.put(SkillType.TAMING, Integer.valueOf(character[25]));
                }
                if (character.length > 26) {
                    this.berserkDATS = Integer.valueOf(character[26]);
                }
                if (character.length > 27) {
                    this.gigaDrillBreakerDATS = Integer.valueOf(character[27]);
                }
                if (character.length > 28) {
                    this.treeFellerDATS = Integer.valueOf(character[28]);
                }
                if (character.length > 29) {
                    this.greenTerraDATS = Integer.valueOf(character[29]);
                }
                if (character.length > 30) {
                    this.serratedStrikesDATS = Integer.valueOf(character[30]);
                }
                if (character.length > 31) {
                    this.skullSplitterDATS = Integer.valueOf(character[31]);
                }
                if (character.length > 32) {
                    this.superBreakerDATS = Integer.valueOf(character[32]);
                }
                if (character.length > 33) {
                    HUDType[] values;
                    for (int length = (values = HUDType.values()).length, i = 0; i < length; ++i) {
                        final HUDType x = values[i];
                        if (x.toString().equalsIgnoreCase(character[33])) {
                            this.hud = x;
                        }
                    }
                }
                in.close();
                return true;
            }
            in.close();
        }
        catch (Exception e) {
            this.log.log(Level.SEVERE, "Exception while reading " + this.location + " (Are you sure you formatted it correctly?)", e);
        }
        return false;
    }
    
    public void save() {
        final Long timestamp = System.currentTimeMillis() / 1000L;
        if (Config.useMySQL) {
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "huds SET " + " hudtype = '" + this.hud.toString() + "' WHERE user_id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "users SET lastlogin = " + timestamp.intValue() + " WHERE id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "users SET party = '" + this.party + "' WHERE id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "spawn SET world = '" + this.myspawnworld + "', x = " + this.getX() + ", y = " + this.getY() + ", z = " + this.getZ() + " WHERE user_id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "cooldowns SET " + " mining = " + this.superBreakerDATS + ", woodcutting = " + this.treeFellerDATS + ", unarmed = " + this.berserkDATS + ", herbalism = " + this.greenTerraDATS + ", excavation = " + this.gigaDrillBreakerDATS + ", swords = " + this.serratedStrikesDATS + ", axes = " + this.skullSplitterDATS + " WHERE user_id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "skills SET " + "  taming = " + this.skills.get(SkillType.TAMING) + ", mining = " + this.skills.get(SkillType.MINING) + ", repair = " + this.skills.get(SkillType.REPAIR) + ", woodcutting = " + this.skills.get(SkillType.WOODCUTTING) + ", unarmed = " + this.skills.get(SkillType.UNARMED) + ", herbalism = " + this.skills.get(SkillType.HERBALISM) + ", excavation = " + this.skills.get(SkillType.EXCAVATION) + ", archery = " + this.skills.get(SkillType.ARCHERY) + ", swords = " + this.skills.get(SkillType.SWORDS) + ", axes = " + this.skills.get(SkillType.AXES) + ", acrobatics = " + this.skills.get(SkillType.ACROBATICS) + " WHERE user_id = " + this.userid);
            nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "experience SET " + "  taming = " + this.skillsXp.get(SkillType.TAMING) + ", mining = " + this.skillsXp.get(SkillType.MINING) + ", repair = " + this.skillsXp.get(SkillType.REPAIR) + ", woodcutting = " + this.skillsXp.get(SkillType.WOODCUTTING) + ", unarmed = " + this.skillsXp.get(SkillType.UNARMED) + ", herbalism = " + this.skillsXp.get(SkillType.HERBALISM) + ", excavation = " + this.skillsXp.get(SkillType.EXCAVATION) + ", archery = " + this.skillsXp.get(SkillType.ARCHERY) + ", swords = " + this.skillsXp.get(SkillType.SWORDS) + ", axes = " + this.skillsXp.get(SkillType.AXES) + ", acrobatics = " + this.skillsXp.get(SkillType.ACROBATICS) + " WHERE user_id = " + this.userid);
        }
        else {
            try {
                final FileReader file = new FileReader(this.location);
                final BufferedReader in = new BufferedReader(file);
                final StringBuilder writer = new StringBuilder();
                String line = "";
                while ((line = in.readLine()) != null) {
                    if (!line.split(":")[0].equalsIgnoreCase(this.playername)) {
                        writer.append(line).append("\r\n");
                    }
                    else {
                        writer.append(String.valueOf(this.playername) + ":");
                        writer.append(this.skills.get(SkillType.MINING) + ":");
                        writer.append(String.valueOf(this.myspawn) + ":");
                        writer.append(String.valueOf(this.party) + ":");
                        writer.append(this.skillsXp.get(SkillType.MINING) + ":");
                        writer.append(this.skills.get(SkillType.WOODCUTTING) + ":");
                        writer.append(this.skillsXp.get(SkillType.WOODCUTTING) + ":");
                        writer.append(this.skills.get(SkillType.REPAIR) + ":");
                        writer.append(this.skills.get(SkillType.UNARMED) + ":");
                        writer.append(this.skills.get(SkillType.HERBALISM) + ":");
                        writer.append(this.skills.get(SkillType.EXCAVATION) + ":");
                        writer.append(this.skills.get(SkillType.ARCHERY) + ":");
                        writer.append(this.skills.get(SkillType.SWORDS) + ":");
                        writer.append(this.skills.get(SkillType.AXES) + ":");
                        writer.append(this.skills.get(SkillType.ACROBATICS) + ":");
                        writer.append(this.skillsXp.get(SkillType.REPAIR) + ":");
                        writer.append(this.skillsXp.get(SkillType.UNARMED) + ":");
                        writer.append(this.skillsXp.get(SkillType.HERBALISM) + ":");
                        writer.append(this.skillsXp.get(SkillType.EXCAVATION) + ":");
                        writer.append(this.skillsXp.get(SkillType.ARCHERY) + ":");
                        writer.append(this.skillsXp.get(SkillType.SWORDS) + ":");
                        writer.append(this.skillsXp.get(SkillType.AXES) + ":");
                        writer.append(this.skillsXp.get(SkillType.ACROBATICS) + ":");
                        writer.append(String.valueOf(this.myspawnworld) + ":");
                        writer.append(this.skills.get(SkillType.TAMING) + ":");
                        writer.append(this.skillsXp.get(SkillType.TAMING) + ":");
                        writer.append(String.valueOf(String.valueOf(this.berserkDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.gigaDrillBreakerDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.treeFellerDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.greenTerraDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.serratedStrikesDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.skullSplitterDATS)) + ":");
                        writer.append(String.valueOf(String.valueOf(this.superBreakerDATS)) + ":");
                        writer.append(String.valueOf(this.hud.toString()) + ":");
                        writer.append("\r\n");
                    }
                }
                in.close();
                final FileWriter out = new FileWriter(this.location);
                out.write(writer.toString());
                out.close();
            }
            catch (Exception e) {
                this.log.log(Level.SEVERE, "Exception while writing to " + this.location + " (Are you sure you formatted it correctly?)", e);
            }
        }
    }
    
    public void addPlayer() {
        try {
            final FileWriter file = new FileWriter(this.location, true);
            final BufferedWriter out = new BufferedWriter(file);
            out.append((CharSequence)(String.valueOf(this.playername) + ":"));
            out.append((CharSequence)"0:");
            out.append((CharSequence)(String.valueOf(this.myspawn) + ":"));
            out.append((CharSequence)(String.valueOf(this.party) + ":"));
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)(String.valueOf(this.myspawnworld) + ":"));
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)"0:");
            out.append((CharSequence)(String.valueOf(Config.defaulthud.toString()) + ":"));
            out.newLine();
            out.close();
        }
        catch (Exception e) {
            this.log.log(Level.SEVERE, "Exception while writing to " + this.location + " (Are you sure you formatted it correctly?)", e);
        }
    }
    
    public void togglePartyHUD() {
        this.partyhud = !this.partyhud;
    }
    
    public boolean getPartyHUD() {
        return this.partyhud;
    }
    
    public void toggleSpoutEnabled() {
        this.spoutcraft = !this.spoutcraft;
    }
    
    public boolean isFilling() {
        return this.filling;
    }
    
    public void toggleFilling() {
        this.filling = !this.filling;
    }
    
    public HUDType getHUDType() {
        return this.hud;
    }
    
    public void setHUDType(final HUDType type) {
        this.hud = type;
        this.save();
    }
    
    public boolean getXpBarLocked() {
        return this.xpbarlocked;
    }
    
    public void toggleXpBarLocked() {
        this.xpbarlocked = !this.xpbarlocked;
    }
    
    public int getXpBarInc() {
        return this.xpbarinc;
    }
    
    public void setXpBarInc(final int newvalue) {
        this.xpbarinc = newvalue;
    }
    
    public void setSkillLock(final SkillType newvalue) {
        this.skillLock = newvalue;
    }
    
    public SkillType getSkillLock() {
        return this.skillLock;
    }
    
    public void setLastGained(final SkillType newvalue) {
        this.lastgained = newvalue;
    }
    
    public SkillType getLastGained() {
        return this.lastgained;
    }
    
    public boolean getAdminChatMode() {
        return this.adminChatMode;
    }
    
    public boolean getPartyChatMode() {
        return this.partyChatMode;
    }
    
    public boolean getGodMode() {
        return this.godMode;
    }
    
    public void togglePlacedAnvil() {
        this.placedAnvil = !this.placedAnvil;
    }
    
    public Boolean getPlacedAnvil() {
        return this.placedAnvil;
    }
    
    public void toggleAdminChat() {
        this.adminChatMode = !this.adminChatMode;
    }
    
    public void toggleGodMode() {
        this.godMode = !this.godMode;
    }
    
    public void togglePartyChat() {
        this.partyChatMode = !this.partyChatMode;
    }
    
    public void setMana(final int newvalue) {
        this.mana = newvalue;
    }
    
    public int getCurrentMana() {
        return this.mana;
    }
    
    public void setDyeChanged(final Boolean bool) {
        this.dyeChanged = bool;
    }
    
    public boolean getDyeChanged() {
        return this.dyeChanged;
    }
    
    public void setBlueDyeCycle(final int newvalue) {
        this.blueDyeCycle = newvalue;
    }
    
    public int getBlueDyeCycle() {
        return this.blueDyeCycle;
    }
    
    public void setBlueDyeCycleSel(final int newvalue) {
        this.blueDyeCycleSel = newvalue;
    }
    
    public int getBlueDyeCycleSel() {
        return this.blueDyeCycleSel;
    }
    
    public void setGreenDyeCycle(final int newvalue) {
        this.greenDyeCycle = newvalue;
    }
    
    public int getGreenDyeCycle() {
        return this.greenDyeCycle;
    }
    
    public void setGreenDyeCycleSel(final int newvalue) {
        this.greenDyeCycleSel = newvalue;
    }
    
    public int getGreenDyeCycleSel() {
        return this.greenDyeCycleSel;
    }
    
    public boolean isPlayer(final String player) {
        return player.equals(this.playername);
    }
    
    public boolean getPartyChatOnlyToggle() {
        return this.partyChatOnly;
    }
    
    public void togglePartyChatOnly() {
        this.partyChatOnly = !this.partyChatOnly;
    }
    
    public boolean getAbilityUse() {
        return this.abilityuse;
    }
    
    public void toggleAbilityUse() {
        this.abilityuse = !this.abilityuse;
    }
    
    public long getMySpawnATS() {
        return this.mySpawnATS;
    }
    
    public void setMySpawnATS(final long newvalue) {
        this.mySpawnATS = (int)(newvalue / 1000L);
    }
    
    public void decreaseBleedTicks() {
        --this.bleedticks;
    }
    
    public Integer getBleedTicks() {
        return this.bleedticks;
    }
    
    public void setBleedTicks(final Integer newvalue) {
        this.bleedticks = newvalue;
    }
    
    public void addBleedTicks(final Integer newvalue) {
        this.bleedticks += newvalue;
    }
    
    public long getRespawnATS() {
        return this.respawnATS;
    }
    
    public void setRespawnATS(final long newvalue) {
        this.respawnATS = (int)(newvalue / 1000L);
    }
    
    public boolean getHoePreparationMode() {
        return this.hoePreparationMode;
    }
    
    public void setHoePreparationMode(final Boolean bool) {
        this.hoePreparationMode = bool;
    }
    
    public long getHoePreparationATS() {
        return this.hoePreparationATS;
    }
    
    public void setHoePreparationATS(final long newvalue) {
        this.hoePreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getSwordsPreparationMode() {
        return this.swordsPreparationMode;
    }
    
    public void setSwordsPreparationMode(final Boolean bool) {
        this.swordsPreparationMode = bool;
    }
    
    public long getSwordsPreparationATS() {
        return this.swordsPreparationATS;
    }
    
    public void setSwordsPreparationATS(final long newvalue) {
        this.swordsPreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getShovelPreparationMode() {
        return this.shovelPreparationMode;
    }
    
    public void setShovelPreparationMode(final Boolean bool) {
        this.shovelPreparationMode = bool;
    }
    
    public long getShovelPreparationATS() {
        return this.shovelPreparationATS;
    }
    
    public void setShovelPreparationATS(final long newvalue) {
        this.shovelPreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getFistsPreparationMode() {
        return this.fistsPreparationMode;
    }
    
    public void setFistsPreparationMode(final Boolean bool) {
        this.fistsPreparationMode = bool;
    }
    
    public long getFistsPreparationATS() {
        return this.fistsPreparationATS;
    }
    
    public void setFistsPreparationATS(final long newvalue) {
        this.fistsPreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getAxePreparationMode() {
        return this.axePreparationMode;
    }
    
    public void setAxePreparationMode(final Boolean bool) {
        this.axePreparationMode = bool;
    }
    
    public long getAxePreparationATS() {
        return this.axePreparationATS;
    }
    
    public void setAxePreparationATS(final long newvalue) {
        this.axePreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getPickaxePreparationMode() {
        return this.pickaxePreparationMode;
    }
    
    public void setPickaxePreparationMode(final Boolean bool) {
        this.pickaxePreparationMode = bool;
    }
    
    public long getPickaxePreparationATS() {
        return this.pickaxePreparationATS;
    }
    
    public void setPickaxePreparationATS(final long newvalue) {
        this.pickaxePreparationATS = (int)(newvalue / 1000L);
    }
    
    public boolean getGreenTerraInformed() {
        return this.greenTerraInformed;
    }
    
    public void setGreenTerraInformed(final Boolean bool) {
        this.greenTerraInformed = bool;
    }
    
    public boolean getGreenTerraMode() {
        return this.greenTerraMode;
    }
    
    public void setGreenTerraMode(final Boolean bool) {
        this.greenTerraMode = bool;
    }
    
    public long getGreenTerraActivatedTimeStamp() {
        return this.greenTerraATS;
    }
    
    public void setGreenTerraActivatedTimeStamp(final Long newvalue) {
        this.greenTerraATS = (int)(newvalue / 1000L);
    }
    
    public long getGreenTerraDeactivatedTimeStamp() {
        return this.greenTerraDATS;
    }
    
    public void setGreenTerraDeactivatedTimeStamp(final Long newvalue) {
        this.greenTerraDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getBerserkInformed() {
        return this.berserkInformed;
    }
    
    public void setBerserkInformed(final Boolean bool) {
        this.berserkInformed = bool;
    }
    
    public boolean getBerserkMode() {
        return this.berserkMode;
    }
    
    public void setBerserkMode(final Boolean bool) {
        this.berserkMode = bool;
    }
    
    public long getBerserkActivatedTimeStamp() {
        return this.berserkATS;
    }
    
    public void setBerserkActivatedTimeStamp(final Long newvalue) {
        this.berserkATS = (int)(newvalue / 1000L);
    }
    
    public long getBerserkDeactivatedTimeStamp() {
        return this.berserkDATS;
    }
    
    public void setBerserkDeactivatedTimeStamp(final Long newvalue) {
        this.berserkDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getSkullSplitterInformed() {
        return this.skullSplitterInformed;
    }
    
    public void setSkullSplitterInformed(final Boolean bool) {
        this.skullSplitterInformed = bool;
    }
    
    public boolean getSkullSplitterMode() {
        return this.skullSplitterMode;
    }
    
    public void setSkullSplitterMode(final Boolean bool) {
        this.skullSplitterMode = bool;
    }
    
    public long getSkullSplitterActivatedTimeStamp() {
        return this.skullSplitterATS;
    }
    
    public void setSkullSplitterActivatedTimeStamp(final Long newvalue) {
        this.skullSplitterATS = (int)(newvalue / 1000L);
    }
    
    public long getSkullSplitterDeactivatedTimeStamp() {
        return this.skullSplitterDATS;
    }
    
    public void setSkullSplitterDeactivatedTimeStamp(final Long newvalue) {
        this.skullSplitterDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getSerratedStrikesInformed() {
        return this.serratedStrikesInformed;
    }
    
    public void setSerratedStrikesInformed(final Boolean bool) {
        this.serratedStrikesInformed = bool;
    }
    
    public boolean getSerratedStrikesMode() {
        return this.serratedStrikesMode;
    }
    
    public void setSerratedStrikesMode(final Boolean bool) {
        this.serratedStrikesMode = bool;
    }
    
    public long getSerratedStrikesActivatedTimeStamp() {
        return this.serratedStrikesATS;
    }
    
    public void setSerratedStrikesActivatedTimeStamp(final Long newvalue) {
        this.serratedStrikesATS = (int)(newvalue / 1000L);
    }
    
    public long getSerratedStrikesDeactivatedTimeStamp() {
        return this.serratedStrikesDATS;
    }
    
    public void setSerratedStrikesDeactivatedTimeStamp(final Long newvalue) {
        this.serratedStrikesDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getGigaDrillBreakerInformed() {
        return this.gigaDrillBreakerInformed;
    }
    
    public void setGigaDrillBreakerInformed(final Boolean bool) {
        this.gigaDrillBreakerInformed = bool;
    }
    
    public boolean getGigaDrillBreakerMode() {
        return this.gigaDrillBreakerMode;
    }
    
    public void setGigaDrillBreakerMode(final Boolean bool) {
        this.gigaDrillBreakerMode = bool;
    }
    
    public long getGigaDrillBreakerActivatedTimeStamp() {
        return this.gigaDrillBreakerATS;
    }
    
    public void setGigaDrillBreakerActivatedTimeStamp(final Long newvalue) {
        this.gigaDrillBreakerATS = (int)(newvalue / 1000L);
    }
    
    public long getGigaDrillBreakerDeactivatedTimeStamp() {
        return this.gigaDrillBreakerDATS;
    }
    
    public void setGigaDrillBreakerDeactivatedTimeStamp(final Long newvalue) {
        this.gigaDrillBreakerDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getTreeFellerInformed() {
        return this.treeFellerInformed;
    }
    
    public void setTreeFellerInformed(final Boolean bool) {
        this.treeFellerInformed = bool;
    }
    
    public boolean getTreeFellerMode() {
        return this.treeFellerMode;
    }
    
    public void setTreeFellerMode(final Boolean bool) {
        this.treeFellerMode = bool;
    }
    
    public long getTreeFellerActivatedTimeStamp() {
        return this.treeFellerATS;
    }
    
    public void setTreeFellerActivatedTimeStamp(final Long newvalue) {
        this.treeFellerATS = (int)(newvalue / 1000L);
    }
    
    public long getTreeFellerDeactivatedTimeStamp() {
        return this.treeFellerDATS;
    }
    
    public void setTreeFellerDeactivatedTimeStamp(final Long newvalue) {
        this.treeFellerDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public boolean getSuperBreakerInformed() {
        return this.superBreakerInformed;
    }
    
    public void setSuperBreakerInformed(final Boolean bool) {
        this.superBreakerInformed = bool;
    }
    
    public boolean getSuperBreakerMode() {
        return this.superBreakerMode;
    }
    
    public void setSuperBreakerMode(final Boolean bool) {
        this.superBreakerMode = bool;
    }
    
    public long getSuperBreakerActivatedTimeStamp() {
        return this.superBreakerATS;
    }
    
    public void setSuperBreakerActivatedTimeStamp(final Long newvalue) {
        this.superBreakerATS = (int)(newvalue / 1000L);
    }
    
    public long getSuperBreakerDeactivatedTimeStamp() {
        return this.superBreakerDATS;
    }
    
    public void setSuperBreakerDeactivatedTimeStamp(final Long newvalue) {
        this.superBreakerDATS = (int)(newvalue / 1000L);
        this.save();
    }
    
    public long getRecentlyHurt() {
        return this.recentlyHurt;
    }
    
    public void setRecentlyHurt(final long newvalue) {
        this.recentlyHurt = (int)(newvalue / 1000L);
    }
    
    public void skillUp(final SkillType skillType, final int newvalue) {
        this.skills.put(skillType, this.skills.get(skillType) + newvalue);
        this.save();
    }
    
    public Integer getSkillLevel(final SkillType skillType) {
        return this.skills.get(skillType);
    }
    
    public Integer getSkillXpLevel(final SkillType skillType) {
        return this.skillsXp.get(skillType);
    }
    
    public void resetSkillXp(final SkillType skillType) {
        this.skills.put(skillType, 0);
    }
    
    public void addXPOverride(final SkillType skillType, final int newvalue) {
        if (skillType == SkillType.ALL) {
            SkillType[] values;
            for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
                final SkillType x = values[i];
                if (x != SkillType.ALL) {
                    this.skillsXp.put(x, this.skillsXp.get(x) + newvalue);
                }
            }
        }
        else {
            int xp = newvalue;
            xp *= Config.xpGainMultiplier;
            this.skillsXp.put(skillType, this.skillsXp.get(skillType) + xp);
            this.lastgained = skillType;
        }
    }
    
    public void addXP(final SkillType skillType, final int newvalue) {
        if (skillType == SkillType.ALL) {
            SkillType[] values;
            for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
                final SkillType x = values[i];
                if (x != SkillType.ALL) {
                    this.skillsXp.put(x, this.skillsXp.get(x) + newvalue);
                }
            }
        }
        else {
            int xp = newvalue;
            switch (skillType) {
                case TAMING: {
                    xp /= (int) Config.tamingxpmodifier;
                    break;
                }
                case MINING: {
                    xp /= (int) Config.miningxpmodifier;
                    break;
                }
                case WOODCUTTING: {
                    xp /= (int) Config.woodcuttingxpmodifier;
                    break;
                }
                case REPAIR: {
                    xp /= (int) Config.repairxpmodifier;
                    break;
                }
                case HERBALISM: {
                    xp /= (int) Config.herbalismxpmodifier;
                    break;
                }
                case ACROBATICS: {
                    xp /= (int) Config.acrobaticsxpmodifier;
                    break;
                }
                case SWORDS: {
                    xp /= (int) Config.swordsxpmodifier;
                    break;
                }
                case ARCHERY: {
                    xp /= (int) Config.archeryxpmodifier;
                    break;
                }
                case UNARMED: {
                    xp /= (int) Config.unarmedxpmodifier;
                    break;
                }
                case EXCAVATION: {
                    xp /= (int) Config.excavationxpmodifier;
                    break;
                }
                case AXES: {
                    xp /= (int) Config.axesxpmodifier;
                    break;
                }
            }
            xp *= Config.xpGainMultiplier;
            this.skillsXp.put(skillType, this.skillsXp.get(skillType) + xp);
            this.lastgained = skillType;
        }
    }
    
    public void removeXP(final SkillType skillType, final int newvalue) {
        if (skillType == SkillType.ALL) {
            this.skillsXp.put(SkillType.TAMING, this.skillsXp.get(SkillType.TAMING) - newvalue);
            this.skillsXp.put(SkillType.MINING, this.skillsXp.get(SkillType.MINING) - newvalue);
            this.skillsXp.put(SkillType.WOODCUTTING, this.skillsXp.get(SkillType.WOODCUTTING) - newvalue);
            this.skillsXp.put(SkillType.REPAIR, this.skillsXp.get(SkillType.REPAIR) - newvalue);
            this.skillsXp.put(SkillType.HERBALISM, this.skillsXp.get(SkillType.HERBALISM) - newvalue);
            this.skillsXp.put(SkillType.ACROBATICS, this.skillsXp.get(SkillType.ACROBATICS) - newvalue);
            this.skillsXp.put(SkillType.SWORDS, this.skillsXp.get(SkillType.SWORDS) - newvalue);
            this.skillsXp.put(SkillType.ARCHERY, this.skillsXp.get(SkillType.ARCHERY) - newvalue);
            this.skillsXp.put(SkillType.UNARMED, this.skillsXp.get(SkillType.UNARMED) - newvalue);
            this.skillsXp.put(SkillType.EXCAVATION, this.skillsXp.get(SkillType.EXCAVATION) - newvalue);
            this.skillsXp.put(SkillType.AXES, this.skillsXp.get(SkillType.AXES) - newvalue);
        }
        else {
            this.skillsXp.put(skillType, this.skillsXp.get(skillType) - newvalue);
        }
    }
    
    public void acceptInvite() {
        this.party = this.invite;
        this.invite = "";
    }
    
    public void modifyInvite(final String invitename) {
        this.invite = invitename;
    }
    
    public String getInvite() {
        return this.invite;
    }
    
    public void modifyskill(final SkillType skillType, final int newvalue) {
        if (skillType == SkillType.ALL) {
            this.skills.put(SkillType.TAMING, newvalue);
            this.skills.put(SkillType.MINING, newvalue);
            this.skills.put(SkillType.WOODCUTTING, newvalue);
            this.skills.put(SkillType.REPAIR, newvalue);
            this.skills.put(SkillType.HERBALISM, newvalue);
            this.skills.put(SkillType.ACROBATICS, newvalue);
            this.skills.put(SkillType.SWORDS, newvalue);
            this.skills.put(SkillType.ARCHERY, newvalue);
            this.skills.put(SkillType.UNARMED, newvalue);
            this.skills.put(SkillType.EXCAVATION, newvalue);
            this.skills.put(SkillType.AXES, newvalue);
            this.skillsXp.put(SkillType.TAMING, 0);
            this.skillsXp.put(SkillType.MINING, 0);
            this.skillsXp.put(SkillType.WOODCUTTING, 0);
            this.skillsXp.put(SkillType.REPAIR, 0);
            this.skillsXp.put(SkillType.HERBALISM, 0);
            this.skillsXp.put(SkillType.ACROBATICS, 0);
            this.skillsXp.put(SkillType.SWORDS, 0);
            this.skillsXp.put(SkillType.ARCHERY, 0);
            this.skillsXp.put(SkillType.UNARMED, 0);
            this.skillsXp.put(SkillType.EXCAVATION, 0);
            this.skillsXp.put(SkillType.AXES, 0);
        }
        else {
            this.skills.put(skillType, newvalue);
            this.skillsXp.put(skillType, newvalue);
        }
        this.save();
    }
    
    public Integer getXpToLevel(final SkillType skillType) {
        return 1020 + this.skills.get(skillType) * 20;
    }
    
    public void setParty(final String newParty) {
        this.party = newParty;
        this.save();
    }
    
    public String getParty() {
        return this.party;
    }
    
    public void removeParty() {
        this.party = null;
        this.save();
    }
    
    public boolean inParty() {
        return this.party != null && !this.party.equals("") && !this.party.equals("null");
    }
    
    public boolean hasPartyInvite() {
        return this.invite != null && !this.invite.equals("") && !this.invite.equals("null");
    }
    
    public String getMySpawnWorld() {
        if (this.myspawnworld != null && !this.myspawnworld.equals("") && !this.myspawnworld.equals("null")) {
            return this.myspawnworld;
        }
        return Bukkit.getServer().getWorlds().get(0).toString();
    }
    
    public void setMySpawn(final double x, final double y, final double z, final String myspawnworldlocation) {
        this.myspawn = String.valueOf(x) + "," + y + "," + z;
        this.myspawnworld = myspawnworldlocation;
        this.save();
    }
    
    public String getX() {
        if (this.myspawn != null) {
            final String[] split = this.myspawn.split(",");
            return split[0];
        }
        return null;
    }
    
    public String getY() {
        if (this.myspawn != null) {
            final String[] split = this.myspawn.split(",");
            return split[1];
        }
        return null;
    }
    
    public String getZ() {
        if (this.myspawn != null) {
            final String[] split = this.myspawn.split(",");
            return split[2];
        }
        return null;
    }
    
    public boolean isDead() {
        return this.dead;
    }
    
    public Location getMySpawn(final Player player) {
        Location loc = null;
        if (this.myspawn == null) {
            return null;
        }
        if (!m.isDouble(this.getX()) || !m.isDouble(this.getY()) || !m.isDouble(this.getZ())) {
            return null;
        }
        loc = new Location(player.getWorld(), Double.parseDouble(this.getX()), Double.parseDouble(this.getY()), Double.parseDouble(this.getZ()));
        loc.setYaw(0.0f);
        loc.setPitch(0.0f);
        if (loc.getX() != 0.0 && loc.getY() != 0.0 && loc.getZ() != 0.0 && loc.getWorld() != null) {
            if (Bukkit.getServer().getWorld(this.getMySpawnWorld()) != null) {
                loc.setWorld(Bukkit.getServer().getWorld(this.getMySpawnWorld()));
            }
            else {
                loc.setWorld((World)Bukkit.getServer().getWorlds().get(0));
            }
            return loc;
        }
        return null;
    }
}
