// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes.popups;

import org.getspout.spoutapi.gui.Widget;
import org.bukkit.plugin.Plugin;
import org.nfcmmo.config.Config;
import org.bukkit.ChatColor;
import org.nfcmmo.nfcMMO;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.gui.GenericLabel;
import org.nfcmmo.datatypes.buttons.ButtonEscape;
import org.nfcmmo.datatypes.buttons.ButtonPartyToggle;
import org.nfcmmo.datatypes.buttons.ButtonHUDStyle;
import org.getspout.spoutapi.gui.GenericPopup;

public class PopupMMO extends GenericPopup
{
    ButtonHUDStyle HUDButton;
    ButtonPartyToggle PartyButton;
    ButtonEscape EscapeButton;
    GenericLabel nfcMMO_label;
    GenericLabel tip_escape;
    int center_x;
    int center_y;
    
    public PopupMMO(final Player player, final PlayerProfile PP, final nfcMMO plugin) {
        this.HUDButton = null;
        this.PartyButton = null;
        this.EscapeButton = null;
        this.nfcMMO_label = new GenericLabel();
        this.tip_escape = new GenericLabel();
        this.center_x = 213;
        this.center_y = 120;
        this.nfcMMO_label.setText(ChatColor.GOLD + "~nfcMMO Menu~");
        this.nfcMMO_label.setX(this.center_x - 35).setY(this.center_y / 2 - 20).setDirty(true);
        this.tip_escape.setText(ChatColor.GRAY + "Press ESCAPE to exit!");
        this.tip_escape.setX(this.nfcMMO_label.getX() - 15).setY(this.nfcMMO_label.getY() + 10).setDirty(true);
        this.HUDButton = new ButtonHUDStyle(PP);
        this.HUDButton.setX(this.center_x - this.HUDButton.getWidth() / 2).setY(this.center_y / 2).setDirty(true);
        if (Config.partybar) {
            this.PartyButton = new ButtonPartyToggle(PP);
            this.PartyButton.setX(this.center_x - this.PartyButton.getWidth() / 2).setY(this.center_y / 2 + this.PartyButton.getHeight()).setDirty(true);
            this.attachWidget((Plugin)plugin, (Widget)this.PartyButton);
        }
        this.EscapeButton = new ButtonEscape();
        this.EscapeButton.setX(this.center_x - this.EscapeButton.getWidth() / 2).setY(this.center_y / 2 + this.HUDButton.getHeight() * 2 + 5).setDirty(true);
        this.attachWidget((Plugin)plugin, (Widget)this.HUDButton);
        this.attachWidget((Plugin)plugin, (Widget)this.nfcMMO_label);
        this.attachWidget((Plugin)plugin, (Widget)this.tip_escape);
        this.attachWidget((Plugin)plugin, (Widget)this.EscapeButton);
        this.setDirty(true);
    }
    
    public void updateButtons(final PlayerProfile PP) {
        this.HUDButton.updateText(PP);
        this.setDirty(true);
    }
}
