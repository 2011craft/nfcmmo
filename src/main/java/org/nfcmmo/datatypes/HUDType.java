// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

public enum HUDType
{
    DISABLED("DISABLED", 0), 
    STANDARD("STANDARD", 1), 
    SMALL("SMALL", 2), 
    RETRO("RETRO", 3);
    
    private HUDType(final String name, final int ordinal) {
    }
}
