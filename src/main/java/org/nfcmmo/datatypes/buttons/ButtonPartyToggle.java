// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes.buttons;

import org.nfcmmo.datatypes.PlayerProfile;
import org.getspout.spoutapi.gui.GenericButton;

public class ButtonPartyToggle extends GenericButton
{
    public ButtonPartyToggle(final PlayerProfile PP) {
        this.setText("Party HUD: " + PP.getPartyHUD());
        this.setTooltip("Toggle the Party HUD!");
        this.setWidth(120).setHeight(20);
        this.setDirty(true);
    }
    
    public void updateText(final PlayerProfile PP) {
        this.setText("Party HUD: " + PP.getPartyHUD());
        this.setDirty(true);
    }
}
