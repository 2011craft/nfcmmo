// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes.buttons;

import org.getspout.spoutapi.gui.GenericButton;

public class ButtonEscape extends GenericButton
{
    public ButtonEscape() {
        this.setText("EXIT");
        this.setWidth(60).setHeight(20);
        this.setDirty(true);
    }
}
