// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes.buttons;

import org.nfcmmo.datatypes.PlayerProfile;
import org.getspout.spoutapi.gui.GenericButton;

public class ButtonHUDStyle extends GenericButton
{
    public ButtonHUDStyle(final PlayerProfile PP) {
        this.setText("HUD Type: " + PP.getHUDType().toString());
        this.setTooltip("Change your HUD style!");
        this.setWidth(120).setHeight(20);
        this.setDirty(true);
    }
    
    public void updateText(final PlayerProfile PP) {
        this.setText("HUD Type: " + PP.getHUDType().toString());
        this.setDirty(true);
    }
}
