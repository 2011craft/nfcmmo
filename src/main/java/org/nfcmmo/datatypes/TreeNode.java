// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

import java.util.ArrayList;

public class TreeNode
{
    TreeNode left;
    TreeNode right;
    PlayerStat ps;
    
    public TreeNode(final String p, final int in) {
        this.left = null;
        this.right = null;
        this.ps = new PlayerStat();
        this.ps.statVal = in;
        this.ps.name = p;
    }
    
    public void add(final String p, final int in) {
        if (in >= this.ps.statVal) {
            if (this.left == null) {
                this.left = new TreeNode(p, in);
            }
            else {
                this.left.add(p, in);
            }
        }
        else if (in < this.ps.statVal) {
            if (this.right == null) {
                this.right = new TreeNode(p, in);
            }
            else {
                this.right.add(p, in);
            }
        }
    }
    
    public ArrayList<PlayerStat> inOrder(ArrayList<PlayerStat> a) {
        if (this.left != null) {
            a = this.left.inOrder(a);
        }
        a.add(this.ps);
        if (this.right != null) {
            a = this.right.inOrder(a);
        }
        return a;
    }
}
