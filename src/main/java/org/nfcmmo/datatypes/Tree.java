// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

import java.util.ArrayList;

public class Tree
{
    TreeNode root;
    
    public Tree() {
        this.root = null;
    }
    
    public void add(final String p, final int in) {
        if (this.root == null) {
            this.root = new TreeNode(p, in);
        }
        else {
            this.root.add(p, in);
        }
    }
    
    public PlayerStat[] inOrder() {
        if (this.root != null) {
            final ArrayList<PlayerStat> order = this.root.inOrder(new ArrayList<PlayerStat>());
            return order.toArray(new PlayerStat[order.size()]);
        }
        final ArrayList<PlayerStat> x = new ArrayList<PlayerStat>();
        final PlayerStat y = new PlayerStat();
        y.name = "$nfcMMO_DummyInfo";
        y.statVal = 0;
        x.add(y);
        return x.toArray(new PlayerStat[x.size()]);
    }
}
