// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

public enum SkillType
{
    ACROBATICS("ACROBATICS", 0), 
    ALL("ALL", 1), 
    ARCHERY("ARCHERY", 2), 
    AXES("AXES", 3), 
    EXCAVATION("EXCAVATION", 4), 
    HERBALISM("HERBALISM", 5), 
    MINING("MINING", 6), 
    REPAIR("REPAIR", 7), 
    SWORDS("SWORDS", 8), 
    TAMING("TAMING", 9), 
    UNARMED("UNARMED", 10), 
    WOODCUTTING("WOODCUTTING", 11);
    
    private SkillType(final String name, final int ordinal) {
    }
}
