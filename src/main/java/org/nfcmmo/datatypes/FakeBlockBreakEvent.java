// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.datatypes;

import org.bukkit.entity.Player;
import org.bukkit.block.Block;
import org.bukkit.event.block.BlockBreakEvent;

public class FakeBlockBreakEvent extends BlockBreakEvent
{
    private static final long serialVersionUID = 1L;
    
    public FakeBlockBreakEvent(final Block theBlock, final Player player) {
        super(theBlock, player);
    }
}
