// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import java.util.HashMap;
import java.util.logging.Level;
import java.io.BufferedReader;
import java.io.FileReader;

import org.craft11.API;
import org.nfcmmo.config.Config;
import org.bukkit.Material;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import org.bukkit.event.Event;
import org.bukkit.Bukkit;
import org.nfcmmo.datatypes.FakeBlockBreakEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.datatypes.SkillType;
import org.bukkit.entity.Player;
import org.bukkit.block.Block;
import java.util.logging.Logger;

public class m
{
    public static final Logger log;
    
    static {
        log = Logger.getLogger("Minecraft");
    }
    
    public static String getCapitalized(final String target) {
        final String firstLetter = target.substring(0, 1);
        final String remainder = target.substring(1);
        final String capitalized = String.valueOf(firstLetter.toUpperCase()) + remainder.toLowerCase();
        return capitalized;
    }
    
    public static int getInt(final String string) {
        if (isInt(string)) {
            return Integer.parseInt(string);
        }
        return 0;
    }
    
    public static Double getDouble(final String string) {
        if (isDouble(string)) {
            return Double.parseDouble(string);
        }
        return 0.0;
    }
    
    public static boolean isDouble(final String string) {
        try {
            Double.parseDouble(string);
        }
        catch (NumberFormatException nFE) {
            return false;
        }
        return true;
    }
    
    public static boolean shouldBeWatched(final Block block) {
        final int id = block.getTypeId();
        return id == 82 || id == 16 || id == 73 || id == 49 || id == 81 || id == 83 || id == 86 || id == 91 || id == 1 || id == 17 || id == 42 || id == 87 || id == 89 || id == 2 || id == 3 || id == 12 || id == 13 || id == 21 || id == 15 || id == 14 || id == 56 || id == 38 || id == 37 || id == 39 || id == 40 || id == 24;
    }
    
    public static int getPowerLevel(final Player player) {
        final PlayerProfile PP = Users.getProfile(player);
        int x = 0;
        if (mcPermissions.getInstance().taming(player)) {
            x += PP.getSkillLevel(SkillType.TAMING);
        }
        if (mcPermissions.getInstance().mining(player)) {
            x += PP.getSkillLevel(SkillType.MINING);
        }
        if (mcPermissions.getInstance().woodcutting(player)) {
            x += PP.getSkillLevel(SkillType.WOODCUTTING);
        }
        if (mcPermissions.getInstance().unarmed(player)) {
            x += PP.getSkillLevel(SkillType.UNARMED);
        }
        if (mcPermissions.getInstance().herbalism(player)) {
            x += PP.getSkillLevel(SkillType.HERBALISM);
        }
        if (mcPermissions.getInstance().excavation(player)) {
            x += PP.getSkillLevel(SkillType.EXCAVATION);
        }
        if (mcPermissions.getInstance().archery(player)) {
            x += PP.getSkillLevel(SkillType.ARCHERY);
        }
        if (mcPermissions.getInstance().swords(player)) {
            x += PP.getSkillLevel(SkillType.SWORDS);
        }
        if (mcPermissions.getInstance().axes(player)) {
            x += PP.getSkillLevel(SkillType.AXES);
        }
        if (mcPermissions.getInstance().acrobatics(player)) {
            x += PP.getSkillLevel(SkillType.ACROBATICS);
        }
        if (mcPermissions.getInstance().repair(player)) {
            x += PP.getSkillLevel(SkillType.REPAIR);
        }
        return x;
    }
    
    public static boolean blockBreakSimulate(final Block block, final Player player) {
        final FakeBlockBreakEvent event = new FakeBlockBreakEvent(block, player);
        if (block != null && player != null) {
            Bukkit.getServer().getPluginManager().callEvent((Event)event);
            return !event.isCancelled();
        }
        return false;
    }
    
    public static void damageTool(final Player player, final short damage) {
        API.DamageTool(player, damage);
    }
    
    public static Integer getTier(final Player player) {
        return API.GetTier(player);
    }

    public static double getDistance(final Location loca, final Location locb) {
        return Math.sqrt(Math.pow(loca.getX() - locb.getX(), 2.0) + Math.pow(loca.getY() - locb.getY(), 2.0) + Math.pow(loca.getZ() - locb.getZ(), 2.0));
    }
    
    public static boolean abilityBlockCheck(final Block block) {
        final int i = block.getTypeId();
        return i != 96 && i != 68 && i != 355 && i != 26 && i != 323 && i != 25 && i != 54 && i != 69 && i != 92 && i != 77 && i != 58 && i != 61 && i != 62 && i != 42 && i != 71 && i != 64 && i != 84 && i != 324 && i != 330;
    }
    
    public static boolean isBlockAround(final Location loc, final Integer radius, final Integer typeid) {
        final Block blockx = loc.getBlock();
        final int ox = blockx.getX();
        final int oy = blockx.getY();
        final int oz = blockx.getZ();
        for (int cx = -radius; cx <= radius; ++cx) {
            for (int cy = -radius; cy <= radius; ++cy) {
                for (int cz = -radius; cz <= radius; ++cz) {
                    final Block block = loc.getWorld().getBlockAt(ox + cx, oy + cy, oz + cz);
                    if (block.getTypeId() == typeid) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    
    public static Integer calculateHealth(final Integer health, final Integer newvalue) {
        if (health + newvalue > 20) {
            return 20;
        }
        return health + newvalue;
    }
    
    public Integer calculateMinusHealth(final Integer health, final Integer newvalue) {
        if (health - newvalue < 1) {
            return 0;
        }
        return health - newvalue;
    }
    
    public static boolean isInt(final String string) {
        try {
            Integer.parseInt(string);
        }
        catch (NumberFormatException nFE) {
            return false;
        }
        return true;
    }
    
    public static void mcDropItem(final Location loc, final int id) {
        if (loc != null) {
            final Material mat = Material.getMaterial(id);
            final byte damage = 0;
            final ItemStack item = new ItemStack(mat, 1, (short)0, Byte.valueOf(damage));
            loc.getWorld().dropItemNaturally(loc, item);
        }
    }
    
    public static boolean isSwords(final ItemStack is) {
        return API.IsSword(is);
    }
    
    public static boolean isHoe(final ItemStack is) {
        return API.IsHoe(is);
    }
    
    public static boolean isShovel(final ItemStack is) {
        return API.IsShovel(is);
    }
    
    public static boolean isAxes(final ItemStack is) {
        return API.IsAxe(is);
    }
    
    public static boolean isMiningPick(final ItemStack is) {
        return API.IsPickaxe(is);
    }
    
    public boolean isGold(final ItemStack is) {
        API.Tool t = API.Tool.forId(is.getTypeId());
        return t == API.Tool.GOLD;
    }
    
    public static void convertToMySQL() {
        if (!Config.useMySQL) {
            return;
        }
        final String location = "plugins/nfcMMO/FlatFileStuff/nfcmmo.users";
        try {
            final FileReader file = new FileReader(location);
            final BufferedReader in = new BufferedReader(file);
            String line = "";
            String playerName = null;
            String mining = null;
            String party = null;
            String miningXP = null;
            String woodcutting = null;
            String woodCuttingXP = null;
            String repair = null;
            String unarmed = null;
            String herbalism = null;
            String excavation = null;
            String archery = null;
            String swords = null;
            String axes = null;
            String acrobatics = null;
            String repairXP = null;
            String unarmedXP = null;
            String herbalismXP = null;
            String excavationXP = null;
            String archeryXP = null;
            String swordsXP = null;
            String axesXP = null;
            String acrobaticsXP = null;
            String taming = null;
            String tamingXP = null;
            int id = 0;
            int theCount = 0;
            while ((line = in.readLine()) != null) {
                final String[] character = line.split(":");
                playerName = character[0];
                if (playerName != null && !playerName.equals("null")) {
                    if (playerName.equals("#Storage place for user information")) {
                        continue;
                    }
                    if (character.length > 1) {
                        mining = character[1];
                    }
                    if (character.length > 3) {
                        party = character[3];
                    }
                    if (character.length > 4) {
                        miningXP = character[4];
                    }
                    if (character.length > 5) {
                        woodcutting = character[5];
                    }
                    if (character.length > 6) {
                        woodCuttingXP = character[6];
                    }
                    if (character.length > 7) {
                        repair = character[7];
                    }
                    if (character.length > 8) {
                        unarmed = character[8];
                    }
                    if (character.length > 9) {
                        herbalism = character[9];
                    }
                    if (character.length > 10) {
                        excavation = character[10];
                    }
                    if (character.length > 11) {
                        archery = character[11];
                    }
                    if (character.length > 12) {
                        swords = character[12];
                    }
                    if (character.length > 13) {
                        axes = character[13];
                    }
                    if (character.length > 14) {
                        acrobatics = character[14];
                    }
                    if (character.length > 15) {
                        repairXP = character[15];
                    }
                    if (character.length > 16) {
                        unarmedXP = character[16];
                    }
                    if (character.length > 17) {
                        herbalismXP = character[17];
                    }
                    if (character.length > 18) {
                        excavationXP = character[18];
                    }
                    if (character.length > 19) {
                        archeryXP = character[19];
                    }
                    if (character.length > 20) {
                        swordsXP = character[20];
                    }
                    if (character.length > 21) {
                        axesXP = character[21];
                    }
                    if (character.length > 22) {
                        acrobaticsXP = character[22];
                    }
                    if (character.length > 24) {
                        taming = character[24];
                    }
                    if (character.length > 25) {
                        tamingXP = character[25];
                    }
                    id = nfcMMO.database.GetInt("SELECT id FROM " + Config.MySQLtablePrefix + "users WHERE user = '" + playerName + "'");
                    if (id > 0) {
                        ++theCount;
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "users SET lastlogin = " + 0 + " WHERE id = " + id);
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "skills SET " + "  taming = taming+" + getInt(taming) + ", mining = mining+" + getInt(mining) + ", repair = repair+" + getInt(repair) + ", woodcutting = woodcutting+" + getInt(woodcutting) + ", unarmed = unarmed+" + getInt(unarmed) + ", herbalism = herbalism+" + getInt(herbalism) + ", excavation = excavation+" + getInt(excavation) + ", archery = archery+" + getInt(archery) + ", swords = swords+" + getInt(swords) + ", axes = axes+" + getInt(axes) + ", acrobatics = acrobatics+" + getInt(acrobatics) + " WHERE user_id = " + id);
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "experience SET " + "  taming = " + getInt(tamingXP) + ", mining = " + getInt(miningXP) + ", repair = " + getInt(repairXP) + ", woodcutting = " + getInt(woodCuttingXP) + ", unarmed = " + getInt(unarmedXP) + ", herbalism = " + getInt(herbalismXP) + ", excavation = " + getInt(excavationXP) + ", archery = " + getInt(archeryXP) + ", swords = " + getInt(swordsXP) + ", axes = " + getInt(axesXP) + ", acrobatics = " + getInt(acrobaticsXP) + " WHERE user_id = " + id);
                    }
                    else {
                        ++theCount;
                        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "users (user, lastlogin) VALUES ('" + playerName + "'," + System.currentTimeMillis() / 1000L + ")");
                        id = nfcMMO.database.GetInt("SELECT id FROM " + Config.MySQLtablePrefix + "users WHERE user = '" + playerName + "'");
                        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "spawn (user_id) VALUES (" + id + ")");
                        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "skills (user_id) VALUES (" + id + ")");
                        nfcMMO.database.Write("INSERT INTO " + Config.MySQLtablePrefix + "experience (user_id) VALUES (" + id + ")");
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "users SET lastlogin = " + 0 + " WHERE id = " + id);
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "users SET party = '" + party + "' WHERE id = " + id);
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "skills SET " + "  taming = " + getInt(taming) + ", mining = " + getInt(mining) + ", repair = " + getInt(repair) + ", woodcutting = " + getInt(woodcutting) + ", unarmed = " + getInt(unarmed) + ", herbalism = " + getInt(herbalism) + ", excavation = " + getInt(excavation) + ", archery = " + getInt(archery) + ", swords = " + getInt(swords) + ", axes = " + getInt(axes) + ", acrobatics = " + getInt(acrobatics) + " WHERE user_id = " + id);
                        nfcMMO.database.Write("UPDATE " + Config.MySQLtablePrefix + "experience SET " + "  taming = " + getInt(tamingXP) + ", mining = " + getInt(miningXP) + ", repair = " + getInt(repairXP) + ", woodcutting = " + getInt(woodCuttingXP) + ", unarmed = " + getInt(unarmedXP) + ", herbalism = " + getInt(herbalismXP) + ", excavation = " + getInt(excavationXP) + ", archery = " + getInt(archeryXP) + ", swords = " + getInt(swordsXP) + ", axes = " + getInt(axesXP) + ", acrobatics = " + getInt(acrobaticsXP) + " WHERE user_id = " + id);
                    }
                }
            }
            System.out.println("[nfcMMO] MySQL Updated from users file, " + theCount + " items added/updated to MySQL DB");
            in.close();
        }
        catch (Exception e) {
            m.log.log(Level.SEVERE, "Exception while reading " + location + " (Are you sure you formatted it correctly?)", e);
        }
    }
}
