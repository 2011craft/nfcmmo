// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.command;

import org.bukkit.Location;
import java.util.HashMap;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.datatypes.HUDmmo;
import org.nfcmmo.datatypes.HUDType;
import org.nfcmmo.spout.SpoutStuff;
import java.util.logging.Level;
import org.nfcmmo.party.Party;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import org.nfcmmo.nfcMMO;
import org.nfcmmo.skills.Skills;
import org.nfcmmo.Leaderboard;
import org.nfcmmo.m;
import org.bukkit.Material;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.bukkit.ChatColor;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.Users;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.Bukkit;
import org.nfcmmo.config.Config;

import java.util.logging.Logger;

public class Commands
{
    public static final Logger log;
    public static boolean xpevent;
    static int oldrate;
    
    static {
        log = Logger.getLogger("Minecraft");
        Commands.xpevent = false;
        Commands.oldrate = Config.xpGainMultiplier;
    }
    
    public static boolean isPlayer(final String playerName) {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player herp = onlinePlayers[i];
            if (herp.getName().toLowerCase().equals(playerName.toLowerCase())) {
                return true;
            }
        }
        return false;
    }
    
    public static Player getPlayer(final String playerName) {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player herp = onlinePlayers[i];
            if (herp.getName().toLowerCase().equals(playerName.toLowerCase())) {
                return herp;
            }
        }
        return null;
    }
    
    public static boolean processCommands(final CommandSender sender, final Command command, final String label, final String[] args) {
        Player player = null;
        PlayerProfile PP = null;
        if (sender instanceof Player) {
            player = (Player)sender;
            PP = Users.getProfile(player);
        }
        final String[] split = new String[args.length + 1];
        split[0] = label;
        for (int a = 0; a < args.length; ++a) {
            split[a + 1] = args[a];
        }
        if (label.equalsIgnoreCase("taming") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillTaming").toLowerCase())) {
            final float skillvalue = PP.getSkillLevel(SkillType.TAMING);
            final String percentage = String.valueOf(skillvalue / 1000.0f * 100.0f);
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillTaming") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainTaming") }));
            if (mcPermissions.getInstance().taming(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.TAMING), PP.getSkillXpLevel(SkillType.TAMING), PP.getXpToLevel(SkillType.TAMING) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming1_0"), mcLocale.getString("m.EffectsTaming1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming2_0"), mcLocale.getString("m.EffectsTaming2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming3_0"), mcLocale.getString("m.EffectsTaming3_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming4_0"), mcLocale.getString("m.EffectsTaming4_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming5_0"), mcLocale.getString("m.EffectsTaming5_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsTaming6_0"), mcLocale.getString("m.EffectsTaming6_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            if (PP.getSkillLevel(SkillType.TAMING) < 100) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockTaming1") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusTaming1_0"), mcLocale.getString("m.AbilBonusTaming1_1") }));
            }
            if (PP.getSkillLevel(SkillType.TAMING) < 250) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockTaming2") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusTaming2_0"), mcLocale.getString("m.AbilBonusTaming2_1") }));
            }
            if (PP.getSkillLevel(SkillType.TAMING) < 500) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockTaming3") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusTaming3_0"), mcLocale.getString("m.AbilBonusTaming3_1") }));
            }
            if (PP.getSkillLevel(SkillType.TAMING) < 750) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockTaming4") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusTaming4_0"), mcLocale.getString("m.AbilBonusTaming4_1") }));
            }
            player.sendMessage(mcLocale.getString("m.TamingGoreChance", new Object[] { percentage }));
        }
        else if (label.equalsIgnoreCase("woodcutting") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillWoodCutting").toLowerCase())) {
            final float skillvalue = PP.getSkillLevel(SkillType.WOODCUTTING);
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.WOODCUTTING); x >= 50; x -= 50, ++ticks) {}
            final String percentage2 = String.valueOf(skillvalue / 1000.0f * 100.0f);
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillWoodCutting") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainWoodCutting") }));
            if (mcPermissions.getInstance().woodcutting(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.WOODCUTTING), PP.getSkillXpLevel(SkillType.WOODCUTTING), PP.getXpToLevel(SkillType.WOODCUTTING) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsWoodCutting1_0"), mcLocale.getString("m.EffectsWoodCutting1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsWoodCutting2_0"), mcLocale.getString("m.EffectsWoodCutting2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsWoodCutting3_0"), mcLocale.getString("m.EffectsWoodCutting3_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            if (PP.getSkillLevel(SkillType.WOODCUTTING) < 100) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockWoodCutting1") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusWoodCutting1_0"), mcLocale.getString("m.AbilBonusWoodCutting1_1") }));
            }
            player.sendMessage(mcLocale.getString("m.WoodCuttingDoubleDropChance", new Object[] { percentage2 }));
            player.sendMessage(mcLocale.getString("m.WoodCuttingTreeFellerLength", new Object[] { ticks }));
        }
        else if (label.equalsIgnoreCase("archery") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillArchery").toLowerCase())) {
            final float skillvalue = PP.getSkillLevel(SkillType.ARCHERY);
            final String percentage = String.valueOf(skillvalue / 1000.0f * 100.0f);
            int ignition = 20;
            if (PP.getSkillLevel(SkillType.ARCHERY) >= 200) {
                ignition += 20;
            }
            if (PP.getSkillLevel(SkillType.ARCHERY) >= 400) {
                ignition += 20;
            }
            if (PP.getSkillLevel(SkillType.ARCHERY) >= 600) {
                ignition += 20;
            }
            if (PP.getSkillLevel(SkillType.ARCHERY) >= 800) {
                ignition += 20;
            }
            if (PP.getSkillLevel(SkillType.ARCHERY) >= 1000) {
                ignition += 20;
            }
            String percentagedaze;
            if (PP.getSkillLevel(SkillType.ARCHERY) < 1000) {
                percentagedaze = String.valueOf(skillvalue / 2000.0f * 100.0f);
            }
            else {
                percentagedaze = "50";
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillArchery") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainArchery") }));
            if (mcPermissions.getInstance().archery(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.ARCHERY), PP.getSkillXpLevel(SkillType.ARCHERY), PP.getXpToLevel(SkillType.ARCHERY) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsArchery1_0"), mcLocale.getString("m.EffectsArchery1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsArchery2_0"), mcLocale.getString("m.EffectsArchery2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsArchery4_0"), mcLocale.getString("m.EffectsArchery4_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.ArcheryDazeChance", new Object[] { percentagedaze }));
            player.sendMessage(mcLocale.getString("m.ArcheryRetrieveChance", new Object[] { percentage }));
            player.sendMessage(mcLocale.getString("m.ArcheryIgnitionLength", new Object[] { ignition / 20 }));
        }
        else if (label.equalsIgnoreCase("axes") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillAxes"))) {
            final float skillvalue2 = PP.getSkillLevel(SkillType.AXES);
            String percentage3;
            if (PP.getSkillLevel(SkillType.AXES) < 750) {
                percentage3 = String.valueOf(skillvalue2 / 1000.0f * 100.0f);
            }
            else {
                percentage3 = "75";
            }
            int ticks2 = 2;
            for (int x2 = PP.getSkillLevel(SkillType.AXES); x2 >= 50; x2 -= 50, ++ticks2) {}
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillAxes") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainAxes") }));
            if (mcPermissions.getInstance().axes(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.AXES), PP.getSkillXpLevel(SkillType.AXES), PP.getXpToLevel(SkillType.AXES) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAxes1_0"), mcLocale.getString("m.EffectsAxes1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAxes2_0"), mcLocale.getString("m.EffectsAxes2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAxes3_0"), mcLocale.getString("m.EffectsAxes3_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.AxesCritChance", new Object[] { percentage3 }));
            if (PP.getSkillLevel(SkillType.AXES) < 500) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockAxes1") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusAxes1_0"), mcLocale.getString("m.AbilBonusAxes1_1") }));
            }
            player.sendMessage(mcLocale.getString("m.AxesSkullLength", new Object[] { ticks2 }));
        }
        else if (label.equalsIgnoreCase("swords") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillSwords").toLowerCase())) {
            int bleedrank = 2;
            final float skillvalue3 = PP.getSkillLevel(SkillType.SWORDS);
            String percentage;
            if (PP.getSkillLevel(SkillType.SWORDS) < 750) {
                percentage = String.valueOf(skillvalue3 / 1000.0f * 100.0f);
            }
            else {
                percentage = "75";
            }
            if (skillvalue3 >= 750.0f) {
                ++bleedrank;
            }
            String counterattackpercentage;
            if (PP.getSkillLevel(SkillType.SWORDS) <= 600) {
                counterattackpercentage = String.valueOf(skillvalue3 / 2000.0f * 100.0f);
            }
            else {
                counterattackpercentage = "30";
            }
            int ticks3 = 2;
            for (int x3 = PP.getSkillLevel(SkillType.SWORDS); x3 >= 50; x3 -= 50, ++ticks3) {}
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillSwords") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainSwords") }));
            if (mcPermissions.getInstance().swords(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.SWORDS), PP.getSkillXpLevel(SkillType.SWORDS), PP.getXpToLevel(SkillType.SWORDS) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsSwords1_0"), mcLocale.getString("m.EffectsSwords1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsSwords2_0"), mcLocale.getString("m.EffectsSwords2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsSwords3_0"), mcLocale.getString("m.EffectsSwords3_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsSwords5_0"), mcLocale.getString("m.EffectsSwords5_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.SwordsCounterAttChance", new Object[] { counterattackpercentage }));
            player.sendMessage(mcLocale.getString("m.SwordsBleedLength", new Object[] { bleedrank }));
            player.sendMessage(mcLocale.getString("m.SwordsTickNote"));
            player.sendMessage(mcLocale.getString("m.SwordsBleedLength", new Object[] { percentage }));
            player.sendMessage(mcLocale.getString("m.SwordsSSLength", new Object[] { ticks3 }));
        }
        else if (label.equalsIgnoreCase("acrobatics") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillAcrobatics").toLowerCase())) {
            final float skillvalue2 = PP.getSkillLevel(SkillType.ACROBATICS);
            final String percentage4 = String.valueOf(skillvalue2 / 1000.0f * 100.0f);
            final String gracepercentage = String.valueOf(skillvalue2 / 1000.0f * 100.0f * 2.0f);
            String dodgepercentage;
            if (PP.getSkillLevel(SkillType.ACROBATICS) <= 800) {
                dodgepercentage = String.valueOf(skillvalue2 / 4000.0f * 100.0f);
            }
            else {
                dodgepercentage = "20";
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillAcrobatics") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainAcrobatics") }));
            if (mcPermissions.getInstance().acrobatics(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.ACROBATICS), PP.getSkillXpLevel(SkillType.ACROBATICS), PP.getXpToLevel(SkillType.ACROBATICS) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAcrobatics1_0"), mcLocale.getString("m.EffectsAcrobatics1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAcrobatics2_0"), mcLocale.getString("m.EffectsAcrobatics2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsAcrobatics3_0"), mcLocale.getString("m.EffectsAcrobatics3_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.AcrobaticsRollChance", new Object[] { percentage4 }));
            player.sendMessage(mcLocale.getString("m.AcrobaticsGracefulRollChance", new Object[] { gracepercentage }));
            player.sendMessage(mcLocale.getString("m.AcrobaticsDodgeChance", new Object[] { dodgepercentage }));
        }
        else if (label.equalsIgnoreCase("mining") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillMining"))) {
            final float skillvalue = PP.getSkillLevel(SkillType.MINING);
            final String percentage = String.valueOf(skillvalue / 1000.0f * 100.0f);
            int ticks2 = 2;
            for (int x2 = PP.getSkillLevel(SkillType.MINING); x2 >= 50; x2 -= 50, ++ticks2) {}
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillMining") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainMining") }));
            if (mcPermissions.getInstance().mining(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.MINING), PP.getSkillXpLevel(SkillType.MINING), PP.getXpToLevel(SkillType.MINING) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsMining1_0"), mcLocale.getString("m.EffectsMining1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsMining2_0"), mcLocale.getString("m.EffectsMining2_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.MiningDoubleDropChance", new Object[] { percentage }));
            player.sendMessage(mcLocale.getString("m.MiningSuperBreakerLength", new Object[] { ticks2 }));
        }
        else if (label.equalsIgnoreCase("repair") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillRepair").toLowerCase())) {
            final float skillvalue = PP.getSkillLevel(SkillType.REPAIR);
            final String percentage = String.valueOf(skillvalue / 1000.0f * 100.0f);
            final String repairmastery = String.valueOf(skillvalue / 500.0f * 100.0f);
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillRepair") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainRepair") }));
            if (mcPermissions.getInstance().repair(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.REPAIR), PP.getSkillXpLevel(SkillType.REPAIR), PP.getXpToLevel(SkillType.REPAIR) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsRepair1_0"), mcLocale.getString("m.EffectsRepair1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsRepair2_0"), mcLocale.getString("m.EffectsRepair2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsRepair3_0"), mcLocale.getString("m.EffectsRepair3_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsRepair4_0", new Object[] { Config.repairdiamondlevel }), mcLocale.getString("m.EffectsRepair4_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.RepairRepairMastery", new Object[] { repairmastery }));
            player.sendMessage(mcLocale.getString("m.RepairSuperRepairChance", new Object[] { percentage }));
        }
        else if (label.equalsIgnoreCase("unarmed")) {
            final float skillvalue4 = PP.getSkillLevel(SkillType.UNARMED);
            String percentage3;
            if (PP.getSkillLevel(SkillType.UNARMED) < 1000) {
                percentage3 = String.valueOf(skillvalue4 / 4000.0f * 100.0f);
            }
            else {
                percentage3 = "25";
            }
            String arrowpercentage;
            if (PP.getSkillLevel(SkillType.UNARMED) < 1000) {
                arrowpercentage = String.valueOf(skillvalue4 / 1000.0f * 100.0f / 2.0f);
            }
            else {
                arrowpercentage = "50";
            }
            int ticks4 = 2;
            for (int x4 = PP.getSkillLevel(SkillType.UNARMED); x4 >= 50; x4 -= 50, ++ticks4) {}
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillUnarmed") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainUnarmed") }));
            if (mcPermissions.getInstance().unarmed(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.UNARMED), PP.getSkillXpLevel(SkillType.UNARMED), PP.getXpToLevel(SkillType.UNARMED) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsUnarmed1_0"), mcLocale.getString("m.EffectsUnarmed1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsUnarmed2_0"), mcLocale.getString("m.EffectsUnarmed2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsUnarmed3_0"), mcLocale.getString("m.EffectsUnarmed3_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsUnarmed4_0"), mcLocale.getString("m.EffectsUnarmed4_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsUnarmed5_0"), mcLocale.getString("m.EffectsUnarmed5_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.UnarmedArrowDeflectChance", new Object[] { arrowpercentage }));
            player.sendMessage(mcLocale.getString("m.UnarmedDisarmChance", new Object[] { percentage3 }));
            if (PP.getSkillLevel(SkillType.UNARMED) < 250) {
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockUnarmed1") }));
            }
            else if (PP.getSkillLevel(SkillType.UNARMED) >= 250 && PP.getSkillLevel(SkillType.UNARMED) < 500) {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusUnarmed1_0"), mcLocale.getString("m.AbilBonusUnarmed1_1") }));
                player.sendMessage(mcLocale.getString("m.AbilityLockTemplate", new Object[] { mcLocale.getString("m.AbilLockUnarmed2") }));
            }
            else {
                player.sendMessage(mcLocale.getString("m.AbilityBonusTemplate", new Object[] { mcLocale.getString("m.AbilBonusUnarmed2_0"), mcLocale.getString("m.AbilBonusUnarmed2_1") }));
            }
            player.sendMessage(mcLocale.getString("m.UnarmedBerserkLength", new Object[] { ticks4 }));
        }
        else if (label.equalsIgnoreCase("herbalism") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillHerbalism").toLowerCase())) {
            int bonus = 0;
            if (PP.getSkillLevel(SkillType.HERBALISM) >= 200) {
                ++bonus;
            }
            if (PP.getSkillLevel(SkillType.HERBALISM) >= 400) {
                ++bonus;
            }
            if (PP.getSkillLevel(SkillType.HERBALISM) >= 600) {
                ++bonus;
            }
            int ticks = 2;
            for (int x = PP.getSkillLevel(SkillType.HERBALISM); x >= 50; x -= 50, ++ticks) {}
            final float skillvalue3 = PP.getSkillLevel(SkillType.HERBALISM);
            final String percentage5 = String.valueOf(skillvalue3 / 1000.0f * 100.0f);
            final String gpercentage = String.valueOf(skillvalue3 / 1500.0f * 100.0f);
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillHerbalism") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainHerbalism") }));
            if (mcPermissions.getInstance().herbalism(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.HERBALISM), PP.getSkillXpLevel(SkillType.HERBALISM), PP.getXpToLevel(SkillType.HERBALISM) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsHerbalism1_0"), mcLocale.getString("m.EffectsHerbalism1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsHerbalism2_0"), mcLocale.getString("m.EffectsHerbalism2_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsHerbalism3_0"), mcLocale.getString("m.EffectsHerbalism3_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsHerbalism5_0"), mcLocale.getString("m.EffectsHerbalism5_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.HerbalismGreenTerraLength", new Object[] { ticks }));
            player.sendMessage(mcLocale.getString("m.HerbalismGreenThumbChance", new Object[] { gpercentage }));
            player.sendMessage(mcLocale.getString("m.HerbalismGreenThumbStage", new Object[] { bonus }));
            player.sendMessage(mcLocale.getString("m.HerbalismDoubleDropChance", new Object[] { percentage5 }));
        }
        else if (label.equalsIgnoreCase("excavation") || split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillExcavation").toLowerCase())) {
            int ticks5 = 2;
            for (int x5 = PP.getSkillLevel(SkillType.EXCAVATION); x5 >= 50; x5 -= 50, ++ticks5) {}
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.SkillExcavation") }));
            player.sendMessage(mcLocale.getString("m.XPGain", new Object[] { mcLocale.getString("m.XPGainExcavation") }));
            if (mcPermissions.getInstance().excavation(player)) {
                player.sendMessage(mcLocale.getString("m.LVL", new Object[] { PP.getSkillLevel(SkillType.EXCAVATION), PP.getSkillXpLevel(SkillType.EXCAVATION), PP.getXpToLevel(SkillType.EXCAVATION) }));
            }
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.Effects") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsExcavation1_0"), mcLocale.getString("m.EffectsExcavation1_1") }));
            player.sendMessage(mcLocale.getString("m.EffectsTemplate", new Object[] { mcLocale.getString("m.EffectsExcavation2_0"), mcLocale.getString("m.EffectsExcavation2_1") }));
            player.sendMessage(mcLocale.getString("m.SkillHeader", new Object[] { mcLocale.getString("m.YourStats") }));
            player.sendMessage(mcLocale.getString("m.ExcavationGreenTerraLength", new Object[] { ticks5 }));
        }
        else if (!label.equalsIgnoreCase("sorcery") && !split[0].toLowerCase().equalsIgnoreCase(mcLocale.getString("m.SkillSorcery").toLowerCase())) {
            if (Config.nfcmmoEnable && label.equalsIgnoreCase(Config.nfcmmo)) {
                player.sendMessage(ChatColor.RED + "-----[]" + ChatColor.GREEN + "nfcMMO" + ChatColor.RED + "[]-----");
                final String description = mcLocale.getString("nfcMMO.Description", new Object[] { Config.nfcc });
                final String[] mcSplit = description.split(",");
                String[] array;
                for (int length = (array = mcSplit).length, n8 = 0; n8 < length; ++n8) {
                    final String x6 = array[n8];
                    player.sendMessage(x6);
                }
                if (Config.spoutEnabled && player instanceof SpoutPlayer) {
                    final SpoutPlayer sPlayer = (SpoutPlayer)player;
                    if (Config.donateMessage) {
                        sPlayer.sendNotification("[nfcMMO] Donate!", "Paypal nossr50@gmail.com", Material.CAKE);
                    }
                }
                else if (Config.donateMessage) {
                    player.sendMessage(ChatColor.GREEN + "If you like my work you can donate via Paypal: nossr50@gmail.com");
                }
            }
            else if (Config.nfccEnable && label.equalsIgnoreCase(Config.nfcc)) {
                player.sendMessage(ChatColor.RED + "---[]" + ChatColor.YELLOW + "nfcMMO Commands" + ChatColor.RED + "[]---");
                if (mcPermissions.getInstance().party(player)) {
                    player.sendMessage(mcLocale.getString("m.nfccPartyCommands"));
                    player.sendMessage(String.valueOf(Config.party) + " " + mcLocale.getString("m.nfccParty"));
                    player.sendMessage(String.valueOf(Config.party) + " q " + mcLocale.getString("m.nfccPartyQ"));
                    if (mcPermissions.getInstance().partyChat(player)) {
                        player.sendMessage("/p " + mcLocale.getString("m.nfccPartyToggle"));
                    }
                    player.sendMessage(String.valueOf(Config.invite) + " " + mcLocale.getString("m.nfccPartyInvite"));
                    player.sendMessage(String.valueOf(Config.accept) + " " + mcLocale.getString("m.nfccPartyAccept"));
                    if (mcPermissions.getInstance().partyTeleport(player)) {
                        player.sendMessage(String.valueOf(Config.ptp) + " " + mcLocale.getString("m.nfccPartyTeleport"));
                    }
                }
                player.sendMessage(mcLocale.getString("m.nfccOtherCommands"));
                player.sendMessage(String.valueOf(Config.stats) + ChatColor.RED + " " + mcLocale.getString("m.nfccStats"));
                player.sendMessage("/nfctop <skillname> <page> " + ChatColor.RED + mcLocale.getString("m.nfccLeaderboards"));
                if (mcPermissions.getInstance().mySpawn(player)) {
                    player.sendMessage(String.valueOf(Config.myspawn) + " " + ChatColor.RED + mcLocale.getString("m.nfccMySpawn"));
                    player.sendMessage(String.valueOf(Config.clearmyspawn) + " " + ChatColor.RED + mcLocale.getString("m.nfccClearMySpawn"));
                }
                if (mcPermissions.getInstance().mcAbility(player)) {
                    player.sendMessage(String.valueOf(Config.nfcability) + ChatColor.RED + " " + mcLocale.getString("m.nfccToggleAbility"));
                }
                if (mcPermissions.getInstance().adminChat(player)) {
                    player.sendMessage("/a " + ChatColor.RED + mcLocale.getString("m.nfccAdminToggle"));
                }
                if (mcPermissions.getInstance().whois(player)) {
                    player.sendMessage(String.valueOf(Config.whois) + " " + mcLocale.getString("m.nfccWhois"));
                }
                if (mcPermissions.getInstance().mmoedit(player)) {
                    player.sendMessage(String.valueOf(Config.mmoedit) + mcLocale.getString("m.nfccMmoedit"));
                }
                if (mcPermissions.getInstance().nfcgod(player)) {
                    player.sendMessage(String.valueOf(Config.nfcgod) + ChatColor.RED + " " + mcLocale.getString("m.nfccMcGod"));
                }
                player.sendMessage(mcLocale.getString("m.nfccSkillInfo"));
                player.sendMessage(String.valueOf(Config.nfcmmo) + " " + mcLocale.getString("m.nfccModDescription"));
            }
            else if (Config.nfcabilityEnable && mcPermissions.permissionsEnabled && label.equalsIgnoreCase(Config.nfcability)) {
                if (PP.getAbilityUse()) {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.AbilitiesOff"));
                    PP.toggleAbilityUse();
                }
                else {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.AbilitiesOn"));
                    PP.toggleAbilityUse();
                }
            }
            else if (Config.xprateEnable && label.equalsIgnoreCase(Config.xprate)) {
                if (sender instanceof Player) {
                    if (!mcPermissions.getInstance().admin(player)) {
                        player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                        return true;
                    }
                    if (split.length <= 1) {
                        player.sendMessage(mcLocale.getString("Commands.xprate.proper", new Object[] { Config.xprate }));
                        player.sendMessage(mcLocale.getString("Commands.xprate.proper2", new Object[] { Config.xprate }));
                    }
                    if (split.length == 2 && split[1].equalsIgnoreCase("reset")) {
                        if (Commands.xpevent) {
                            Player[] onlinePlayers;
                            for (int length2 = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, n9 = 0; n9 < length2; ++n9) {
                                final Player x7 = onlinePlayers[n9];
                                x7.sendMessage(mcLocale.getString("Commands.xprate.over"));
                            }
                            Commands.xpevent = !Commands.xpevent;
                            Config.xpGainMultiplier = Commands.oldrate;
                        }
                        else {
                            Config.xpGainMultiplier = Commands.oldrate;
                        }
                    }
                    if (split.length >= 2 && m.isInt(split[1])) {
                        Commands.oldrate = Config.xpGainMultiplier;
                        if (split.length < 3 || (!split[2].equalsIgnoreCase("true") && !split[2].equalsIgnoreCase("false"))) {
                            player.sendMessage(mcLocale.getString("Commands.xprate.proper3"));
                            return true;
                        }
                        if (split[2].equalsIgnoreCase("true")) {
                            Commands.xpevent = true;
                        }
                        else {
                            Commands.xpevent = false;
                        }
                        Config.xpGainMultiplier = m.getInt(split[1]);
                        if (Commands.xpevent = true) {
                            Player[] onlinePlayers2;
                            for (int length3 = (onlinePlayers2 = Bukkit.getServer().getOnlinePlayers()).length, n10 = 0; n10 < length3; ++n10) {
                                final Player x7 = onlinePlayers2[n10];
                                x7.sendMessage(mcLocale.getString("Commands.xprate.started"));
                                x7.sendMessage(mcLocale.getString("Commands.xprate.started2", new Object[] { Config.xpGainMultiplier }));
                            }
                        }
                    }
                }
                else {
                    if (split.length <= 1) {
                        System.out.println(mcLocale.getString("Commands.xprate.proper", new Object[] { Config.xprate }));
                        System.out.println(mcLocale.getString("Commands.xprate.proper2", new Object[] { Config.xprate }));
                    }
                    if (split.length == 2 && split[1].equalsIgnoreCase("reset")) {
                        if (Commands.xpevent) {
                            Player[] onlinePlayers3;
                            for (int length4 = (onlinePlayers3 = Bukkit.getServer().getOnlinePlayers()).length, n11 = 0; n11 < length4; ++n11) {
                                final Player x7 = onlinePlayers3[n11];
                                x7.sendMessage(mcLocale.getString("Commands.xprate.over"));
                            }
                            Commands.xpevent = !Commands.xpevent;
                            Config.xpGainMultiplier = Commands.oldrate;
                        }
                        else {
                            Config.xpGainMultiplier = Commands.oldrate;
                        }
                    }
                    if (split.length >= 2 && m.isInt(split[1])) {
                        Commands.oldrate = Config.xpGainMultiplier;
                        if (split.length < 3 || (!split[2].equalsIgnoreCase("true") && !split[2].equalsIgnoreCase("false"))) {
                            System.out.println(mcLocale.getString("Commands.xprate.proper3"));
                            return true;
                        }
                        if (split[2].equalsIgnoreCase("true")) {
                            Commands.xpevent = true;
                        }
                        else {
                            Commands.xpevent = false;
                        }
                        Config.xpGainMultiplier = m.getInt(split[1]);
                        if (Commands.xpevent = true) {
                            Player[] onlinePlayers4;
                            for (int length5 = (onlinePlayers4 = Bukkit.getServer().getOnlinePlayers()).length, n12 = 0; n12 < length5; ++n12) {
                                final Player x7 = onlinePlayers4[n12];
                                x7.sendMessage(ChatColor.GOLD + "XP EVENT FOR nfcMMO HAS STARTED!");
                                x7.sendMessage(ChatColor.GOLD + "nfcMMO XP RATE IS NOW " + Config.xpGainMultiplier + "x!!");
                            }
                        }
                    }
                }
            }
            else if (label.equalsIgnoreCase("mmoupdate")) {
                if (!mcPermissions.getInstance().admin(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                player.sendMessage(ChatColor.GRAY + "Starting conversion...");
                Users.clearUsers();
                m.convertToMySQL();
                Player[] onlinePlayers5;
                for (int length6 = (onlinePlayers5 = Bukkit.getServer().getOnlinePlayers()).length, n13 = 0; n13 < length6; ++n13) {
                    final Player x7 = onlinePlayers5[n13];
                    Users.addUser(x7);
                }
                player.sendMessage(ChatColor.GREEN + "Conversion finished!");
            }
            else if (Config.nfctopEnable && label.equalsIgnoreCase(Config.nfctop)) {
                if (!Config.useMySQL) {
                    if (split.length == 1) {
                        final int p = 1;
                        final String[] info = Leaderboard.retrieveInfo(SkillType.ALL.toString(), p);
                        player.sendMessage(mcLocale.getString("mcPlayerListener.PowerLevelLeaderboard"));
                        int n = 1 * p;
                        String[] array2;
                        for (int length7 = (array2 = info).length, n14 = 0; n14 < length7; ++n14) {
                            final String x8 = array2[n14];
                            if (x8 != null) {
                                String digit = String.valueOf(n);
                                if (n < 10) {
                                    digit = "0" + String.valueOf(n);
                                }
                                final String[] splitx = x8.split(":");
                                player.sendMessage(String.valueOf(digit) + ". " + ChatColor.GREEN + splitx[1] + " - " + ChatColor.WHITE + splitx[0]);
                                ++n;
                            }
                        }
                    }
                    if (split.length >= 2 && m.isInt(split[1])) {
                        int p = 1;
                        if (split.length >= 2 && m.isInt(split[1])) {
                            p = Integer.valueOf(split[1]);
                        }
                        int pt;
                        if ((pt = p) > 1) {
                            pt = --pt + pt * 10;
                            pt = 10;
                        }
                        final String[] info2 = Leaderboard.retrieveInfo(SkillType.ALL.toString(), p);
                        player.sendMessage(mcLocale.getString("mcPlayerListener.PowerLevelLeaderboard"));
                        int n2 = 1 * pt;
                        String[] array3;
                        for (int length8 = (array3 = info2).length, n15 = 0; n15 < length8; ++n15) {
                            final String x9 = array3[n15];
                            if (x9 != null) {
                                String digit2 = String.valueOf(n2);
                                if (n2 < 10) {
                                    digit2 = "0" + String.valueOf(n2);
                                }
                                final String[] splitx2 = x9.split(":");
                                player.sendMessage(String.valueOf(digit2) + ". " + ChatColor.GREEN + splitx2[1] + " - " + ChatColor.WHITE + splitx2[0]);
                                ++n2;
                            }
                        }
                    }
                    if (split.length >= 2 && Skills.isSkill(split[1])) {
                        int p = 1;
                        if (split.length >= 3 && m.isInt(split[2])) {
                            p = Integer.valueOf(split[2]);
                        }
                        int pt;
                        if ((pt = p) > 1) {
                            pt = --pt + pt * 10;
                            pt = 10;
                        }
                        final String firstLetter = split[1].substring(0, 1);
                        final String remainder = split[1].substring(1);
                        final String capitalized = String.valueOf(firstLetter.toUpperCase()) + remainder.toLowerCase();
                        final String[] info3 = Leaderboard.retrieveInfo(split[1].toUpperCase(), p);
                        player.sendMessage(mcLocale.getString("mcPlayerListener.SkillLeaderboard", new Object[] { capitalized }));
                        int n3 = 1 * pt;
                        String[] array4;
                        for (int length9 = (array4 = info3).length, n16 = 0; n16 < length9; ++n16) {
                            final String x10 = array4[n16];
                            if (x10 != null) {
                                String digit3 = String.valueOf(n3);
                                if (n3 < 10) {
                                    digit3 = "0" + String.valueOf(n3);
                                }
                                final String[] splitx3 = x10.split(":");
                                player.sendMessage(String.valueOf(digit3) + ". " + ChatColor.GREEN + splitx3[1] + " - " + ChatColor.WHITE + splitx3[0]);
                                ++n3;
                            }
                        }
                    }
                }
                else {
                    final String powerlevel = "taming+mining+woodcutting+repair+unarmed+herbalism+excavation+archery+swords+axes+acrobatics";
                    if (split.length >= 2 && Skills.isSkill(split[1])) {
                        final String lowercase = split[1].toLowerCase();
                        final String firstLetter = split[1].substring(0, 1);
                        final String remainder = split[1].substring(1);
                        final String capitalized = String.valueOf(firstLetter.toUpperCase()) + remainder.toLowerCase();
                        player.sendMessage(mcLocale.getString("mcPlayerListener.SkillLeaderboard", new Object[] { capitalized }));
                        if (split.length >= 3 && m.isInt(split[2])) {
                            int n4 = 1;
                            final int n5 = Integer.valueOf(split[2]);
                            if (n5 > 1) {
                                n4 = 10;
                                n4 *= n5 - 1;
                            }
                            final HashMap<Integer, ArrayList<String>> userslist = nfcMMO.database.Read("SELECT " + lowercase + ", user_id FROM " + Config.MySQLtablePrefix + "skills WHERE " + lowercase + " > 0 ORDER BY `" + Config.MySQLtablePrefix + "skills`.`" + lowercase + "` DESC ");
                            for (int i = n4; i <= n4 + 10 && i <= userslist.size() && nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist.get(i).get(1)) + "'") != null; ++i) {
                                final HashMap<Integer, ArrayList<String>> username = nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist.get(i).get(1)) + "'");
                                player.sendMessage(String.valueOf(String.valueOf(i)) + ". " + ChatColor.GREEN + userslist.get(i).get(0) + " - " + ChatColor.WHITE + username.get(1).get(0));
                            }
                            return true;
                        }
                        final HashMap<Integer, ArrayList<String>> userslist2 = nfcMMO.database.Read("SELECT " + lowercase + ", user_id FROM " + Config.MySQLtablePrefix + "skills WHERE " + lowercase + " > 0 ORDER BY `" + Config.MySQLtablePrefix + "skills`.`" + lowercase + "` DESC ");
                        for (int j = 1; j <= 10 && j <= userslist2.size() && nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist2.get(j).get(1)) + "'") != null; ++j) {
                            final HashMap<Integer, ArrayList<String>> username2 = nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist2.get(j).get(1)) + "'");
                            player.sendMessage(String.valueOf(String.valueOf(j)) + ". " + ChatColor.GREEN + userslist2.get(j).get(0) + " - " + ChatColor.WHITE + username2.get(1).get(0));
                        }
                        return true;
                    }
                    else if (split.length >= 1) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.PowerLevelLeaderboard"));
                        if (split.length >= 2 && m.isInt(split[1])) {
                            int n6 = 1;
                            final int n7 = Integer.valueOf(split[1]);
                            if (n7 > 1) {
                                n6 = 10;
                                n6 *= n7 - 1;
                            }
                            final HashMap<Integer, ArrayList<String>> userslist3 = nfcMMO.database.Read("SELECT " + powerlevel + ", user_id FROM " + Config.MySQLtablePrefix + "skills WHERE " + powerlevel + " > 0 ORDER BY taming+mining+woodcutting+repair+unarmed+herbalism+excavation+archery+swords+axes+acrobatics DESC ");
                            for (int k = n6; k <= n6 + 10 && k <= userslist3.size() && nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist3.get(k).get(1)) + "'") != null; ++k) {
                                final HashMap<Integer, ArrayList<String>> username3 = nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist3.get(k).get(1)) + "'");
                                player.sendMessage(String.valueOf(String.valueOf(k)) + ". " + ChatColor.GREEN + userslist3.get(k).get(0) + " - " + ChatColor.WHITE + username3.get(1).get(0));
                            }
                            return true;
                        }
                        final HashMap<Integer, ArrayList<String>> userslist4 = nfcMMO.database.Read("SELECT taming+mining+woodcutting+repair+unarmed+herbalism+excavation+archery+swords+axes+acrobatics, user_id FROM " + Config.MySQLtablePrefix + "skills WHERE " + powerlevel + " > 0 ORDER BY taming+mining+woodcutting+repair+unarmed+herbalism+excavation+archery+swords+axes+acrobatics DESC ");
                        for (int l = 1; l <= 10; ++l) {
                            if (l > userslist4.size()) {
                                break;
                            }
                            if (nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist4.get(l).get(1)) + "'") == null) {
                                break;
                            }
                            final HashMap<Integer, ArrayList<String>> username4 = nfcMMO.database.Read("SELECT user FROM " + Config.MySQLtablePrefix + "users WHERE id = '" + Integer.valueOf(userslist4.get(l).get(1)) + "'");
                            player.sendMessage(String.valueOf(String.valueOf(l)) + ". " + ChatColor.GREEN + userslist4.get(l).get(0) + " - " + ChatColor.WHITE + username4.get(1).get(0));
                        }
                    }
                }
            }
            else if (Config.nfcrefreshEnable && label.equalsIgnoreCase(Config.nfcrefresh)) {
                if (!mcPermissions.getInstance().nfcrefresh(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (split.length >= 2 && isPlayer(split[1])) {
                    player.sendMessage("You have refreshed " + split[1] + "'s cooldowns!");
                    player = getPlayer(split[1]);
                }
                PP = Users.getProfile(player);
                PP.setRecentlyHurt(0L);
                PP.setHoePreparationMode(false);
                PP.setAxePreparationMode(false);
                PP.setFistsPreparationMode(false);
                PP.setSwordsPreparationMode(false);
                PP.setPickaxePreparationMode(false);
                PP.setGreenTerraMode(false);
                PP.setGreenTerraDeactivatedTimeStamp(0L);
                PP.setGigaDrillBreakerMode(false);
                PP.setGigaDrillBreakerDeactivatedTimeStamp(0L);
                PP.setSerratedStrikesMode(false);
                PP.setSerratedStrikesDeactivatedTimeStamp(0L);
                PP.setSuperBreakerMode(false);
                PP.setSuperBreakerDeactivatedTimeStamp(0L);
                PP.setTreeFellerMode(false);
                PP.setTreeFellerDeactivatedTimeStamp(0L);
                PP.setBerserkMode(false);
                PP.setBerserkDeactivatedTimeStamp(0L);
                player.sendMessage(mcLocale.getString("mcPlayerListener.AbilitiesRefreshed"));
            }
            else if (Config.nfcgodEnable && label.equalsIgnoreCase(Config.nfcgod)) {
                if (mcPermissions.permissionsEnabled) {
                    if (!mcPermissions.getInstance().nfcgod(player)) {
                        player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                        return true;
                    }
                    if (PP.getGodMode()) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.GodModeDisabled"));
                        PP.toggleGodMode();
                    }
                    else {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.GodModeEnabled"));
                        PP.toggleGodMode();
                    }
                }
            }
            else if (Config.clearmyspawnEnable && Config.enableMySpawn && label.equalsIgnoreCase(Config.clearmyspawn) && mcPermissions.getInstance().mySpawn(player)) {
                final double x11 = Bukkit.getServer().getWorlds().get(0).getSpawnLocation().getX();
                final double y = Bukkit.getServer().getWorlds().get(0).getSpawnLocation().getY();
                final double z = Bukkit.getServer().getWorlds().get(0).getSpawnLocation().getZ();
                final String worldname = Bukkit.getServer().getWorlds().get(0).getName();
                PP.setMySpawn(x11, y, z, worldname);
                player.sendMessage(mcLocale.getString("mcPlayerListener.MyspawnCleared"));
            }
            else if (Config.mmoeditEnable && mcPermissions.permissionsEnabled && label.equalsIgnoreCase(new StringBuilder().append(Config.mmoedit).toString())) {
                if (!mcPermissions.getInstance().mmoedit(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (split.length < 3) {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.mmoedit + " playername skillname newvalue");
                    return true;
                }
                if (split.length == 4) {
                    if (isPlayer(split[1]) && m.isInt(split[3]) && Skills.isSkill(split[2])) {
                        final int newvalue = Integer.valueOf(split[3]);
                        Users.getProfile(getPlayer(split[1])).modifyskill(Skills.getSkillType(split[2]), newvalue);
                        player.sendMessage(ChatColor.RED + split[2] + " has been modified.");
                    }
                }
                else if (split.length == 3) {
                    if (m.isInt(split[2]) && Skills.isSkill(split[1])) {
                        final int newvalue = Integer.valueOf(split[2]);
                        PP.modifyskill(Skills.getSkillType(split[1]), newvalue);
                        player.sendMessage(ChatColor.RED + split[1] + " has been modified.");
                    }
                }
                else {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.mmoedit + " playername skillname newvalue");
                }
            }
            else if (Config.addxpEnable && mcPermissions.permissionsEnabled && label.equalsIgnoreCase(Config.addxp)) {
                if (!mcPermissions.getInstance().mmoedit(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (split.length < 3) {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.addxp + " playername skillname xp");
                    return true;
                }
                if (split.length == 4) {
                    if (isPlayer(split[1]) && m.isInt(split[3]) && Skills.isSkill(split[2])) {
                        final int newvalue = Integer.valueOf(split[3]);
                        Users.getProfile(getPlayer(split[1])).addXP(Skills.getSkillType(split[2]), newvalue);
                        getPlayer(split[1]).sendMessage(ChatColor.GREEN + "Experience granted!");
                        player.sendMessage(ChatColor.RED + split[2] + " has been modified.");
                        Skills.XpCheckAll(getPlayer(split[1]));
                    }
                }
                else if (split.length == 3 && m.isInt(split[2]) && Skills.isSkill(split[1])) {
                    final int newvalue = Integer.valueOf(split[2]);
                    Users.getProfile(player).addXP(Skills.getSkillType(split[1]), newvalue);
                    player.sendMessage(ChatColor.RED + split[1] + " has been modified.");
                }
                else {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.addxp + " playername skillname xp");
                }
            }
            else if (Config.ptpEnable && label.equalsIgnoreCase(Config.ptp) && PP.inParty()) {
                if (!mcPermissions.getInstance().partyTeleport(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (split.length < 2) {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.ptp + " <playername>");
                    return true;
                }
                if (!isPlayer(split[1])) {
                    player.sendMessage("That is not a valid player");
                }
                if (isPlayer(split[1])) {
                    final Player target = getPlayer(split[1]);
                    final PlayerProfile PPt = Users.getProfile(target);
                    if (PP.getParty().equals(PPt.getParty())) {
                        player.teleport((Entity)target);
                        player.sendMessage(ChatColor.GREEN + "You have teleported to " + target.getName());
                        target.sendMessage(ChatColor.GREEN + player.getName() + " has teleported to you.");
                    }
                }
            }
            else if (Config.whoisEnable && label.equalsIgnoreCase(Config.whois) && mcPermissions.getInstance().whois(player)) {
                if (split.length < 2) {
                    player.sendMessage(ChatColor.RED + "Proper usage is /" + Config.whois + " <playername>");
                    return true;
                }
                if (isPlayer(split[1])) {
                    final Player target = getPlayer(split[1]);
                    final PlayerProfile PPt = Users.getProfile(target);
                    player.sendMessage(ChatColor.GREEN + "~~WHOIS RESULTS~~");
                    player.sendMessage(target.getName());
                    if (PPt.inParty()) {
                        player.sendMessage("Party: " + PPt.getParty());
                    }
                    player.sendMessage("Health: " + target.getHealth() + ChatColor.GRAY + " (20 is full health)");
                    player.sendMessage("OP: " + target.isOp());
                    player.sendMessage(ChatColor.GREEN + "nfcMMO Stats for " + ChatColor.YELLOW + target.getName());
                    player.sendMessage(ChatColor.GOLD + "-=GATHERING SKILLS=-");
                    if (mcPermissions.getInstance().excavation(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.ExcavationSkill"), PPt.getSkillLevel(SkillType.EXCAVATION), PPt.getSkillXpLevel(SkillType.EXCAVATION), PPt.getXpToLevel(SkillType.EXCAVATION)));
                    }
                    if (mcPermissions.getInstance().herbalism(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.HerbalismSkill"), PPt.getSkillLevel(SkillType.HERBALISM), PPt.getSkillXpLevel(SkillType.HERBALISM), PPt.getXpToLevel(SkillType.HERBALISM)));
                    }
                    if (mcPermissions.getInstance().mining(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.MiningSkill"), PPt.getSkillLevel(SkillType.MINING), PPt.getSkillXpLevel(SkillType.MINING), PPt.getXpToLevel(SkillType.MINING)));
                    }
                    if (mcPermissions.getInstance().woodcutting(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.WoodcuttingSkill"), PPt.getSkillLevel(SkillType.WOODCUTTING), PPt.getSkillXpLevel(SkillType.WOODCUTTING), PPt.getXpToLevel(SkillType.WOODCUTTING)));
                    }
                    player.sendMessage(ChatColor.GOLD + "-=COMBAT SKILLS=-");
                    if (mcPermissions.getInstance().axes(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.AxesSkill"), PPt.getSkillLevel(SkillType.AXES), PPt.getSkillXpLevel(SkillType.AXES), PPt.getXpToLevel(SkillType.AXES)));
                    }
                    if (mcPermissions.getInstance().archery(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.ArcherySkill"), PPt.getSkillLevel(SkillType.ARCHERY), PPt.getSkillXpLevel(SkillType.ARCHERY), PPt.getXpToLevel(SkillType.ARCHERY)));
                    }
                    if (mcPermissions.getInstance().swords(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.SwordsSkill"), PPt.getSkillLevel(SkillType.SWORDS), PPt.getSkillXpLevel(SkillType.SWORDS), PPt.getXpToLevel(SkillType.SWORDS)));
                    }
                    if (mcPermissions.getInstance().taming(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.TamingSkill"), PPt.getSkillLevel(SkillType.TAMING), PPt.getSkillXpLevel(SkillType.TAMING), PPt.getXpToLevel(SkillType.TAMING)));
                    }
                    if (mcPermissions.getInstance().unarmed(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.UnarmedSkill"), PPt.getSkillLevel(SkillType.UNARMED), PPt.getSkillXpLevel(SkillType.UNARMED), PPt.getXpToLevel(SkillType.UNARMED)));
                    }
                    player.sendMessage(ChatColor.GOLD + "-=MISC SKILLS=-");
                    if (mcPermissions.getInstance().acrobatics(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.AcrobaticsSkill"), PPt.getSkillLevel(SkillType.ACROBATICS), PPt.getSkillXpLevel(SkillType.ACROBATICS), PPt.getXpToLevel(SkillType.ACROBATICS)));
                    }
                    if (mcPermissions.getInstance().repair(target)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.RepairSkill"), PPt.getSkillLevel(SkillType.REPAIR), PPt.getSkillXpLevel(SkillType.REPAIR), PPt.getXpToLevel(SkillType.REPAIR)));
                    }
                    player.sendMessage(String.valueOf(mcLocale.getString("mcPlayerListener.PowerLevel")) + ChatColor.GREEN + m.getPowerLevel(target));
                }
            }
            else if (Config.statsEnable && label.equalsIgnoreCase(Config.stats)) {
                player.sendMessage(mcLocale.getString("mcPlayerListener.YourStats"));
                if (mcPermissions.getEnabled()) {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.NoSkillNote"));
                }
                final ChatColor header = ChatColor.GOLD;
                if (Skills.hasGatheringSkills(player)) {
                    player.sendMessage(header + "-=GATHERING SKILLS=-");
                    if (mcPermissions.getInstance().excavation(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.ExcavationSkill"), PP.getSkillLevel(SkillType.EXCAVATION), PP.getSkillXpLevel(SkillType.EXCAVATION), PP.getXpToLevel(SkillType.EXCAVATION)));
                    }
                    if (mcPermissions.getInstance().herbalism(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.HerbalismSkill"), PP.getSkillLevel(SkillType.HERBALISM), PP.getSkillXpLevel(SkillType.HERBALISM), PP.getXpToLevel(SkillType.HERBALISM)));
                    }
                    if (mcPermissions.getInstance().mining(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.MiningSkill"), PP.getSkillLevel(SkillType.MINING), PP.getSkillXpLevel(SkillType.MINING), PP.getXpToLevel(SkillType.MINING)));
                    }
                    if (mcPermissions.getInstance().woodcutting(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.WoodcuttingSkill"), PP.getSkillLevel(SkillType.WOODCUTTING), PP.getSkillXpLevel(SkillType.WOODCUTTING), PP.getXpToLevel(SkillType.WOODCUTTING)));
                    }
                }
                if (Skills.hasCombatSkills(player)) {
                    player.sendMessage(header + "-=COMBAT SKILLS=-");
                    if (mcPermissions.getInstance().axes(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.AxesSkill"), PP.getSkillLevel(SkillType.AXES), PP.getSkillXpLevel(SkillType.AXES), PP.getXpToLevel(SkillType.AXES)));
                    }
                    if (mcPermissions.getInstance().archery(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.ArcherySkill"), PP.getSkillLevel(SkillType.ARCHERY), PP.getSkillXpLevel(SkillType.ARCHERY), PP.getXpToLevel(SkillType.ARCHERY)));
                    }
                    if (mcPermissions.getInstance().swords(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.SwordsSkill"), PP.getSkillLevel(SkillType.SWORDS), PP.getSkillXpLevel(SkillType.SWORDS), PP.getXpToLevel(SkillType.SWORDS)));
                    }
                    if (mcPermissions.getInstance().taming(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.TamingSkill"), PP.getSkillLevel(SkillType.TAMING), PP.getSkillXpLevel(SkillType.TAMING), PP.getXpToLevel(SkillType.TAMING)));
                    }
                    if (mcPermissions.getInstance().unarmed(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.UnarmedSkill"), PP.getSkillLevel(SkillType.UNARMED), PP.getSkillXpLevel(SkillType.UNARMED), PP.getXpToLevel(SkillType.UNARMED)));
                    }
                }
                if (Skills.hasMiscSkills(player)) {
                    player.sendMessage(header + "-=MISC SKILLS=-");
                    if (mcPermissions.getInstance().acrobatics(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.AcrobaticsSkill"), PP.getSkillLevel(SkillType.ACROBATICS), PP.getSkillXpLevel(SkillType.ACROBATICS), PP.getXpToLevel(SkillType.ACROBATICS)));
                    }
                    if (mcPermissions.getInstance().repair(player)) {
                        player.sendMessage(Skills.getSkillStats(mcLocale.getString("mcPlayerListener.RepairSkill"), PP.getSkillLevel(SkillType.REPAIR), PP.getSkillXpLevel(SkillType.REPAIR), PP.getXpToLevel(SkillType.REPAIR)));
                    }
                }
                player.sendMessage(String.valueOf(mcLocale.getString("mcPlayerListener.PowerLevel")) + ChatColor.GREEN + m.getPowerLevel(player));
            }
            else if (Config.inviteEnable && label.equalsIgnoreCase(Config.invite) && mcPermissions.getInstance().party(player)) {
                final Party Pinstance = Party.getInstance();
                if (!PP.inParty()) {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.NotInParty"));
                    return true;
                }
                if (split.length < 2) {
                    player.sendMessage(ChatColor.RED + "Usage is /" + Config.invite + " <playername>");
                    return true;
                }
                if (PP.inParty() && split.length >= 2 && isPlayer(split[1])) {
                    if (!Pinstance.canInvite(player, PP)) {
                        player.sendMessage(mcLocale.getString("Party.Locked"));
                        return true;
                    }
                    final Player target2 = getPlayer(split[1]);
                    final PlayerProfile PPt2 = Users.getProfile(target2);
                    PPt2.modifyInvite(PP.getParty());
                    player.sendMessage(mcLocale.getString("mcPlayerListener.InviteSuccess"));
                    target2.sendMessage(mcLocale.getString("mcPlayerListener.ReceivedInvite1", new Object[] { PPt2.getInvite(), player.getName() }));
                    target2.sendMessage(mcLocale.getString("mcPlayerListener.ReceivedInvite2", new Object[] { Config.accept }));
                }
            }
            else if (Config.acceptEnable && label.equalsIgnoreCase(Config.accept) && mcPermissions.getInstance().party(player)) {
                if (PP.hasPartyInvite()) {
                    final Party Pinstance = Party.getInstance();
                    if (PP.inParty()) {
                        Pinstance.removeFromParty(player, PP);
                    }
                    PP.acceptInvite();
                    Pinstance.addToParty(player, PP, PP.getParty(), true);
                }
                else {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.NoInvites"));
                }
            }
            else if (Config.partyEnable && label.equalsIgnoreCase(Config.party)) {
                if (!mcPermissions.getInstance().party(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                final Party Pinstance = Party.getInstance();
                if (PP.inParty() && (!Pinstance.isParty(PP.getParty()) || !Pinstance.isInParty(player, PP))) {
                    Pinstance.addToParty(player, PP, PP.getParty(), false);
                }
                if (args.length == 0 && !PP.inParty()) {
                    player.sendMessage(mcLocale.getString("Party.Help1", new Object[] { Config.party }));
                    player.sendMessage(mcLocale.getString("Party.Help2", new Object[] { Config.party }));
                    player.sendMessage(mcLocale.getString("Party.Help3", new Object[] { Config.party }));
                    return true;
                }
                if (args.length == 0 && PP.inParty()) {
                    String tempList = "";
                    int x = 0;
                    Player[] onlinePlayers6;
                    for (int length10 = (onlinePlayers6 = Bukkit.getServer().getOnlinePlayers()).length, n17 = 0; n17 < length10; ++n17) {
                        final Player p2 = onlinePlayers6[n17];
                        if (PP.getParty().equals(Users.getProfile(p2).getParty())) {
                            if (p2 != null && x + 1 >= Pinstance.partyCount(player, Bukkit.getServer().getOnlinePlayers())) {
                                if (Pinstance.isPartyLeader(p2, PP.getParty())) {
                                    tempList = String.valueOf(tempList) + ChatColor.GOLD + p2.getName();
                                    ++x;
                                }
                                else {
                                    tempList = String.valueOf(tempList) + ChatColor.WHITE + p2.getName();
                                    ++x;
                                }
                            }
                            if (p2 != null && x < Pinstance.partyCount(player, Bukkit.getServer().getOnlinePlayers())) {
                                if (Pinstance.isPartyLeader(p2, PP.getParty())) {
                                    tempList = String.valueOf(tempList) + ChatColor.GOLD + p2.getName() + ", ";
                                    ++x;
                                }
                                else {
                                    tempList = String.valueOf(tempList) + ChatColor.WHITE + p2.getName() + ", ";
                                    ++x;
                                }
                            }
                        }
                    }
                    player.sendMessage(mcLocale.getString("mcPlayerListener.YouAreInParty", new Object[] { PP.getParty() }));
                    player.sendMessage(String.valueOf(mcLocale.getString("mcPlayerListener.PartyMembers")) + " (" + tempList + ChatColor.GREEN + ")");
                    return true;
                }
                if (args.length == 1) {
                    if (args[0].equals("q") && PP.inParty()) {
                        Pinstance.removeFromParty(player, PP);
                        player.sendMessage(mcLocale.getString("mcPlayerListener.LeftParty"));
                        return true;
                    }
                    if (args[0].equalsIgnoreCase("?")) {
                        player.sendMessage(mcLocale.getString("Party.Help4", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help2", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help5", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help6", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help7", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help8", new Object[] { Config.party }));
                        player.sendMessage(mcLocale.getString("Party.Help9", new Object[] { Config.party }));
                    }
                    else if (args[0].equalsIgnoreCase("lock")) {
                        if (PP.inParty()) {
                            if (Pinstance.isPartyLeader(player, PP.getParty())) {
                                Pinstance.lockParty(PP.getParty());
                                player.sendMessage(mcLocale.getString("Party.Locked"));
                            }
                            else {
                                player.sendMessage(mcLocale.getString("Party.NotOwner"));
                            }
                        }
                        else {
                            player.sendMessage(mcLocale.getString("Party.InvalidName"));
                        }
                    }
                    else {
                        if (!args[0].equalsIgnoreCase("unlock")) {
                            if (PP.inParty()) {
                                Pinstance.removeFromParty(player, PP);
                            }
                            Pinstance.addToParty(player, PP, args[0], false);
                            return true;
                        }
                        if (PP.inParty()) {
                            if (Pinstance.isPartyLeader(player, PP.getParty())) {
                                Pinstance.unlockParty(PP.getParty());
                                player.sendMessage(mcLocale.getString("Party.Unlocked"));
                            }
                            else {
                                player.sendMessage(mcLocale.getString("Party.NotOwner"));
                            }
                        }
                        else {
                            player.sendMessage(mcLocale.getString("Party.InvalidName"));
                        }
                    }
                }
                else if (args.length == 2 && PP.inParty()) {
                    if (args[0].equalsIgnoreCase("password")) {
                        if (Pinstance.isPartyLeader(player, PP.getParty())) {
                            if (Pinstance.isPartyLocked(PP.getParty())) {
                                Pinstance.setPartyPassword(PP.getParty(), args[1]);
                                player.sendMessage(mcLocale.getString("Party.PasswordSet", new Object[] { args[1] }));
                            }
                            else {
                                player.sendMessage(mcLocale.getString("Party.IsntLocked"));
                            }
                        }
                        else {
                            player.sendMessage(mcLocale.getString("Party.NotOwner"));
                        }
                    }
                    else if (args[0].equalsIgnoreCase("kick")) {
                        if (Pinstance.isPartyLeader(player, PP.getParty())) {
                            if (Pinstance.isPartyLocked(PP.getParty())) {
                                Player tPlayer = null;
                                if (Bukkit.getServer().getPlayer(args[1]) != null) {
                                    tPlayer = Bukkit.getServer().getPlayer(args[1]);
                                }
                                if (tPlayer == null) {
                                    player.sendMessage(mcLocale.getString("Party.CouldNotKick", new Object[] { args[1] }));
                                }
                                if (!Pinstance.inSameParty(player, tPlayer)) {
                                    player.sendMessage(mcLocale.getString("Party.NotInYourParty", new Object[] { tPlayer.getName() }));
                                }
                                else {
                                    if (!mcPermissions.getInstance().admin(player) && mcPermissions.getInstance().admin(tPlayer)) {
                                        player.sendMessage(mcLocale.getString("Party.CouldNotKick", new Object[] { tPlayer.getName() }));
                                    }
                                    final PlayerProfile tPP = Users.getProfile(tPlayer);
                                    Pinstance.removeFromParty(tPlayer, tPP);
                                    tPlayer.sendMessage(mcLocale.getString("mcPlayerListener.LeftParty"));
                                }
                            }
                            else {
                                player.sendMessage(mcLocale.getString("Party.IsntLocked"));
                            }
                        }
                        else {
                            player.sendMessage(mcLocale.getString("Party.NotOwner"));
                        }
                    }
                    else if (args[0].equalsIgnoreCase("owner")) {
                        if (Pinstance.isPartyLeader(player, PP.getParty())) {
                            Player tPlayer = null;
                            if (Bukkit.getServer().getPlayer(args[1]) != null) {
                                tPlayer = Bukkit.getServer().getPlayer(args[1]);
                            }
                            if (tPlayer == null) {
                                player.sendMessage(mcLocale.getString("Party.CouldNotSetOwner", new Object[] { args[1] }));
                            }
                            if (!Pinstance.inSameParty(player, tPlayer)) {
                                player.sendMessage(mcLocale.getString("Party.CouldNotSetOwner", new Object[] { tPlayer.getName() }));
                            }
                            else {
                                Pinstance.setPartyLeader(PP.getParty(), tPlayer.getName());
                            }
                        }
                        else {
                            player.sendMessage(mcLocale.getString("Party.NotOwner"));
                        }
                    }
                    else {
                        Pinstance.removeFromParty(player, PP);
                        Pinstance.addToParty(player, PP, args[0], false, args[1]);
                    }
                }
                else if (args.length == 2 && !PP.inParty()) {
                    Pinstance.addToParty(player, PP, args[0], false, args[1]);
                }
            }
            else if (Config.partyEnable && label.equalsIgnoreCase("p")) {
                if (!(sender instanceof Player)) {
                    if (args.length < 2) {
                        return true;
                    }
                    String pMessage = args[1];
                    for (int m = 2; m <= args.length - 1; ++m) {
                        pMessage = String.valueOf(pMessage) + " " + args[m];
                    }
                    final String pPrefix = ChatColor.GREEN + "(" + ChatColor.WHITE + "*Console*" + ChatColor.GREEN + ") ";
                    Commands.log.log(Level.INFO, "[P](" + args[0] + ")" + "<*Console*> " + pMessage);
                    Player[] onlinePlayers7;
                    for (int length11 = (onlinePlayers7 = Bukkit.getServer().getOnlinePlayers()).length, n18 = 0; n18 < length11; ++n18) {
                        final Player herp = onlinePlayers7[n18];
                        if (Users.getProfile(herp).inParty() && Users.getProfile(herp).getParty().equalsIgnoreCase(args[0])) {
                            herp.sendMessage(String.valueOf(pPrefix) + pMessage);
                        }
                    }
                    return true;
                }
                else {
                    if (!mcPermissions.getInstance().party(player)) {
                        player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                        return true;
                    }
                    if (args.length >= 1) {
                        String pMessage = args[0];
                        for (int m = 1; m <= args.length - 1; ++m) {
                            pMessage = String.valueOf(pMessage) + " " + args[m];
                        }
                        final String pPrefix = ChatColor.GREEN + "(" + ChatColor.WHITE + player.getDisplayName() + ChatColor.GREEN + ") ";
                        Commands.log.log(Level.INFO, "[P](" + PP.getParty() + ")" + "<" + player.getDisplayName() + "> " + pMessage);
                        Player[] onlinePlayers8;
                        for (int length12 = (onlinePlayers8 = Bukkit.getServer().getOnlinePlayers()).length, n19 = 0; n19 < length12; ++n19) {
                            final Player herp = onlinePlayers8[n19];
                            if (Users.getProfile(herp).inParty() && Party.getInstance().inSameParty(herp, player)) {
                                herp.sendMessage(String.valueOf(pPrefix) + pMessage);
                            }
                        }
                        return true;
                    }
                    if (PP.getAdminChatMode()) {
                        PP.toggleAdminChat();
                    }
                    PP.togglePartyChat();
                    if (PP.getPartyChatMode()) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.PartyChatOn"));
                    }
                    else {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.PartyChatOff"));
                    }
                }
            }
            else if (label.equalsIgnoreCase("a")) {
                if (!(sender instanceof Player) && args.length >= 1) {
                    String aMessage = args[0];
                    for (int m = 1; m <= args.length - 1; ++m) {
                        aMessage = String.valueOf(aMessage) + " " + args[m];
                    }
                    final String aPrefix = ChatColor.AQUA + "{" + ChatColor.WHITE + "*Console*" + ChatColor.AQUA + "} ";
                    Commands.log.log(Level.INFO, "[A]<*Console*> " + aMessage);
                    Player[] onlinePlayers9;
                    for (int length13 = (onlinePlayers9 = Bukkit.getServer().getOnlinePlayers()).length, n20 = 0; n20 < length13; ++n20) {
                        final Player herp = onlinePlayers9[n20];
                        if (mcPermissions.getInstance().adminChat(herp) || herp.isOp()) {
                            herp.sendMessage(String.valueOf(aPrefix) + aMessage);
                        }
                    }
                    return true;
                }
                if (!mcPermissions.getInstance().adminChat(player) && !player.isOp()) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (args.length >= 1) {
                    String aMessage = args[0];
                    for (int m = 1; m <= args.length - 1; ++m) {
                        aMessage = String.valueOf(aMessage) + " " + args[m];
                    }
                    final String aPrefix = ChatColor.AQUA + "{" + ChatColor.WHITE + player.getDisplayName() + ChatColor.AQUA + "} ";
                    Commands.log.log(Level.INFO, "[A]<" + player.getDisplayName() + "> " + aMessage);
                    Player[] onlinePlayers10;
                    for (int length14 = (onlinePlayers10 = Bukkit.getServer().getOnlinePlayers()).length, n21 = 0; n21 < length14; ++n21) {
                        final Player herp = onlinePlayers10[n21];
                        if (mcPermissions.getInstance().adminChat(herp) || herp.isOp()) {
                            herp.sendMessage(String.valueOf(aPrefix) + aMessage);
                        }
                    }
                    return true;
                }
                if (PP.getPartyChatMode()) {
                    PP.togglePartyChat();
                }
                PP.toggleAdminChat();
                if (PP.getAdminChatMode()) {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.AdminChatOn"));
                }
                else {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.AdminChatOff"));
                }
            }
            else if (Config.myspawnEnable && Config.enableMySpawn && label.equalsIgnoreCase(Config.myspawn)) {
                if (!mcPermissions.getInstance().mySpawn(player)) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
                if (System.currentTimeMillis() < PP.getMySpawnATS() * 1000L + 3600000L) {
                    final long x12 = PP.getMySpawnATS() * 1000L + 3600000L - System.currentTimeMillis();
                    final int y2 = (int)(x12 / 60000L);
                    final int z2 = (int)(x12 / 1000L - y2 * 60);
                    player.sendMessage(mcLocale.getString("mcPlayerListener.MyspawnTimeNotice", new Object[] { y2, z2 }));
                    return true;
                }
                PP.setMySpawnATS(System.currentTimeMillis());
                if (PP.getMySpawn(player) != null) {
                    final Location mySpawn = PP.getMySpawn(player);
                    if (mySpawn != null) {
                        player.teleport(mySpawn);
                        player.teleport(mySpawn);
                    }
                }
                else {
                    player.sendMessage(mcLocale.getString("mcPlayerListener.MyspawnNotExist"));
                }
            }
            else if (Config.spoutEnabled && Config.xpbar && Config.xplockEnable && label.equalsIgnoreCase(Config.xplock)) {
                if (split.length >= 2 && Skills.isSkill(split[1]) && mcPermissions.permission(player, "nfcmmo.skills." + Skills.getSkillType(split[1]).toString().toLowerCase())) {
                    if (PP.getXpBarLocked()) {
                        PP.setSkillLock(Skills.getSkillType(split[1]));
                        player.sendMessage(mcLocale.getString("Commands.xplock.locked", new Object[] { m.getCapitalized(PP.getSkillLock().toString()) }));
                    }
                    else {
                        PP.setSkillLock(Skills.getSkillType(split[1]));
                        PP.toggleXpBarLocked();
                        player.sendMessage(mcLocale.getString("Commands.xplock.locked", new Object[] { m.getCapitalized(PP.getSkillLock().toString()) }));
                    }
                    SpoutStuff.updateXpBar(player);
                }
                else if (split.length < 2) {
                    if (PP.getXpBarLocked()) {
                        PP.toggleXpBarLocked();
                        player.sendMessage(mcLocale.getString("Commands.xplock.unlocked"));
                    }
                    else if (PP.getLastGained() != null) {
                        PP.toggleXpBarLocked();
                        PP.setSkillLock(PP.getLastGained());
                        player.sendMessage(mcLocale.getString("Commands.xplock.locked", new Object[] { m.getCapitalized(PP.getSkillLock().toString()) }));
                    }
                }
                else if (split.length >= 2 && !Skills.isSkill(split[1])) {
                    player.sendMessage("Commands.xplock.invalid");
                }
                else if (split.length >= 2 && Skills.isSkill(split[1]) && !mcPermissions.permission(player, "nfcmmo.skills." + Skills.getSkillType(split[1]).toString().toLowerCase())) {
                    player.sendMessage(ChatColor.YELLOW + "[nfcMMO] " + ChatColor.DARK_RED + mcLocale.getString("mcPlayerListener.NoPermission"));
                    return true;
                }
            }
            else if (Config.spoutEnabled && label.equalsIgnoreCase("mchud") && split.length >= 2) {
                HUDType[] values;
                for (int length15 = (values = HUDType.values()).length, n22 = 0; n22 < length15; ++n22) {
                    final HUDType x13 = values[n22];
                    if (x13.toString().toLowerCase().equals(split[1].toLowerCase()) && SpoutStuff.playerHUDs.containsKey(player)) {
                        SpoutStuff.playerHUDs.get(player).resetHUD();
                        SpoutStuff.playerHUDs.remove(player);
                        PP.setHUDType(x13);
                        SpoutStuff.playerHUDs.put(player, new HUDmmo(player));
                    }
                }
            }
        }
        return true;
    }
}
