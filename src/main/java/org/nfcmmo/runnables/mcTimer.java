// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.runnables;

import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.entity.Player;
import org.nfcmmo.skills.Swords;
import org.nfcmmo.skills.Skills;
import org.nfcmmo.Users;
import org.nfcmmo.nfcMMO;

public class mcTimer implements Runnable
{
    private final nfcMMO plugin;
    int thecount;
    
    public mcTimer(final nfcMMO plugin) {
        this.thecount = 1;
        this.plugin = plugin;
    }
    
    @Override
    public void run() {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = this.plugin.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player player = onlinePlayers[i];
            if (player != null) {
                final PlayerProfile PP = Users.getProfile(player);
                if (PP != null) {
                    Skills.monitorSkills(player);
                    Skills.watchCooldowns(player);
                    if (this.thecount % 2 == 0 && PP.getBleedTicks() >= 1) {
                        player.damage(2);
                        PP.decreaseBleedTicks();
                    }
                    if (this.thecount % 2 == 0) {
                        Swords.bleedSimulate(this.plugin);
                    }
                    ++this.thecount;
                    if (this.thecount >= 81) {
                        this.thecount = 1;
                    }
                }
            }
        }
    }
}
