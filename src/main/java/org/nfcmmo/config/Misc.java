// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.config;

import org.nfcmmo.nfcMMO;
import org.bukkit.entity.LivingEntity;
import java.util.HashMap;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;

import java.util.ArrayList;
import java.util.logging.Logger;

public class Misc
{
    String location;
    protected static final Logger log;
    public ArrayList<Entity> mobSpawnerList;
    public ArrayList<Block> blockWatchList;
    public ArrayList<Block> treeFeller;
    public HashMap<Entity, Integer> arrowTracker;
    public ArrayList<LivingEntity> bleedTracker;
    nfcMMO plugin;
    public LivingEntity[] bleedQue;
    public int bleedQuePos;
    public LivingEntity[] bleedRemovalQue;
    public int bleedRemovalQuePos;
    
    static {
        log = Logger.getLogger("Minecraft");
    }
    
    public Misc(final nfcMMO nfcMMO) {
        this.location = "nfcmmo.properties";
        this.mobSpawnerList = new ArrayList<Entity>();
        this.blockWatchList = new ArrayList<Block>();
        this.treeFeller = new ArrayList<Block>();
        this.arrowTracker = new HashMap<Entity, Integer>();
        this.bleedTracker = new ArrayList<LivingEntity>();
        this.plugin = null;
        this.bleedQue = new LivingEntity[20];
        this.bleedQuePos = 0;
        this.bleedRemovalQue = new LivingEntity[20];
        this.bleedRemovalQuePos = 0;
        this.plugin = nfcMMO;
    }
    
    public void addToBleedQue(final LivingEntity entity) {
        this.bleedQue[this.bleedQuePos] = entity;
        ++this.bleedQuePos;
        if (this.bleedQuePos >= this.bleedQue.length) {
            final LivingEntity[] temp = new LivingEntity[this.bleedQue.length * 2];
            System.arraycopy(this.bleedQue, 0, temp, 0, this.bleedQue.length);
            this.bleedQue = temp;
        }
    }
    
    public void addToBleedRemovalQue(final LivingEntity entity) {
        this.bleedRemovalQue[this.bleedRemovalQuePos] = entity;
        ++this.bleedRemovalQuePos;
        if (this.bleedRemovalQuePos >= this.bleedRemovalQue.length) {
            final LivingEntity[] temp = new LivingEntity[this.bleedRemovalQue.length * 2];
            System.arraycopy(this.bleedRemovalQue, 0, temp, 0, this.bleedRemovalQue.length);
            this.bleedRemovalQue = temp;
        }
    }
}
