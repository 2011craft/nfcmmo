// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.locale.mcLocale;
import org.bukkit.Material;
import org.nfcmmo.skills.Skills;
import org.bukkit.inventory.ItemStack;
import org.nfcmmo.config.Config;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Player;

public class Item
{
    public static void itemchecks(final Player player, final Plugin plugin) {
        final ItemStack inhand = player.getItemInHand();
        if (Config.chimaeraWingEnable && inhand.getTypeId() == Config.chimaeraId) {
            chimaerawing(player, plugin);
        }
    }
    
    public static void chimaerawing(final Player player, final Plugin plugin) {
        final PlayerProfile PP = Users.getProfile(player);
        final ItemStack is = player.getItemInHand();
        final Block block = player.getLocation().getBlock();
        if (mcPermissions.getInstance().chimaeraWing(player) && is.getTypeId() == Config.chimaeraId) {
            if (Skills.cooldownOver(player, PP.getRecentlyHurt(), 60) && is.getAmount() >= Config.feathersConsumedByChimaeraWing) {
                final Block derp = player.getLocation().getBlock();
                int y = derp.getY();
                final ItemStack[] inventory = player.getInventory().getContents();
                while (true) {
                    ItemStack[] array;
                    for (int length = (array = inventory).length, i = 0; i < length; ++i) {
                        final ItemStack x = array[i];
                        if (x != null && x.getTypeId() == Config.chimaeraId) {
                            if (x.getAmount() >= Config.feathersConsumedByChimaeraWing + 1) {
                                x.setAmount(x.getAmount() - Config.feathersConsumedByChimaeraWing);
                                player.getInventory().setContents(inventory);
                                player.updateInventory();
                            }
                            else {
                                x.setAmount(0);
                                x.setTypeId(0);
                                player.getInventory().setContents(inventory);
                                player.updateInventory();
                            }
                            while (y < 127) {
                                ++y;
                                if (player != null && player.getLocation().getWorld().getBlockAt(block.getX(), y, block.getZ()).getType() != Material.AIR) {
                                    player.sendMessage(mcLocale.getString("Item.ChimaeraWingFail"));
                                    player.teleport(player.getLocation().getWorld().getBlockAt(block.getX(), y - 1, block.getZ()).getLocation());
                                    return;
                                }
                            }
                            if (PP.getMySpawn(player) != null) {
                                final Location mySpawn = PP.getMySpawn(player);
                                if (mySpawn != null) {
                                    player.teleport(mySpawn);
                                    player.teleport(mySpawn);
                                }
                            }
                            else {
                                player.teleport(player.getWorld().getSpawnLocation());
                            }
                            player.sendMessage(mcLocale.getString("Item.ChimaeraWingPass"));
                            return;
                        }
                    }
                    continue;
                }
            }
            if (!Skills.cooldownOver(player, PP.getRecentlyHurt(), 60) && is.getAmount() >= 10) {
                player.sendMessage(mcLocale.getString("Item.InjuredWait", new Object[] { Skills.calculateTimeLeft(player, PP.getRecentlyHurt(), 60) }));
            }
            else if (is.getTypeId() == Config.chimaeraId && is.getAmount() <= 9) {
                player.sendMessage(mcLocale.getString("Item.NeedFeathers"));
            }
        }
    }
}
