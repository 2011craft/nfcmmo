// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.bukkit.event.block.BlockFromToEvent;
import org.nfcmmo.skills.Unarmed;
import org.bukkit.event.block.BlockDamageEvent;
import org.nfcmmo.skills.Excavation;
import org.bukkit.Statistic;
import org.bukkit.inventory.ItemStack;
import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.sound.SoundEffect;
import org.nfcmmo.skills.Skills;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.skills.WoodCutting;
import org.nfcmmo.skills.Mining;
import org.nfcmmo.skills.Herbalism;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.datatypes.FakeBlockBreakEvent;
import org.bukkit.event.block.BlockBreakEvent;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.nfcmmo.locale.mcLocale;
import org.bukkit.Material;
import org.getspout.spoutapi.SpoutManager;
import org.nfcmmo.Users;
import org.nfcmmo.config.Config;
import org.nfcmmo.m;
import org.bukkit.event.block.BlockPlaceEvent;
import org.nfcmmo.nfcMMO;
import org.bukkit.event.block.BlockListener;

public class mcBlockListener extends BlockListener
{
    private final nfcMMO plugin;
    
    public mcBlockListener(final nfcMMO plugin) {
        this.plugin = plugin;
    }
    
    public void onBlockPlace(final BlockPlaceEvent event) {
        final Player player = event.getPlayer();
        Block block;
        if (event.getBlockReplacedState() != null && event.getBlockReplacedState().getTypeId() == 78) {
            block = event.getBlockAgainst();
        }
        else {
            block = event.getBlock();
        }
        if (m.shouldBeWatched(block)) {
            if (block.getTypeId() != 17 && block.getTypeId() != 39 && block.getTypeId() != 40 && block.getTypeId() != 91 && block.getTypeId() != 86) {
                block.setData((byte)5);
            }
            else if (block.getTypeId() == 17 || block.getTypeId() == 39 || block.getTypeId() == 40 || block.getTypeId() == 91 || block.getTypeId() == 86) {
                this.plugin.misc.blockWatchList.add(block);
            }
        }
        if (block.getTypeId() == 42 && Config.anvilmessages) {
            final PlayerProfile PP = Users.getProfile(player);
            if (Config.spoutEnabled) {
                final SpoutPlayer sPlayer = SpoutManager.getPlayer(player);
                if (sPlayer.isSpoutCraftEnabled()) {
                    if (!PP.getPlacedAnvil()) {
                        sPlayer.sendNotification("[nfcMMO] Anvil Placed", "Right click to repair!", Material.IRON_BLOCK);
                        PP.togglePlacedAnvil();
                    }
                }
                else if (!PP.getPlacedAnvil()) {
                    event.getPlayer().sendMessage(mcLocale.getString("mcBlockListener.PlacedAnvil"));
                    PP.togglePlacedAnvil();
                }
            }
            else if (!PP.getPlacedAnvil()) {
                event.getPlayer().sendMessage(mcLocale.getString("mcBlockListener.PlacedAnvil"));
                PP.togglePlacedAnvil();
            }
        }
    }
    
    public void onBlockBreak(final BlockBreakEvent event) {
        final Player player = event.getPlayer();
        final PlayerProfile PP = Users.getProfile(player);
        final Block block = event.getBlock();
        final ItemStack inhand = player.getItemInHand();
        if (event.isCancelled()) {
            return;
        }
        if (event instanceof FakeBlockBreakEvent) {
            return;
        }
        if (PP.getHoePreparationMode() && mcPermissions.getInstance().herbalismAbility(player) && block.getTypeId() == 59 && block.getData() == 7) {
            Herbalism.greenTerraCheck(player, block);
        }
        if (PP.getGreenTerraMode() && Herbalism.canBeGreenTerra(block)) {
            Herbalism.herbalismProcCheck(block, player, event, this.plugin);
            Herbalism.greenTerraWheat(player, block, event, this.plugin);
        }
        if (mcPermissions.getInstance().mining(player)) {
            if (Config.miningrequirespickaxe) {
                if (m.isMiningPick(inhand)) {
                    Mining.miningBlockCheck(player, block, this.plugin);
                }
            }
            else {
                Mining.miningBlockCheck(player, block, this.plugin);
            }
        }
        if (player != null && block.getTypeId() == 17 && mcPermissions.getInstance().woodcutting(player)) {
            if (Config.woodcuttingrequiresaxe) {
                if (m.isAxes(inhand) && !this.plugin.misc.blockWatchList.contains(block)) {
                    WoodCutting.woodCuttingProcCheck(player, block);
                    if (block.getData() == 0) {
                        PP.addXP(SkillType.WOODCUTTING, Config.mpine);
                    }
                    if (block.getData() == 1) {
                        PP.addXP(SkillType.WOODCUTTING, Config.mspruce);
                    }
                    if (block.getData() == 2) {
                        PP.addXP(SkillType.WOODCUTTING, Config.mbirch);
                    }
                }
            }
            else if (!this.plugin.misc.blockWatchList.contains(block)) {
                WoodCutting.woodCuttingProcCheck(player, block);
                if (block.getData() == 0) {
                    PP.addXP(SkillType.WOODCUTTING, Config.mpine);
                }
                if (block.getData() == 1) {
                    PP.addXP(SkillType.WOODCUTTING, Config.mspruce);
                }
                if (block.getData() == 2) {
                    PP.addXP(SkillType.WOODCUTTING, Config.mbirch);
                }
            }
            Skills.XpCheckSkill(SkillType.WOODCUTTING, player);
            if (mcPermissions.getInstance().woodCuttingAbility(player) && PP.getTreeFellerMode() && block.getTypeId() == 17 && m.blockBreakSimulate(block, player)) {
                if (Config.spoutEnabled) {
                    SpoutStuff.playSoundForPlayer(SoundEffect.EXPLODE, player, block.getLocation());
                }
                WoodCutting.treeFeller(block, player, this.plugin);
                for (final Block blockx : this.plugin.misc.treeFeller) {
                    if (blockx != null) {
                        Material mat = Material.getMaterial(block.getTypeId());
                        byte type = 0;
                        if (block.getTypeId() == 17) {
                            type = block.getData();
                        }
                        ItemStack item = new ItemStack(mat, 1, (short)0, Byte.valueOf(type));
                        if (blockx.getTypeId() == 17) {
                            blockx.getLocation().getWorld().dropItemNaturally(blockx.getLocation(), item);
                            if (!this.plugin.misc.blockWatchList.contains(block)) {
                                WoodCutting.woodCuttingProcCheck(player, blockx);
                                PP.addXP(SkillType.WOODCUTTING, Config.mpine);
                            }
                        }
                        if (blockx.getTypeId() == 18) {
                            mat = Material.SAPLING;
                            item = new ItemStack(mat, 1, (short)0, Byte.valueOf(blockx.getData()));
                            if (Math.random() * 10.0 > 9.0) {
                                blockx.getLocation().getWorld().dropItemNaturally(blockx.getLocation(), item);
                            }
                        }
                        if (blockx.getType() != Material.AIR) {
                            player.incrementStatistic(Statistic.MINE_BLOCK, event.getBlock().getType());
                        }
                        blockx.setType(Material.AIR);
                    }
                }
                if (Config.toolsLoseDurabilityFromAbilities) {
                    m.damageTool(player, (short) Config.abilityDurabilityLoss);
                }
                this.plugin.misc.treeFeller.clear();
            }
        }
        if (Excavation.canBeGigaDrillBroken(block) && mcPermissions.getInstance().excavation(player) && block.getData() != 5) {
            Excavation.excavationProcCheck(block.getData(), block.getType(), block.getLocation(), player);
        }
        if (PP.getHoePreparationMode() && mcPermissions.getInstance().herbalism(player) && Herbalism.canBeGreenTerra(block)) {
            Herbalism.greenTerraCheck(player, block);
        }
        if (mcPermissions.getInstance().herbalism(player) && block.getData() != 5) {
            Herbalism.herbalismProcCheck(block, player, event, this.plugin);
        }
        if (block.getData() == 5 && m.shouldBeWatched(block)) {
            block.setData((byte)0);
            if (this.plugin.misc.blockWatchList.contains(block)) {
                this.plugin.misc.blockWatchList.remove(block);
            }
        }
    }
    
    public void onBlockDamage(final BlockDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }
        final Player player = event.getPlayer();
        final PlayerProfile PP = Users.getProfile(player);
        final ItemStack inhand = player.getItemInHand();
        final Block block = event.getBlock();
        Skills.monitorSkills(player);
        if (PP.getHoePreparationMode() && Herbalism.canBeGreenTerra(block)) {
            Herbalism.greenTerraCheck(player, block);
        }
        if (PP.getAxePreparationMode() && block.getTypeId() == 17) {
            WoodCutting.treeFellerCheck(player, block);
        }
        if (PP.getPickaxePreparationMode() && Mining.canBeSuperBroken(block)) {
            Mining.superBreakerCheck(player, block);
        }
        if (PP.getShovelPreparationMode() && Excavation.canBeGigaDrillBroken(block)) {
            Excavation.gigaDrillBreakerActivationCheck(player, block);
        }
        if (PP.getFistsPreparationMode() && (Excavation.canBeGigaDrillBroken(block) || block.getTypeId() == 78)) {
            Unarmed.berserkActivationCheck(player);
        }
        if (Config.spoutEnabled && block.getTypeId() == 17 && Users.getProfile(player).getTreeFellerMode()) {
            SpoutStuff.playSoundForPlayer(SoundEffect.FIZZ, player, block.getLocation());
        }
        if (PP.getGreenTerraMode() && mcPermissions.getInstance().herbalismAbility(player) && PP.getGreenTerraMode()) {
            Herbalism.greenTerra(player, block);
        }
        if (PP.getGigaDrillBreakerMode() && m.blockBreakSimulate(block, player) && Excavation.canBeGigaDrillBroken(block) && m.isShovel(inhand)) {
            for (int x = 0; x < 3; ++x) {
                if (block.getData() != 5) {
                    Excavation.excavationProcCheck(block.getData(), block.getType(), block.getLocation(), player);
                }
            }
            Material mat = Material.getMaterial(block.getTypeId());
            if (block.getType() == Material.GRASS) {
                mat = Material.DIRT;
            }
            if (block.getType() == Material.CLAY) {
                mat = Material.CLAY_BALL;
            }
            final byte type = block.getData();
            final ItemStack item = new ItemStack(mat, 1, (short)0, Byte.valueOf(type));
            block.setType(Material.AIR);
            player.incrementStatistic(Statistic.MINE_BLOCK, event.getBlock().getType());
            if (Config.toolsLoseDurabilityFromAbilities) {
                m.damageTool(player, (short) Config.abilityDurabilityLoss);
            }
            if (item.getType() == Material.CLAY_BALL) {
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
            }
            else {
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item);
            }
            if (Config.spoutEnabled) {
                SpoutStuff.playSoundForPlayer(SoundEffect.POP, player, block.getLocation());
            }
        }
        if (PP.getBerserkMode() && m.blockBreakSimulate(block, player) && player.getItemInHand().getTypeId() == 0 && (Excavation.canBeGigaDrillBroken(block) || block.getTypeId() == 78)) {
            Material mat2 = Material.getMaterial(block.getTypeId());
            if (block.getTypeId() == 2) {
                mat2 = Material.DIRT;
            }
            if (block.getTypeId() == 78) {
                mat2 = Material.SNOW_BALL;
            }
            if (block.getTypeId() == 82) {
                mat2 = Material.CLAY_BALL;
            }
            final byte type2 = block.getData();
            final ItemStack item2 = new ItemStack(mat2, 1, (short)0, Byte.valueOf(type2));
            player.incrementStatistic(Statistic.MINE_BLOCK, event.getBlock().getType());
            block.setType(Material.AIR);
            if (item2.getType() == Material.CLAY_BALL) {
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item2);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item2);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item2);
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item2);
            }
            else {
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), item2);
            }
            if (Config.spoutEnabled) {
                SpoutStuff.playSoundForPlayer(SoundEffect.POP, player, block.getLocation());
            }
        }
        if (PP.getSuperBreakerMode() && Mining.canBeSuperBroken(block) && m.blockBreakSimulate(block, player)) {
            if (Config.miningrequirespickaxe) {
                if (m.isMiningPick(inhand)) {
                    Mining.SuperBreakerBlockCheck(player, block, this.plugin);
                }
            }
            else {
                Mining.SuperBreakerBlockCheck(player, block, this.plugin);
            }
        }
        if (block.getTypeId() == 18 && mcPermissions.getInstance().woodcutting(player) && PP.getSkillLevel(SkillType.WOODCUTTING) >= 100 && m.isAxes(player.getItemInHand()) && m.blockBreakSimulate(block, player)) {
            m.damageTool(player, (short)1);
            if (Math.random() * 10.0 > 9.0) {
                final ItemStack x2 = new ItemStack(Material.SAPLING, 1, (short)0, Byte.valueOf(block.getData()));
                block.getLocation().getWorld().dropItemNaturally(block.getLocation(), x2);
            }
            block.setType(Material.AIR);
            player.incrementStatistic(Statistic.MINE_BLOCK, event.getBlock().getType());
            if (Config.spoutEnabled) {
                SpoutStuff.playSoundForPlayer(SoundEffect.POP, player, block.getLocation());
            }
        }
        if (block.getType() == Material.AIR && this.plugin.misc.blockWatchList.contains(block)) {
            this.plugin.misc.blockWatchList.remove(block);
        }
    }
    
    public void onBlockFromTo(final BlockFromToEvent event) {
        final Block blockFrom = event.getBlock();
        final Block blockTo = event.getToBlock();
        if (m.shouldBeWatched(blockFrom) && blockFrom.getData() == 5) {
            blockTo.setData((byte)5);
        }
    }
}
