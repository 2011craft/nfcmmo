// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.bukkit.craftbukkit.command.ColouredConsoleSender;
import org.nfcmmo.party.Party;
import org.bukkit.Bukkit;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.block.Block;
import org.bukkit.plugin.Plugin;
import org.nfcmmo.Item;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.skills.Herbalism;
import org.bukkit.Material;
import org.nfcmmo.skills.Skills;
import org.nfcmmo.m;
import org.nfcmmo.skills.Repair;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.ChatColor;
import org.nfcmmo.command.Commands;
import org.nfcmmo.locale.mcLocale;
import org.bukkit.event.player.PlayerJoinEvent;
import org.nfcmmo.spout.mmoHelper;
import org.nfcmmo.spout.SpoutStuff;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.entity.Player;
import org.nfcmmo.mcPermissions;
import org.nfcmmo.config.Config;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.nfcmmo.Users;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.nfcmmo.nfcMMO;
import org.bukkit.Location;
import java.util.logging.Logger;
import org.bukkit.event.player.PlayerListener;

public class mcPlayerListener extends PlayerListener
{
    protected static final Logger log;
    public Location spawn;
    private nfcMMO plugin;
    
    static {
        log = Logger.getLogger("Minecraft");
    }
    
    public mcPlayerListener(final nfcMMO instance) {
        this.spawn = null;
        this.plugin = instance;
    }
    
    public void onPlayerPickupItem(final PlayerPickupItemEvent event) {
        if (Users.getProfile(event.getPlayer()).getBerserkMode()) {
            event.setCancelled(true);
        }
    }
    
    public void onPlayerRespawn(final PlayerRespawnEvent event) {
        final Player player = event.getPlayer();
        final PlayerProfile PP = Users.getProfile(player);
        if (Config.enableMySpawn && mcPermissions.getInstance().mySpawn(player) && player != null && PP != null) {
            PP.setRespawnATS(System.currentTimeMillis());
            final Location mySpawn = PP.getMySpawn(player);
            if (mySpawn != null) {
                event.setRespawnLocation(mySpawn);
            }
        }
    }
    
    public void onPlayerLogin(final PlayerLoginEvent event) {
        Users.addUser(event.getPlayer());
    }
    
    public void onPlayerQuit(final PlayerQuitEvent event) {
        final Player player = event.getPlayer();
        if (Config.spoutEnabled) {
            if (SpoutStuff.playerHUDs.containsKey(player)) {
                SpoutStuff.playerHUDs.remove(player);
            }
            if (mmoHelper.containers.containsKey(player)) {
                mmoHelper.containers.remove(player);
            }
        }
        Users.removeUser(event.getPlayer());
    }
    
    public void onPlayerJoin(final PlayerJoinEvent event) {
        final Player player = event.getPlayer();
        if (mcPermissions.getInstance().motd(player) && Config.enableMotd) {
            player.sendMessage(mcLocale.getString("mcPlayerListener.MOTD", new Object[] { this.plugin.getDescription().getVersion(), Config.nfcmmo }));
        }
        if (Commands.xpevent) {
            player.sendMessage(ChatColor.GOLD + "nfcMMO is currently in an XP rate event! XP rate is " + Config.xpGainMultiplier + "x!");
        }
    }
    
    public void onPlayerInteract(final PlayerInteractEvent event) {
        final Player player = event.getPlayer();
        final PlayerProfile PP = Users.getProfile(player);
        final Action action = event.getAction();
        final Block block = event.getClickedBlock();
        if (action == Action.RIGHT_CLICK_BLOCK) {
            final ItemStack is = player.getItemInHand();
            if (Config.enableMySpawn && block != null && player != null && block.getTypeId() == 26 && mcPermissions.getInstance().setMySpawn(player)) {
                final Location loc = player.getLocation();
                if (mcPermissions.getInstance().setMySpawn(player)) {
                    PP.setMySpawn(loc.getX(), loc.getY(), loc.getZ(), loc.getWorld().getName());
                }
            }
            if (block != null && player != null && mcPermissions.getInstance().repair(player) && event.getClickedBlock().getTypeId() == 42) {
                Repair.repairCheck(player, is, event.getClickedBlock());
            }
            if (m.abilityBlockCheck(block)) {
                if (block != null && m.isHoe(player.getItemInHand()) && block.getTypeId() != 3 && block.getTypeId() != 2 && block.getTypeId() != 60) {
                    Skills.hoeReadinessCheck(player);
                }
                Skills.abilityActivationCheck(player);
            }
            if (block != null && (block.getType() == Material.COBBLESTONE || block.getType() == Material.DIRT) && player.getItemInHand().getType() == Material.SEEDS) {
                boolean pass = false;
                if (Herbalism.hasSeeds(player) && mcPermissions.getInstance().herbalism(player)) {
                    Herbalism.removeSeeds(player);
                    if (Config.enableCobbleToMossy && m.blockBreakSimulate(block, player) && block.getType() == Material.COBBLESTONE && Math.random() * 1500.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.GreenThumb"));
                        block.setType(Material.MOSSY_COBBLESTONE);
                        pass = true;
                    }
                    if (block.getType() == Material.DIRT && m.blockBreakSimulate(block, player) && Math.random() * 1500.0 <= PP.getSkillLevel(SkillType.HERBALISM)) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.GreenThumb"));
                        block.setType(Material.GRASS);
                        pass = true;
                    }
                    if (!pass) {
                        player.sendMessage(mcLocale.getString("mcPlayerListener.GreenThumbFail"));
                    }
                }
                return;
            }
        }
        if (action == Action.RIGHT_CLICK_AIR) {
            Skills.hoeReadinessCheck(player);
            Skills.abilityActivationCheck(player);
        }
        if (action == Action.RIGHT_CLICK_AIR) {
            Item.itemchecks(player, (Plugin)this.plugin);
        }
        if (action == Action.RIGHT_CLICK_BLOCK && m.abilityBlockCheck(event.getClickedBlock())) {
            Item.itemchecks(player, (Plugin)this.plugin);
        }
    }
    
    public void onPlayerChat(final PlayerChatEvent event) {
        final Player player = event.getPlayer();
        final PlayerProfile PP = Users.getProfile(player);
        if (PP.getPartyChatMode()) {
            event.setCancelled(true);
            final String format = ChatColor.GREEN + "(" + ChatColor.WHITE + player.getDisplayName() + ChatColor.GREEN + ") " + event.getMessage();
            Player[] onlinePlayers;
            for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
                final Player x = onlinePlayers[i];
                if (Party.getInstance().inSameParty(player, x)) {
                    x.sendMessage(format);
                }
            }
            if (Bukkit.getServer() instanceof ColouredConsoleSender) {
                final ColouredConsoleSender ccs = (ColouredConsoleSender)Bukkit.getServer();
                ccs.sendMessage(ChatColor.GREEN + "[P]" + format);
            }
        }
        else if (PP.getAdminChatMode()) {
            event.setCancelled(true);
            final String format = ChatColor.AQUA + "{" + ChatColor.WHITE + player.getDisplayName() + ChatColor.AQUA + "} " + event.getMessage();
            Player[] onlinePlayers2;
            for (int length2 = (onlinePlayers2 = Bukkit.getServer().getOnlinePlayers()).length, j = 0; j < length2; ++j) {
                final Player x = onlinePlayers2[j];
                if (x.isOp() || mcPermissions.getInstance().adminChat(x)) {
                    x.sendMessage(format);
                }
            }
            if (Bukkit.getServer() instanceof ColouredConsoleSender) {
                final ColouredConsoleSender ccs = (ColouredConsoleSender)Bukkit.getServer();
                ccs.sendMessage(ChatColor.AQUA + "[A]" + format);
            }
        }
    }
}
