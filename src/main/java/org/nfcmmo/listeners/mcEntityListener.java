// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.bukkit.inventory.ItemStack;
import org.nfcmmo.config.Config;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.nfcmmo.skills.Skills;
import org.bukkit.event.entity.EntityDeathEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.Combat;
import org.nfcmmo.party.Party;
import org.nfcmmo.skills.Acrobatics;
import org.nfcmmo.locale.mcLocale;
import org.nfcmmo.datatypes.SkillType;
import org.bukkit.plugin.Plugin;
import org.bukkit.entity.Entity;
import org.nfcmmo.skills.Taming;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.LivingEntity;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.nfcmmo.nfcMMO;
import org.bukkit.event.entity.EntityListener;

public class mcEntityListener extends EntityListener
{
    private final nfcMMO plugin;
    
    public mcEntityListener(final nfcMMO plugin) {
        this.plugin = plugin;
    }
    
    public void onEntityDamage(final EntityDamageEvent event) {
        if (event.isCancelled()) {
            return;
        }
        if (event instanceof EntityDamageByEntityEvent) {
            final EntityDamageByEntityEvent eventb = (EntityDamageByEntityEvent)event;
            if (eventb.getEntity() instanceof Player && eventb.getDamager() instanceof Player && !event.getEntity().getWorld().getPVP()) {
                return;
            }
        }
        if (event.getEntity() instanceof Player) {
            final Player defender = (Player)event.getEntity();
            final PlayerProfile PPd = Users.getProfile(defender);
            if (defender != null && PPd.getGodMode()) {
                event.setCancelled(true);
            }
            if (PPd == null) {
                Users.addUser(defender);
            }
        }
        if (event.getEntity() instanceof LivingEntity) {
            final LivingEntity entityliving = (LivingEntity)event.getEntity();
            if (entityliving.getNoDamageTicks() < entityliving.getMaximumNoDamageTicks() / 2.0f) {
                final Entity x = event.getEntity();
                final EntityDamageEvent.DamageCause type = event.getCause();
                if (event.getEntity() instanceof Wolf && ((Wolf)event.getEntity()).isTamed() && Taming.getOwner(event.getEntity(), (Plugin)this.plugin) != null) {
                    final Wolf theWolf = (Wolf)event.getEntity();
                    final Player master = Taming.getOwner((Entity)theWolf, (Plugin)this.plugin);
                    final PlayerProfile PPo = Users.getProfile(master);
                    if (master == null || PPo == null) {
                        return;
                    }
                    if ((event.getCause() == EntityDamageEvent.DamageCause.CONTACT || event.getCause() == EntityDamageEvent.DamageCause.LAVA || event.getCause() == EntityDamageEvent.DamageCause.FIRE) && PPo.getSkillLevel(SkillType.TAMING) >= 100 && event.getDamage() < ((Wolf)event.getEntity()).getHealth()) {
                        event.getEntity().teleport(Taming.getOwner((Entity)theWolf, (Plugin)this.plugin).getLocation());
                        master.sendMessage(mcLocale.getString("mcEntityListener.WolfComesBack"));
                        event.getEntity().setFireTicks(0);
                    }
                    if (event.getCause() == EntityDamageEvent.DamageCause.FALL && PPo.getSkillLevel(SkillType.TAMING) >= 100) {
                        event.setCancelled(true);
                    }
                    if (event.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
                        event.getEntity().setFireTicks(0);
                    }
                }
                if (x instanceof Player) {
                    final Player player = (Player)x;
                    if (type == EntityDamageEvent.DamageCause.FALL) {
                        Acrobatics.acrobaticsCheck(player, event);
                    }
                }
                if (event instanceof EntityDamageByEntityEvent && !event.isCancelled()) {
                    final EntityDamageByEntityEvent eventb2 = (EntityDamageByEntityEvent)event;
                    final Entity f = eventb2.getDamager();
                    final Entity e = event.getEntity();
                    if (event.getEntity() instanceof Player && f instanceof Player) {
                        final Player defender2 = (Player)e;
                        final Player attacker = (Player)f;
                        if (Party.getInstance().inSameParty(defender2, attacker)) {
                            event.setCancelled(true);
                        }
                    }
                    Combat.combatChecks(event, this.plugin);
                }
                if (event.getEntity() instanceof Player) {
                    final Player herpderp = (Player)event.getEntity();
                    if (!event.isCancelled() && event.getDamage() >= 1) {
                        Users.getProfile(herpderp).setRecentlyHurt(System.currentTimeMillis());
                    }
                }
            }
        }
    }
    
    public void onEntityDeath(final EntityDeathEvent event) {
        final Entity x = event.getEntity();
        x.setFireTicks(0);
        if (this.plugin.misc.bleedTracker.contains(x)) {
            this.plugin.misc.addToBleedRemovalQue((LivingEntity)x);
        }
        Skills.arrowRetrievalCheck(x, this.plugin);
        if (x instanceof Player) {
            final Player player = (Player)x;
            Users.getProfile(player).setBleedTicks(0);
        }
    }
    
    public void onCreatureSpawn(final CreatureSpawnEvent event) {
        final CreatureSpawnEvent.SpawnReason reason = event.getSpawnReason();
        if (reason == CreatureSpawnEvent.SpawnReason.SPAWNER && !Config.xpGainsMobSpawners) {
            this.plugin.misc.mobSpawnerList.add(event.getEntity());
        }
    }
    
    public boolean isBow(final ItemStack is) {
        return is.getTypeId() == 261;
    }
    
    public boolean isPlayer(final Entity entity) {
        return entity instanceof Player;
    }
}
