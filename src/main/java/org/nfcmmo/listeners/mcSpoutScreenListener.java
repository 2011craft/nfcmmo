// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.getspout.spoutapi.event.screen.ScreenCloseEvent;
import org.nfcmmo.datatypes.PlayerProfile;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.datatypes.buttons.ButtonPartyToggle;
import org.nfcmmo.datatypes.buttons.ButtonEscape;
import org.nfcmmo.datatypes.popups.PopupMMO;
import org.nfcmmo.datatypes.HUDType;
import org.nfcmmo.datatypes.HUDmmo;
import org.nfcmmo.spout.SpoutStuff;
import org.nfcmmo.datatypes.buttons.ButtonHUDStyle;
import org.bukkit.entity.Player;
import org.nfcmmo.Users;
import org.getspout.spoutapi.event.screen.ButtonClickEvent;
import org.nfcmmo.nfcMMO;
import org.getspout.spoutapi.event.screen.ScreenListener;

public class mcSpoutScreenListener extends ScreenListener
{
    nfcMMO plugin;
    
    public mcSpoutScreenListener(final nfcMMO pluginx) {
        this.plugin = null;
        this.plugin = pluginx;
    }
    
    public void onButtonClick(final ButtonClickEvent event) {
        final SpoutPlayer sPlayer = event.getPlayer();
        final PlayerProfile PP = Users.getProfile((Player)sPlayer);
        if (event.getButton() instanceof ButtonHUDStyle) {
            if (SpoutStuff.playerHUDs.containsKey(sPlayer)) {
                SpoutStuff.playerHUDs.get(sPlayer).resetHUD();
                SpoutStuff.playerHUDs.remove(sPlayer);
                switch (PP.getHUDType()) {
                    case RETRO: {
                        PP.setHUDType(HUDType.STANDARD);
                        break;
                    }
                    case STANDARD: {
                        PP.setHUDType(HUDType.SMALL);
                        break;
                    }
                    case SMALL: {
                        PP.setHUDType(HUDType.DISABLED);
                        break;
                    }
                    case DISABLED: {
                        PP.setHUDType(HUDType.RETRO);
                        break;
                    }
                }
                SpoutStuff.playerHUDs.put((Player)sPlayer, new HUDmmo((Player)sPlayer));
                SpoutStuff.playerScreens.get(sPlayer).updateButtons(PP);
            }
        }
        else if (event.getButton() instanceof ButtonEscape) {
            sPlayer.getMainScreen().closePopup();
        }
        else if (event.getButton() instanceof ButtonPartyToggle) {
            PP.togglePartyHUD();
            final ButtonPartyToggle bpt = (ButtonPartyToggle)event.getButton();
            bpt.updateText(PP);
            SpoutStuff.playerHUDs.get(sPlayer).resetHUD();
            SpoutStuff.playerHUDs.get(sPlayer).initializeHUD((Player)sPlayer);
        }
    }
    
    public void onScreenClose(final ScreenCloseEvent event) {
        if (event.getScreen() instanceof PopupMMO) {
            SpoutStuff.playerScreens.remove(event.getPlayer());
        }
    }
}
