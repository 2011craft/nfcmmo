// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.getspout.spoutapi.player.SpoutPlayer;
import org.getspout.spoutapi.gui.PopupScreen;
import org.nfcmmo.datatypes.popups.PopupMMO;
import org.bukkit.entity.Player;
import org.nfcmmo.Users;
import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.gui.ScreenType;
import org.getspout.spoutapi.event.input.KeyPressedEvent;
import org.nfcmmo.nfcMMO;
import org.getspout.spoutapi.event.input.InputListener;

public class mcSpoutInputListener extends InputListener
{
    nfcMMO plugin;
    
    public mcSpoutInputListener(final nfcMMO pluginx) {
        this.plugin = null;
        this.plugin = pluginx;
    }
    
    public void onKeyPressedEvent(final KeyPressedEvent event) {
        if (!event.getPlayer().isSpoutCraftEnabled() || event.getPlayer().getMainScreen().getActivePopup() != null) {
            return;
        }
        if (event.getScreenType() != ScreenType.GAME_SCREEN) {
            return;
        }
        final SpoutPlayer sPlayer = event.getPlayer();
        if (event.getKey() == SpoutStuff.keypress) {
            if (!SpoutStuff.playerScreens.containsKey(sPlayer)) {
                final PopupMMO mmoPop = new PopupMMO((Player)sPlayer, Users.getProfile((Player)sPlayer), this.plugin);
                SpoutStuff.playerScreens.put(sPlayer, mmoPop);
                sPlayer.getMainScreen().attachPopupScreen((PopupScreen)SpoutStuff.playerScreens.get(sPlayer));
                sPlayer.getMainScreen().setDirty(true);
            }
            else {
                sPlayer.getMainScreen().attachPopupScreen((PopupScreen)SpoutStuff.playerScreens.get(sPlayer));
                sPlayer.getMainScreen().setDirty(true);
            }
        }
    }
}
