// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.listeners;

import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.Users;
import org.bukkit.entity.Player;
import org.nfcmmo.datatypes.HUDmmo;
import org.nfcmmo.spout.SpoutStuff;
import org.getspout.spoutapi.event.spout.SpoutCraftEnableEvent;
import org.nfcmmo.nfcMMO;
import org.getspout.spoutapi.event.spout.SpoutListener;

public class mcSpoutListener extends SpoutListener
{
    nfcMMO plugin;
    
    public mcSpoutListener(final nfcMMO pluginx) {
        this.plugin = null;
        this.plugin = pluginx;
    }
    
    public void onSpoutCraftEnable(final SpoutCraftEnableEvent event) {
        final SpoutPlayer sPlayer = event.getPlayer();
        if (sPlayer.isSpoutCraftEnabled()) {
            SpoutStuff.playerHUDs.put((Player)sPlayer, new HUDmmo((Player)sPlayer));
            Users.getProfile((Player)sPlayer).toggleSpoutEnabled();
        }
    }
}
