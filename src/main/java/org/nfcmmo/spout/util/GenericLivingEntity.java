// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.spout.util;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.nfcmmo.spout.mmoHelper;
import org.bukkit.entity.LivingEntity;
import org.getspout.spoutapi.gui.WidgetAnchor;
import org.getspout.spoutapi.gui.ContainerType;
import org.getspout.spoutapi.gui.GenericLabel;
import org.getspout.spoutapi.gui.RenderPriority;
import org.getspout.spoutapi.gui.GenericGradient;
import org.getspout.spoutapi.gui.Widget;
import org.getspout.spoutapi.gui.Color;
import org.getspout.spoutapi.gui.Gradient;
import org.getspout.spoutapi.gui.Label;
import org.getspout.spoutapi.gui.Container;
import org.getspout.spoutapi.gui.GenericContainer;

public class GenericLivingEntity extends GenericContainer
{
    private Container _space;
    private Label _label;
    private Gradient _health;
    private Gradient _armor;
    private GenericFace _face;
    private int health;
    private int armor;
    private int def_width;
    private int def_height;
    private boolean target;
    String face;
    String label;
    
    public GenericLivingEntity() {
        this.health = 100;
        this.armor = 100;
        this.def_width = 80;
        this.def_height = 14;
        this.target = false;
        this.face = "~";
        this.label = "";
        final Color black = new Color(0.0f, 0.0f, 0.0f, 0.75f);
        this.addChildren(new Widget[] { new GenericContainer(new Widget[] { (Widget)(this._space = (Container)new GenericContainer().setMinWidth(this.def_width / 4).setMaxWidth(this.def_width / 4).setVisible(this.target)), (Widget)new GenericContainer(new Widget[] { new GenericGradient().setTopColor(black).setBottomColor(black).setPriority(RenderPriority.Highest), new GenericContainer(new Widget[] { (Widget)(this._health = (Gradient)new GenericGradient()), (Widget)(this._armor = (Gradient)new GenericGradient()) }).setMargin(1).setPriority(RenderPriority.High), (Widget)new GenericContainer(new Widget[] { (Widget)(this._face = (GenericFace)new GenericFace().setMargin(3, 0, 3, 3)), (Widget)(this._label = (Label)new GenericLabel().setMargin(3)) }).setLayout(ContainerType.HORIZONTAL) }).setLayout(ContainerType.OVERLAY) }).setLayout(ContainerType.HORIZONTAL).setMargin(0, 0, 1, 0).setFixed(true).setWidth(this.def_width).setHeight(this.def_height) }).setAlign(WidgetAnchor.TOP_LEFT).setFixed(true).setWidth(this.def_width).setHeight(this.def_height + 1);
        this.setHealthColor(new Color(1.0f, 0.0f, 0.0f, 0.75f));
        this.setArmorColor(new Color(0.75f, 0.75f, 0.75f, 0.75f));
    }
    
    public GenericLivingEntity setEntity(final String name) {
        return this.setEntity(name, "");
    }
    
    public GenericLivingEntity setEntity(final String name, final String prefix) {
        final Player player = this.getPlugin().getServer().getPlayer(name);
        if (player != null && player.isOnline()) {
            return this.setEntity((LivingEntity)player, prefix);
        }
        this.setHealth(0);
        this.setArmor(0);
        this.setLabel(String.valueOf("".equals(prefix) ? "" : prefix) + mmoHelper.getColor((Player)((this.screen != null) ? this.screen.getPlayer() : null), null) + name);
        this.setFace("~" + name);
        return this;
    }
    
    public GenericLivingEntity setEntity(final LivingEntity entity) {
        return this.setEntity(entity, "");
    }
    
    public GenericLivingEntity setEntity(final LivingEntity entity, final String prefix) {
        if (entity != null && entity instanceof LivingEntity) {
            this.setHealth(mmoHelper.getHealth((Entity)entity));
            this.setArmor(mmoHelper.getArmor((Entity)entity));
            this.setLabel(String.valueOf("".equals(prefix) ? "" : prefix) + mmoHelper.getColor((Player)((this.screen != null) ? this.screen.getPlayer() : null), entity) + mmoHelper.getSimpleName(entity, !this.target));
            this.setFace((entity instanceof Player) ? ((Player)entity).getName() : ("+" + mmoHelper.getSimpleName(entity, false).replaceAll(" ", "")));
        }
        else {
            this.setHealth(0);
            this.setArmor(0);
            this.setLabel("");
            this.setFace("");
        }
        return this;
    }
    
    public GenericLivingEntity setTargets(LivingEntity... targets) {
        final Widget[] widgets = this.getChildren();
        if (targets == null) {
            targets = new LivingEntity[0];
        }
        for (int i = targets.length + 1; i < widgets.length; ++i) {
            this.removeChild(widgets[i]);
        }
        for (int i = 0; i < targets.length; ++i) {
            GenericLivingEntity child;
            if (widgets.length > i + 1) {
                child = (GenericLivingEntity)widgets[i + 1];
            }
            else {
                this.addChild((Widget)(child = new GenericLivingEntity()));
            }
            child.setTarget(true);
            child.setEntity(targets[i]);
        }
        this.setHeight((targets.length + 1) * (this.def_height + 1));
        this.updateLayout();
        return this;
    }
    
    public GenericLivingEntity setTarget(final boolean target) {
        if (this.target != target) {
            this.target = target;
            this._space.setVisible(target);
            this.updateLayout();
        }
        return this;
    }
    
    public GenericLivingEntity setHealth(final int health) {
        if (this.health != health) {
            this.health = health;
            this.updateLayout();
        }
        return this;
    }
    
    public GenericLivingEntity setHealthColor(final Color color) {
        this._health.setTopColor(color).setBottomColor(color);
        return this;
    }
    
    public GenericLivingEntity setArmor(final int armor) {
        if (this.armor != armor) {
            this.armor = armor;
            this.updateLayout();
        }
        return this;
    }
    
    public GenericLivingEntity setArmorColor(final Color color) {
        this._armor.setTopColor(color).setBottomColor(color);
        return this;
    }
    
    public GenericLivingEntity setLabel(final String label) {
        if (!this.label.equals(label)) {
            this.label = label;
            this._label.setText(label).setDirty(true);
            this.updateLayout();
        }
        return this;
    }
    
    public GenericLivingEntity setFace(final String name) {
        if (!this.face.equals(name)) {
            this.face = name;
            this._face.setVisible(!name.isEmpty());
            this._face.setName(name);
            this.updateLayout();
        }
        return this;
    }
    
    public Container updateLayout() {
        super.updateLayout();
        this._armor.setWidth(this._armor.getContainer().getWidth() * this.armor / 100).setDirty(true);
        this._health.setWidth(this._health.getContainer().getWidth() * this.health / 100).setDirty(true);
        return (Container)this;
    }
}
