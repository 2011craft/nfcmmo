// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.spout.util;

import java.util.ArrayList;

public class ArrayListString extends ArrayList<String>
{
    private static final long serialVersionUID = -8111006526598412404L;
    
    public boolean contains(final String o) {
        for (final String e : this) {
            if (o == null) {
                if (e != null) {
                    continue;
                }
            }
            else if (!o.equalsIgnoreCase(e)) {
                continue;
            }
            return true;
        }
        return false;
    }
    
    public int indexOf(final String o) {
        for (int i = 0; i < this.size(); ++i) {
            if (o == null) {
                if (this.get(i) == null) {
                    return i;
                }
            }
            else if (o.equalsIgnoreCase(this.get(i))) {
                return i;
            }
        }
        return -1;
    }
    
    public int lastIndexOf(final String o) {
        for (int i = this.size() - 1; i >= 0; --i) {
            if (o == null) {
                if (this.get(i) == null) {
                    return i;
                }
            }
            else if (o.equalsIgnoreCase(this.get(i))) {
                return i;
            }
        }
        return -1;
    }
    
    public boolean remove(final String o) {
        final int i = this.indexOf(o);
        if (i != -1) {
            this.remove(i);
            return true;
        }
        return false;
    }
    
    public String get(final String index) {
        final int i = this.indexOf(index);
        if (i != -1) {
            return this.get(i);
        }
        return null;
    }
    
    public ArrayListString meFirst(final String name) {
        final ArrayListString copy = new ArrayListString();
        if (this.contains(name)) {
            copy.add(name);
        }
        for (final String next : this) {
            if (!next.equalsIgnoreCase(name)) {
                copy.add(next);
            }
        }
        return copy;
    }
}
