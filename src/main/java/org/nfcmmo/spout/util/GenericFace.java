// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.spout.util;

import org.nfcmmo.config.Config;
import org.getspout.spoutapi.gui.GenericTexture;

public final class GenericFace extends GenericTexture
{
    private static String facePath;
    private static int defaultSize;
    private String name;
    
    static {
        GenericFace.facePath = "http://face.rycochet.net/";
        GenericFace.defaultSize = 8;
    }
    
    public GenericFace() {
        this("", GenericFace.defaultSize);
    }
    
    public GenericFace(final String name) {
        this(name, GenericFace.defaultSize);
    }
    
    public GenericFace(final String name, final int size) {
        if (Config.showFaces) {
            this.setWidth(size).setHeight(size).setFixed(true);
            this.setName(name);
        }
        else {
            this.setVisible(false);
        }
    }
    
    public String getName() {
        return this.name;
    }
    
    public GenericFace setName(final String name) {
        if (Config.showFaces) {
            this.name = ((name == null) ? "" : name);
            super.setUrl(String.valueOf(GenericFace.facePath) + this.name + ".png");
            super.setDirty(true);
        }
        return this;
    }
    
    public GenericFace setSize(final int size) {
        if (Config.showFaces) {
            super.setWidth(size).setHeight(size);
        }
        return this;
    }
}
