// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.spout;

import org.nfcmmo.datatypes.HUDType;
import org.nfcmmo.datatypes.PlayerProfile;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.nfcmmo.Users;
import org.getspout.spoutapi.sound.SoundManager;
import org.bukkit.Location;
import org.getspout.spoutapi.sound.SoundEffect;
import org.getspout.spoutapi.SpoutManager;
import org.getspout.spoutapi.gui.Color;
import org.bukkit.plugin.Plugin;
import org.bukkit.event.Listener;
import org.bukkit.event.Event;
import java.util.ArrayList;
import org.nfcmmo.config.Config;
import org.nfcmmo.m;
import org.nfcmmo.datatypes.SkillType;
import java.io.InputStream;
import java.util.jar.JarEntry;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.io.OutputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.util.jar.JarFile;
import java.io.File;
import org.bukkit.Bukkit;
import org.getspout.spoutapi.keyboard.Keyboard;
import org.nfcmmo.datatypes.popups.PopupMMO;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.datatypes.HUDmmo;
import org.bukkit.entity.Player;
import java.util.HashMap;
import org.nfcmmo.listeners.mcSpoutScreenListener;
import org.nfcmmo.listeners.mcSpoutInputListener;
import org.nfcmmo.listeners.mcSpoutListener;
import org.nfcmmo.nfcMMO;

public class SpoutStuff
{
    static nfcMMO plugin;
    private static final mcSpoutListener spoutListener;
    private static final mcSpoutInputListener spoutInputListener;
    private static final mcSpoutScreenListener spoutScreenListener;
    public static HashMap<Player, HUDmmo> playerHUDs;
    public static HashMap<SpoutPlayer, PopupMMO> playerScreens;
    public static Keyboard keypress;
    
    static {
        SpoutStuff.plugin = (nfcMMO)Bukkit.getServer().getPluginManager().getPlugin("nfcMMO");
        spoutListener = new mcSpoutListener(SpoutStuff.plugin);
        spoutInputListener = new mcSpoutInputListener(SpoutStuff.plugin);
        spoutScreenListener = new mcSpoutScreenListener(SpoutStuff.plugin);
        SpoutStuff.playerHUDs = new HashMap<Player, HUDmmo>();
        SpoutStuff.playerScreens = new HashMap<SpoutPlayer, PopupMMO>();
    }
    
    public static void writeFile(final String theFileName, final String theFilePath) {
        try {
            final File currentFile = new File("plugins/nfcMMO/Resources/" + theFilePath + theFileName);
            final JarFile jar = new JarFile(nfcMMO.nfcmmo);
            final JarEntry entry = jar.getJarEntry("resources/" + theFileName);
            final InputStream is = jar.getInputStream(entry);
            final byte[] buf = new byte[2048];
            final OutputStream os = new BufferedOutputStream(new FileOutputStream(currentFile));
            int nbRead;
            while ((nbRead = is.read(buf)) != -1) {
                os.write(buf, 0, nbRead);
            }
            os.flush();
            os.close();
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (IOException e2) {
            e2.printStackTrace();
        }
    }
    
    public static void extractFiles() {
        new File("plugins/nfcMMO/Resources/").mkdir();
        new File("plugins/nfcMMO/Resources/HUD/").mkdir();
        new File("plugins/nfcMMO/Resources/HUD/Standard/").mkdir();
        new File("plugins/nfcMMO/Resources/HUD/Retro/").mkdir();
        new File("plugins/nfcMMO/Resources/Sound/").mkdir();
        for (int x = 0; x < 255; ++x) {
            final String theFilePath = "HUD/Standard/";
            if (x < 10) {
                final String theFileName = "xpbar_inc00" + x + ".png";
                writeFile(theFileName, theFilePath);
            }
            else if (x < 100) {
                final String theFileName = "xpbar_inc0" + x + ".png";
                writeFile(theFileName, theFilePath);
            }
            else {
                final String theFileName = "xpbar_inc" + x + ".png";
                writeFile(theFileName, theFilePath);
            }
        }
        final String theFilePathA = "HUD/Standard/";
        final String theFilePathB = "HUD/Retro/";
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType y = values[i];
            if (y != SkillType.ALL) {
                final String theFileNameA = String.valueOf(m.getCapitalized(y.toString())) + ".png";
                final String theFileNameB = String.valueOf(m.getCapitalized(y.toString())) + "_r.png";
                writeFile(theFileNameA, theFilePathA);
                writeFile(theFileNameB, theFilePathB);
            }
        }
        writeFile("Icon.png", theFilePathA);
        writeFile("Icon_r.png", theFilePathB);
        final String theSoundFilePath = "Sound/";
        writeFile("repair.wav", theSoundFilePath);
        writeFile("level.wav", theSoundFilePath);
    }
    
    public static void setupSpoutConfigs() {
        final String temp = Config.readString("Spout.Menu.Key", "KEY_M");
        Keyboard[] values;
        for (int length = (values = Keyboard.values()).length, i = 0; i < length; ++i) {
            final Keyboard x = values[i];
            if (x.toString().equalsIgnoreCase(temp)) {
                SpoutStuff.keypress = x;
            }
        }
        if (SpoutStuff.keypress == null) {
            System.out.println("Invalid KEY for Spout.Menu.Key, using KEY_M");
            SpoutStuff.keypress = Keyboard.KEY_M;
        }
    }
    
    public static ArrayList<File> getFiles() {
        final ArrayList<File> files = new ArrayList<File>();
        final String dir = "plugins/nfcMMO/Resources/";
        for (int x = 0; x < 255; ++x) {
            if (x < 10) {
                files.add(new File(String.valueOf(dir) + "HUD/Standard/xpbar_inc00" + x + ".png"));
            }
            else if (x < 100) {
                files.add(new File(String.valueOf(dir) + "HUD/Standard/xpbar_inc0" + x + ".png"));
            }
            else {
                files.add(new File(String.valueOf(dir) + "HUD/Standard/xpbar_inc" + x + ".png"));
            }
        }
        SkillType[] values;
        for (int length = (values = SkillType.values()).length, i = 0; i < length; ++i) {
            final SkillType y = values[i];
            if (y != SkillType.ALL) {
                files.add(new File(String.valueOf(dir) + "HUD/Standard/" + m.getCapitalized(y.toString()) + ".png"));
                files.add(new File(String.valueOf(dir) + "HUD/Retro/" + m.getCapitalized(y.toString()) + "_r.png"));
            }
        }
        files.add(new File(String.valueOf(dir) + "HUD/Standard/Icon.png"));
        files.add(new File(String.valueOf(dir) + "HUD/Retro/Icon_r.png"));
        files.add(new File(String.valueOf(dir) + "Sound/repair.wav"));
        files.add(new File(String.valueOf(dir) + "Sound/level.wav"));
        return files;
    }
    
    public static void registerCustomEvent() {
        Bukkit.getServer().getPluginManager().registerEvent(Event.Type.CUSTOM_EVENT, (Listener)SpoutStuff.spoutListener, Event.Priority.Normal, (Plugin)SpoutStuff.plugin);
        Bukkit.getServer().getPluginManager().registerEvent(Event.Type.CUSTOM_EVENT, (Listener)SpoutStuff.spoutInputListener, Event.Priority.Normal, (Plugin)SpoutStuff.plugin);
        Bukkit.getServer().getPluginManager().registerEvent(Event.Type.CUSTOM_EVENT, (Listener)SpoutStuff.spoutScreenListener, Event.Priority.Normal, (Plugin)SpoutStuff.plugin);
    }
    
    public static Color getRetroColor(final SkillType type) {
        switch (type) {
            case ACROBATICS: {
                return new Color((float) Config.acrobatics_r, (float) Config.acrobatics_g, (float) Config.acrobatics_b, 1.0f);
            }
            case ARCHERY: {
                return new Color((float) Config.archery_r, (float) Config.archery_g, (float) Config.archery_b, 1.0f);
            }
            case AXES: {
                return new Color((float) Config.axes_r, (float) Config.axes_g, (float) Config.axes_b, 1.0f);
            }
            case EXCAVATION: {
                return new Color((float) Config.excavation_r, (float) Config.excavation_g, (float) Config.excavation_b, 1.0f);
            }
            case HERBALISM: {
                return new Color((float) Config.herbalism_r, (float) Config.herbalism_g, (float) Config.herbalism_b, 1.0f);
            }
            case MINING: {
                return new Color((float) Config.mining_r, (float) Config.mining_g, (float) Config.mining_b, 1.0f);
            }
            case REPAIR: {
                return new Color((float) Config.repair_r, (float) Config.repair_g, (float) Config.repair_b, 1.0f);
            }
            case SWORDS: {
                return new Color((float) Config.swords_r, (float) Config.swords_g, (float) Config.swords_b, 1.0f);
            }
            case TAMING: {
                return new Color((float) Config.taming_r, (float) Config.taming_g, (float) Config.taming_b, 1.0f);
            }
            case UNARMED: {
                return new Color((float) Config.unarmed_r, (float) Config.unarmed_g, (float) Config.unarmed_b, 1.0f);
            }
            case WOODCUTTING: {
                return new Color((float) Config.woodcutting_r, (float) Config.woodcutting_g, (float) Config.woodcutting_b, 1.0f);
            }
            default: {
                return new Color(0.3f, 0.3f, 0.75f, 1.0f);
            }
        }
    }
    
    public static SpoutPlayer getSpoutPlayer(final String playerName) {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player x = onlinePlayers[i];
            if (x.getName().equalsIgnoreCase(playerName)) {
                return SpoutManager.getPlayer(x);
            }
        }
        return null;
    }
    
    public static void playSoundForPlayer(final SoundEffect effect, final Player player, final Location location) {
        final SoundManager SM = SpoutManager.getSoundManager();
        final SpoutPlayer sPlayer = SpoutManager.getPlayer(player);
        SM.playSoundEffect(sPlayer, effect, location);
    }
    
    public static void playRepairNoise(final Player player) {
        final SoundManager SM = SpoutManager.getSoundManager();
        final SpoutPlayer sPlayer = SpoutManager.getPlayer(player);
        SM.playCustomSoundEffect(Bukkit.getServer().getPluginManager().getPlugin("nfcMMO"), sPlayer, "http://nfcmmo.rycochet.net/nfcmmo/Sound/repair.wav", false);
    }
    
    public static void playLevelUpNoise(final Player player) {
        final SoundManager SM = SpoutManager.getSoundManager();
        final SpoutPlayer sPlayer = SpoutManager.getPlayer(player);
        SM.playCustomSoundEffect(Bukkit.getServer().getPluginManager().getPlugin("nfcMMO"), sPlayer, "http://nfcmmo.rycochet.net/nfcmmo/Sound/level.wav", false);
    }
    
    public static void levelUpNotification(final SkillType skillType, final SpoutPlayer sPlayer) {
        final PlayerProfile PP = Users.getProfile((Player)sPlayer);
        Material mat = null;
        switch (skillType) {
            case TAMING: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.PORK;
                        break;
                    }
                    case 2: {
                        mat = Material.PORK;
                        break;
                    }
                    case 3: {
                        mat = Material.GRILLED_PORK;
                        break;
                    }
                    case 4: {
                        mat = Material.GRILLED_PORK;
                        break;
                    }
                    case 5: {
                        mat = Material.BONE;
                        break;
                    }
                }
                break;
            }
            case MINING: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.COAL_ORE;
                        break;
                    }
                    case 2: {
                        mat = Material.IRON_ORE;
                        break;
                    }
                    case 3: {
                        mat = Material.GOLD_ORE;
                        break;
                    }
                    case 4: {
                        mat = Material.LAPIS_ORE;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_ORE;
                        break;
                    }
                }
                break;
            }
            case WOODCUTTING: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.WOOD;
                        break;
                    }
                    case 2: {
                        mat = Material.WOOD;
                        break;
                    }
                    case 3: {
                        mat = Material.WOOD;
                        break;
                    }
                    case 4: {
                        mat = Material.LOG;
                        break;
                    }
                    case 5: {
                        mat = Material.LOG;
                        break;
                    }
                }
                break;
            }
            case REPAIR: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.COBBLESTONE;
                        break;
                    }
                    case 2: {
                        mat = Material.IRON_BLOCK;
                        break;
                    }
                    case 3: {
                        mat = Material.GOLD_BLOCK;
                        break;
                    }
                    case 4: {
                        mat = Material.LAPIS_BLOCK;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_BLOCK;
                        break;
                    }
                }
                break;
            }
            case HERBALISM: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.YELLOW_FLOWER;
                        break;
                    }
                    case 2: {
                        mat = Material.RED_ROSE;
                        break;
                    }
                    case 3: {
                        mat = Material.BROWN_MUSHROOM;
                        break;
                    }
                    case 4: {
                        mat = Material.RED_MUSHROOM;
                        break;
                    }
                    case 5: {
                        mat = Material.PUMPKIN;
                        break;
                    }
                }
                break;
            }
            case ACROBATICS: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.LEATHER_BOOTS;
                        break;
                    }
                    case 2: {
                        mat = Material.CHAINMAIL_BOOTS;
                        break;
                    }
                    case 3: {
                        mat = Material.IRON_BOOTS;
                        break;
                    }
                    case 4: {
                        mat = Material.GOLD_BOOTS;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_BOOTS;
                        break;
                    }
                }
                break;
            }
            case SWORDS: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.WOOD_SWORD;
                        break;
                    }
                    case 2: {
                        mat = Material.STONE_SWORD;
                        break;
                    }
                    case 3: {
                        mat = Material.IRON_SWORD;
                        break;
                    }
                    case 4: {
                        mat = Material.GOLD_SWORD;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_SWORD;
                        break;
                    }
                }
                break;
            }
            case ARCHERY: {
                mat = Material.ARROW;
                break;
            }
            case UNARMED: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.LEATHER_HELMET;
                        break;
                    }
                    case 2: {
                        mat = Material.CHAINMAIL_HELMET;
                        break;
                    }
                    case 3: {
                        mat = Material.IRON_HELMET;
                        break;
                    }
                    case 4: {
                        mat = Material.GOLD_HELMET;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_HELMET;
                        break;
                    }
                }
                break;
            }
            case EXCAVATION: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.WOOD_SPADE;
                        break;
                    }
                    case 2: {
                        mat = Material.STONE_SPADE;
                        break;
                    }
                    case 3: {
                        mat = Material.IRON_SPADE;
                        break;
                    }
                    case 4: {
                        mat = Material.GOLD_SPADE;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_SPADE;
                        break;
                    }
                }
                break;
            }
            case AXES: {
                switch (getNotificationTier(PP.getSkillLevel(skillType))) {
                    case 1: {
                        mat = Material.WOOD_AXE;
                        break;
                    }
                    case 2: {
                        mat = Material.STONE_AXE;
                        break;
                    }
                    case 3: {
                        mat = Material.IRON_AXE;
                        break;
                    }
                    case 4: {
                        mat = Material.GOLD_AXE;
                        break;
                    }
                    case 5: {
                        mat = Material.DIAMOND_AXE;
                        break;
                    }
                }
                break;
            }
            default: {
                mat = Material.WATCH;
                break;
            }
        }
        sPlayer.sendNotification(ChatColor.GREEN + "Level Up!", ChatColor.YELLOW + m.getCapitalized(skillType.toString()) + ChatColor.DARK_AQUA + " (" + ChatColor.GREEN + PP.getSkillLevel(skillType) + ChatColor.DARK_AQUA + ")", mat);
        playLevelUpNoise((Player)sPlayer);
    }
    
    public static Integer getNotificationTier(final Integer level) {
        if (level < 200) {
            return 1;
        }
        if (level >= 200 && level < 400) {
            return 2;
        }
        if (level >= 400 && level < 600) {
            return 3;
        }
        if (level >= 600 && level < 800) {
            return 4;
        }
        return 5;
    }
    
    public static Integer getXpInc(final int skillxp, final int xptolevel, final HUDType hud) {
        if (hud == HUDType.STANDARD) {
            final double percentage = skillxp / (double)xptolevel;
            final double inc = 0.0039370078740157;
            return (int)(percentage / inc);
        }
        if (hud == HUDType.RETRO) {
            final double percentage = skillxp / (double)xptolevel;
            final double inc = 0.0079365079365079;
            return (int)(percentage / inc);
        }
        return 1;
    }
    
    public static void updateXpBar(final Player player) {
        SpoutStuff.playerHUDs.get(player).updateXpBarDisplay(Users.getProfile(player).getHUDType(), player);
    }
    
    public static String getUrlBar(final Integer number) {
        if (number.toString().toCharArray().length == 1) {
            return "xpbar_inc00" + number + ".png";
        }
        if (number.toString().toCharArray().length == 2) {
            return "xpbar_inc0" + number + ".png";
        }
        return "xpbar_inc" + number + ".png";
    }
    
    public static String getUrlIcon(final SkillType skillType) {
        return String.valueOf(m.getCapitalized(skillType.toString())) + ".png";
    }
    
    public static boolean shouldBeFilled(final PlayerProfile PP) {
        return PP.getXpBarInc() < getXpInc(PP.getSkillXpLevel(PP.getLastGained()), PP.getXpToLevel(PP.getLastGained()), HUDType.STANDARD);
    }
}
