// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.spout;

import org.getspout.spoutapi.gui.WidgetAnchor;
import org.bukkit.plugin.Plugin;
import org.getspout.spoutapi.player.SpoutPlayer;
import org.nfcmmo.Users;
import org.getspout.spoutapi.gui.Widget;
import org.nfcmmo.spout.util.GenericLivingEntity;
import org.nfcmmo.party.Party;
import org.getspout.spoutapi.gui.Container;
import org.bukkit.World;
import org.bukkit.Bukkit;
import java.util.ArrayList;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.Wolf;
import org.bukkit.entity.Squid;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Slime;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Giant;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Cow;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.HumanEntity;
import org.nfcmmo.config.Config;
import org.bukkit.inventory.ItemStack;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Flying;
import org.bukkit.entity.WaterMob;
import org.bukkit.entity.Monster;
import org.bukkit.ChatColor;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Entity;
import org.getspout.spoutapi.gui.GenericContainer;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class mmoHelper
{
    public static HashMap<Player, GenericContainer> containers;
    
    static {
        mmoHelper.containers = new HashMap<Player, GenericContainer>();
    }
    
    public static int getHealth(final Entity player) {
        if (player != null && player instanceof LivingEntity) {
            try {
                return Math.min(((LivingEntity)player).getHealth() * 5, 100);
            }
            catch (Exception ex) {}
        }
        return 0;
    }
    
    public static String getColor(final Player player, final LivingEntity target) {
        if (target instanceof Player) {
            if (((Player)target).isOp()) {
                return ChatColor.GOLD.toString();
            }
            return ChatColor.YELLOW.toString();
        }
        else if (target instanceof Monster) {
            if (player != null && player.equals(((Monster)target).getTarget())) {
                return ChatColor.RED.toString();
            }
            return ChatColor.YELLOW.toString();
        }
        else {
            if (target instanceof WaterMob) {
                return ChatColor.GREEN.toString();
            }
            if (target instanceof Flying) {
                return ChatColor.YELLOW.toString();
            }
            if (!(target instanceof Animals)) {
                return ChatColor.GRAY.toString();
            }
            if (player != null && player.equals(((Animals)target).getTarget())) {
                return ChatColor.RED.toString();
            }
            if (!(target instanceof Tameable)) {
                return ChatColor.GRAY.toString();
            }
            final Tameable pet = (Tameable)target;
            if (pet.isTamed()) {
                return ChatColor.GREEN.toString();
            }
            return ChatColor.YELLOW.toString();
        }
    }
    
    public static int getArmor(final Entity player) {
        if (player != null && player instanceof Player) {
            int armor = 0;
            final int[] multi = { 15, 30, 40, 15 };
            final ItemStack[] inv = ((Player)player).getInventory().getArmorContents();
            for (int i = 0; i < inv.length; ++i) {
                final int max = inv[i].getType().getMaxDurability();
                if (max >= 0) {
                    armor += multi[i] * (max - inv[i].getDurability()) / max;
                }
            }
            return armor;
        }
        return 0;
    }
    
    public static String getSimpleName(final LivingEntity target, final boolean showOwner) {
        String name = "";
        if (target instanceof Player) {
            if (Config.showDisplayName) {
                name = String.valueOf(name) + ((Player)target).getName();
            }
            else {
                name = String.valueOf(name) + ((Player)target).getDisplayName();
            }
        }
        else if (target instanceof HumanEntity) {
            name = String.valueOf(name) + ((HumanEntity)target).getName();
        }
        else {
            if (target instanceof Tameable && ((Tameable)target).isTamed()) {
                if (showOwner && ((Tameable)target).getOwner() instanceof Player) {
                    if (Config.showDisplayName) {
                        name = String.valueOf(name) + ((Player)((Tameable)target).getOwner()).getName() + "'s ";
                    }
                    else {
                        name = String.valueOf(name) + ((Player)((Tameable)target).getOwner()).getDisplayName() + "'s ";
                    }
                }
                else {
                    name = String.valueOf(name) + "Pet ";
                }
            }
            if (target instanceof Chicken) {
                name = String.valueOf(name) + "Chicken";
            }
            else if (target instanceof Cow) {
                name = String.valueOf(name) + "Cow";
            }
            else if (target instanceof Creeper) {
                name = String.valueOf(name) + "Creeper";
            }
            else if (target instanceof Ghast) {
                name = String.valueOf(name) + "Ghast";
            }
            else if (target instanceof Giant) {
                name = String.valueOf(name) + "Giant";
            }
            else if (target instanceof Pig) {
                name = String.valueOf(name) + "Pig";
            }
            else if (target instanceof PigZombie) {
                name = String.valueOf(name) + "PigZombie";
            }
            else if (target instanceof Sheep) {
                name = String.valueOf(name) + "Sheep";
            }
            else if (target instanceof Slime) {
                name = String.valueOf(name) + "Slime";
            }
            else if (target instanceof Skeleton) {
                name = String.valueOf(name) + "Skeleton";
            }
            else if (target instanceof Spider) {
                name = String.valueOf(name) + "Spider";
            }
            else if (target instanceof Squid) {
                name = String.valueOf(name) + "Squid";
            }
            else if (target instanceof Wolf) {
                name = String.valueOf(name) + "Wolf";
            }
            else if (target instanceof Zombie) {
                name = String.valueOf(name) + "Zombie";
            }
            else if (target instanceof Monster) {
                name = String.valueOf(name) + "Monster";
            }
            else if (target instanceof Creature) {
                name = String.valueOf(name) + "Creature";
            }
            else {
                name = String.valueOf(name) + "Unknown";
            }
        }
        return name;
    }
    
    public static LivingEntity[] getPets(final HumanEntity player) {
        final ArrayList<LivingEntity> pets = new ArrayList<LivingEntity>();
        if (player != null && (!(player instanceof Player) || ((Player)player).isOnline())) {
            final String name = player.getName();
            for (final World world : Bukkit.getServer().getWorlds()) {
                for (final LivingEntity entity : world.getLivingEntities()) {
                    if (entity instanceof Tameable && ((Tameable)entity).isTamed() && ((Tameable)entity).getOwner() instanceof Player && name.equals(((Player)((Tameable)entity).getOwner()).getName())) {
                        pets.add(entity);
                    }
                }
            }
        }
        final LivingEntity[] list = new LivingEntity[pets.size()];
        pets.toArray(list);
        return list;
    }
    
    public static void update(final Player player) {
        final Container container = (Container)mmoHelper.containers.get(player);
        if (container != null) {
            int index = 0;
            final Widget[] bars = container.getChildren();
            for (final String name : Party.getInstance().getPartyMembersByName(player).meFirst(player.getName())) {
                GenericLivingEntity bar;
                if (index >= bars.length) {
                    container.addChild((Widget)(bar = new GenericLivingEntity()));
                }
                else {
                    bar = (GenericLivingEntity)bars[index];
                }
                bar.setEntity(name, Party.getInstance().isPartyLeader(Bukkit.getServer().getPlayer(name), Users.getProfile(Bukkit.getServer().getPlayer(name)).getParty()) ? (ChatColor.GREEN + "@") : "");
                ++index;
            }
            while (index < bars.length) {
                container.removeChild(bars[index++]);
            }
            container.updateLayout();
        }
    }
    
    public static void initialize(final SpoutPlayer sPlayer, final Plugin plugin) {
        final GenericContainer container = new GenericContainer();
        container.setAlign(WidgetAnchor.TOP_LEFT).setAnchor(WidgetAnchor.TOP_LEFT).setX(3).setY(3).setWidth(427).setHeight(240).setFixed(true);
        mmoHelper.containers.put((Player)sPlayer, container);
        sPlayer.getMainScreen().attachWidget(plugin, (Widget)container);
    }
    
    public static void updateAll() {
        Player[] onlinePlayers;
        for (int length = (onlinePlayers = Bukkit.getServer().getOnlinePlayers()).length, i = 0; i < length; ++i) {
            final Player x = onlinePlayers[i];
            if (Users.getProfile(x).inParty()) {
                update(x);
            }
        }
    }
}
