// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import org.nfcmmo.config.Config;
import java.io.IOException;
import java.io.FileWriter;
import java.io.File;
import org.nfcmmo.datatypes.PlayerStat;
import org.nfcmmo.datatypes.SkillType;
import java.util.logging.Level;
import java.io.BufferedReader;
import java.io.FileReader;
import org.nfcmmo.datatypes.Tree;
import java.util.logging.Logger;

public class Leaderboard
{
    static String location;
    protected static final Logger log;
    
    static {
        Leaderboard.location = "plugins/nfcMMO/FlatFileStuff/nfcmmo.users";
        log = Logger.getLogger("Minecraft");
    }
    
    public static void makeLeaderboards() {
        final Tree Mining = new Tree();
        final Tree WoodCutting = new Tree();
        final Tree Herbalism = new Tree();
        final Tree Excavation = new Tree();
        final Tree Acrobatics = new Tree();
        final Tree Repair = new Tree();
        final Tree Swords = new Tree();
        final Tree Axes = new Tree();
        final Tree Archery = new Tree();
        final Tree Unarmed = new Tree();
        final Tree Taming = new Tree();
        final Tree PowerLevel = new Tree();
        try {
            final FileReader file = new FileReader(Leaderboard.location);
            final BufferedReader in = new BufferedReader(file);
            String line = "";
            while ((line = in.readLine()) != null) {
                final String[] character = line.split(":");
                final String p = character[0];
                int Plvl = 0;
                if (character.length > 1 && m.isInt(character[1])) {
                    Mining.add(p, Integer.valueOf(character[1]));
                    Plvl += Integer.valueOf(character[1]);
                }
                if (character.length > 5 && m.isInt(character[5])) {
                    WoodCutting.add(p, Integer.valueOf(character[5]));
                    Plvl += Integer.valueOf(character[5]);
                }
                if (character.length > 7 && m.isInt(character[7])) {
                    Repair.add(p, Integer.valueOf(character[7]));
                    Plvl += Integer.valueOf(character[7]);
                }
                if (character.length > 8 && m.isInt(character[8])) {
                    Unarmed.add(p, Integer.valueOf(character[8]));
                    Plvl += Integer.valueOf(character[8]);
                }
                if (character.length > 9 && m.isInt(character[9])) {
                    Herbalism.add(p, Integer.valueOf(character[9]));
                    Plvl += Integer.valueOf(character[9]);
                }
                if (character.length > 10 && m.isInt(character[10])) {
                    Excavation.add(p, Integer.valueOf(character[10]));
                    Plvl += Integer.valueOf(character[10]);
                }
                if (character.length > 11 && m.isInt(character[11])) {
                    Archery.add(p, Integer.valueOf(character[11]));
                    Plvl += Integer.valueOf(character[11]);
                }
                if (character.length > 12 && m.isInt(character[12])) {
                    Swords.add(p, Integer.valueOf(character[12]));
                    Plvl += Integer.valueOf(character[12]);
                }
                if (character.length > 13 && m.isInt(character[13])) {
                    Axes.add(p, Integer.valueOf(character[13]));
                    Plvl += Integer.valueOf(character[13]);
                }
                if (character.length > 14 && m.isInt(character[14])) {
                    Acrobatics.add(p, Integer.valueOf(character[14]));
                    Plvl += Integer.valueOf(character[14]);
                }
                if (character.length > 24 && m.isInt(character[24])) {
                    Taming.add(p, Integer.valueOf(character[24]));
                    Plvl += Integer.valueOf(character[24]);
                }
                PowerLevel.add(p, Plvl);
            }
            in.close();
        }
        catch (Exception e) {
            Leaderboard.log.log(Level.SEVERE, "Exception while reading " + Leaderboard.location + " (Are you sure you formatted it correctly?)", e);
        }
        leaderWrite(Mining.inOrder(), SkillType.MINING);
        leaderWrite(WoodCutting.inOrder(), SkillType.WOODCUTTING);
        leaderWrite(Repair.inOrder(), SkillType.REPAIR);
        leaderWrite(Unarmed.inOrder(), SkillType.UNARMED);
        leaderWrite(Herbalism.inOrder(), SkillType.HERBALISM);
        leaderWrite(Excavation.inOrder(), SkillType.EXCAVATION);
        leaderWrite(Archery.inOrder(), SkillType.ARCHERY);
        leaderWrite(Swords.inOrder(), SkillType.SWORDS);
        leaderWrite(Axes.inOrder(), SkillType.AXES);
        leaderWrite(Acrobatics.inOrder(), SkillType.ACROBATICS);
        leaderWrite(Taming.inOrder(), SkillType.TAMING);
        leaderWrite(PowerLevel.inOrder(), SkillType.ALL);
    }
    
    public static void leaderWrite(final PlayerStat[] ps, final SkillType skillType) {
        final String theLocation = "plugins/nfcMMO/FlatFileStuff/Leaderboards/" + skillType + ".nfcmmo";
        final File theDir = new File(theLocation);
        if (!theDir.exists()) {
            FileWriter writer = null;
            try {
                writer = new FileWriter(theLocation);
            }
            catch (Exception e) {
                Leaderboard.log.log(Level.SEVERE, "Exception while creating " + theLocation, e);
                try {
                    if (writer != null) {
                        writer.close();
                    }
                }
                catch (IOException e2) {
                    Leaderboard.log.log(Level.SEVERE, "Exception while closing writer for " + theLocation, e2);
                }
                return;
            }
            finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }
                }
                catch (IOException e2) {
                    Leaderboard.log.log(Level.SEVERE, "Exception while closing writer for " + theLocation, e2);
                }
            }
            try {
                if (writer != null) {
                    writer.close();
                }
            }
            catch (IOException e2) {
                Leaderboard.log.log(Level.SEVERE, "Exception while closing writer for " + theLocation, e2);
            }
        }
        else {
            try {
                final FileReader file = new FileReader(theLocation);
                final BufferedReader in = new BufferedReader(file);
                final StringBuilder writer2 = new StringBuilder();
                for (final PlayerStat p : ps) {
                    if (!p.name.equals("$nfcMMO_DummyInfo")) {
                        if (p.statVal != 0) {
                            writer2.append(String.valueOf(p.name) + ":" + p.statVal);
                            writer2.append("\r\n");
                        }
                    }
                }
                in.close();
                final FileWriter out = new FileWriter(theLocation);
                out.write(writer2.toString());
                out.close();
            }
            catch (Exception e3) {
                Leaderboard.log.log(Level.SEVERE, "Exception while writing to " + theLocation + " (Are you sure you formatted it correctly?)", e3);
            }
        }
    }
    
    public static String[] retrieveInfo(final String skillName, final int pagenumber) {
        final String theLocation = "plugins/nfcMMO/FlatFileStuff/Leaderboards/" + skillName + ".nfcmmo";
        try {
            final FileReader file = new FileReader(theLocation);
            final BufferedReader in = new BufferedReader(file);
            final int destination = (pagenumber - 1) * 10;
            int x = 0;
            int y = 0;
            String line = "";
            final String[] info = new String[10];
            while ((line = in.readLine()) != null && y < 10) {
                if (++x >= destination && y < 10) {
                    info[y] = line.toString();
                    ++y;
                }
            }
            in.close();
            return info;
        }
        catch (Exception e) {
            Leaderboard.log.log(Level.SEVERE, "Exception while reading " + theLocation + " (Are you sure you formatted it correctly?)", e);
            return null;
        }
    }
    
    public static void updateLeaderboard(final PlayerStat ps, final SkillType skillType) {
        if (Config.useMySQL) {
            return;
        }
        final String theLocation = "plugins/nfcMMO/FlatFileStuff/Leaderboards/" + skillType + ".nfcmmo";
        try {
            final FileReader file = new FileReader(theLocation);
            final BufferedReader in = new BufferedReader(file);
            final StringBuilder writer = new StringBuilder();
            String line = "";
            Boolean inserted = false;
            while ((line = in.readLine()) != null) {
                if (Integer.valueOf(line.split(":")[1]) < ps.statVal && !inserted) {
                    writer.append(String.valueOf(ps.name) + ":" + ps.statVal).append("\r\n");
                    inserted = true;
                }
                if (!line.split(":")[0].equalsIgnoreCase(ps.name)) {
                    writer.append(line).append("\r\n");
                }
            }
            if (!inserted) {
                writer.append(String.valueOf(ps.name) + ":" + ps.statVal).append("\r\n");
            }
            in.close();
            final FileWriter out = new FileWriter(theLocation);
            out.write(writer.toString());
            out.close();
        }
        catch (Exception e) {
            Leaderboard.log.log(Level.SEVERE, "Exception while writing to " + theLocation + " (Are you sure you formatted it correctly?)", e);
        }
    }
}
