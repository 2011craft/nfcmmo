// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo.locale;

import org.bukkit.ChatColor;
import java.text.MessageFormat;
import java.util.MissingResourceException;
import java.util.Locale;
import org.nfcmmo.config.Config;

import java.util.ResourceBundle;

public class mcLocale
{
    private static final String BUNDLE_NAME = "org.nfcmmo.local.locale";
    private static ResourceBundle RESOURCE_BUNDLE;
    
    static {
        mcLocale.RESOURCE_BUNDLE = null;
    }
    
    public static String getString(final String key) {
        return getString(key, null);
    }
    
    public static String getString(final String key, final Object[] messageArguments) {
        try {
            if (mcLocale.RESOURCE_BUNDLE == null) {
                final String myLocale = Config.locale.toLowerCase();
                try {
                    mcLocale.RESOURCE_BUNDLE = ResourceBundle.getBundle("org.nfcmmo.locale.locale", new Locale(myLocale));
                }
                catch (MissingResourceException e) {
                    mcLocale.RESOURCE_BUNDLE = ResourceBundle.getBundle("org.nfcmmo.locale.locale", new Locale("en_us"));
                }
            }
            String output = mcLocale.RESOURCE_BUNDLE.getString(key);
            if (messageArguments != null) {
                final MessageFormat formatter = new MessageFormat("");
                formatter.applyPattern(output);
                output = formatter.format(messageArguments);
            }
            output = addColors(output);
            return output;
        }
        catch (MissingResourceException e2) {
            return String.valueOf('!') + key + '!';
        }
    }
    
    private static String addColors(String input) {
        input = input.replaceAll("\\Q[[BLACK]]\\E", ChatColor.BLACK.toString());
        input = input.replaceAll("\\Q[[DARK_BLUE]]\\E", ChatColor.DARK_BLUE.toString());
        input = input.replaceAll("\\Q[[DARK_GREEN]]\\E", ChatColor.DARK_GREEN.toString());
        input = input.replaceAll("\\Q[[DARK_AQUA]]\\E", ChatColor.DARK_AQUA.toString());
        input = input.replaceAll("\\Q[[DARK_RED]]\\E", ChatColor.DARK_RED.toString());
        input = input.replaceAll("\\Q[[DARK_PURPLE]]\\E", ChatColor.DARK_PURPLE.toString());
        input = input.replaceAll("\\Q[[GOLD]]\\E", ChatColor.GOLD.toString());
        input = input.replaceAll("\\Q[[GRAY]]\\E", ChatColor.GRAY.toString());
        input = input.replaceAll("\\Q[[DARK_GRAY]]\\E", ChatColor.DARK_GRAY.toString());
        input = input.replaceAll("\\Q[[BLUE]]\\E", ChatColor.BLUE.toString());
        input = input.replaceAll("\\Q[[GREEN]]\\E", ChatColor.GREEN.toString());
        input = input.replaceAll("\\Q[[AQUA]]\\E", ChatColor.AQUA.toString());
        input = input.replaceAll("\\Q[[RED]]\\E", ChatColor.RED.toString());
        input = input.replaceAll("\\Q[[LIGHT_PURPLE]]\\E", ChatColor.LIGHT_PURPLE.toString());
        input = input.replaceAll("\\Q[[YELLOW]]\\E", ChatColor.YELLOW.toString());
        input = input.replaceAll("\\Q[[WHITE]]\\E", ChatColor.WHITE.toString());
        return input;
    }
}
