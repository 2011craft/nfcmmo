// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import org.bukkit.World;
import org.bukkit.entity.Animals;
import org.nfcmmo.party.Party;
import org.nfcmmo.skills.Archery;
import org.nfcmmo.datatypes.PlayerProfile;
import org.nfcmmo.skills.Acrobatics;
import org.bukkit.entity.Entity;
import org.nfcmmo.skills.Taming;
import org.nfcmmo.locale.mcLocale;
import org.bukkit.entity.Wolf;
import org.nfcmmo.skills.Skills;
import org.bukkit.entity.Ghast;
import org.bukkit.entity.Slime;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Zombie;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Spider;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Monster;
import org.nfcmmo.config.Config;
import org.nfcmmo.skills.Swords;
import org.bukkit.entity.LivingEntity;
import org.nfcmmo.skills.Axes;
import org.bukkit.entity.Arrow;
import org.bukkit.plugin.Plugin;
import org.nfcmmo.datatypes.SkillType;
import org.nfcmmo.skills.Unarmed;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;

public class Combat
{
    public static void combatChecks(final EntityDamageEvent event, final nfcMMO pluginx) {
        if (event.isCancelled() || event.getDamage() == 0) {
            return;
        }
        if (event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent)event).getDamager() instanceof Player) {
            final EntityDamageByEntityEvent eventb = (EntityDamageByEntityEvent)event;
            final Player attacker = (Player)((EntityDamageByEntityEvent)event).getDamager();
            final PlayerProfile PPa = Users.getProfile(attacker);
            if (mcPermissions.getInstance().unarmed(attacker) && attacker.getItemInHand().getTypeId() == 0) {
                Unarmed.unarmedBonus(attacker, eventb);
            }
            if (m.isAxes(attacker.getItemInHand()) && mcPermissions.getInstance().axes(attacker) && Users.getProfile(attacker).getSkillLevel(SkillType.AXES) >= 500) {
                event.setDamage(event.getDamage() + 4);
            }
            combatAbilityChecks(attacker, PPa, (Plugin)pluginx);
            if (!(((EntityDamageByEntityEvent)event).getDamager() instanceof Arrow)) {
                if (mcPermissions.getInstance().axes(attacker)) {
                    Axes.axeCriticalCheck(attacker, eventb, (Plugin)pluginx);
                }
                if (!pluginx.misc.bleedTracker.contains(event.getEntity())) {
                    Swords.bleedCheck(attacker, (LivingEntity)event.getEntity(), pluginx);
                }
                if (event.getEntity() instanceof Player && mcPermissions.getInstance().unarmed(attacker)) {
                    final Player defender = (Player)event.getEntity();
                    Unarmed.disarmProcCheck(attacker, defender);
                }
                if (PPa.getBerserkMode()) {
                    event.setDamage(event.getDamage() + event.getDamage() / 2);
                }
                if (PPa.getSkullSplitterMode() && m.isAxes(attacker.getItemInHand())) {
                    Axes.applyAoeDamage(attacker, eventb, (Plugin)pluginx);
                }
                if (PPa.getSerratedStrikesMode() && m.isSwords(attacker.getItemInHand())) {
                    Swords.applySerratedStrikes(attacker, eventb, pluginx);
                }
                if (event.getEntity() instanceof Player) {
                    final Player defender = (Player)event.getEntity();
                    final PlayerProfile PPd = Users.getProfile(defender);
                    if (attacker != null && defender != null && Config.pvpxp && System.currentTimeMillis() >= PPd.getRespawnATS() * 1000L + 5000L && (PPd.getLastLogin() + 5) * 1000 < System.currentTimeMillis() && defender.getHealth() >= 1) {
                        final int xp = (int)(event.getDamage() * 2 * Config.pvpxprewardmodifier);
                        if (m.isAxes(attacker.getItemInHand()) && mcPermissions.getInstance().axes(attacker)) {
                            PPa.addXP(SkillType.AXES, xp * 10);
                        }
                        if (m.isSwords(attacker.getItemInHand()) && mcPermissions.getInstance().swords(attacker)) {
                            PPa.addXP(SkillType.SWORDS, xp * 10);
                        }
                        if (attacker.getItemInHand().getTypeId() == 0 && mcPermissions.getInstance().unarmed(attacker)) {
                            PPa.addXP(SkillType.UNARMED, xp * 10);
                        }
                    }
                }
                if (event.getEntity() instanceof Monster && !pluginx.misc.mobSpawnerList.contains(event.getEntity())) {
                    int xp2 = 0;
                    if (event.getEntity() instanceof Creeper) {
                        xp2 = event.getDamage() * 4;
                    }
                    if (event.getEntity() instanceof Spider) {
                        xp2 = event.getDamage() * 3;
                    }
                    if (event.getEntity() instanceof Skeleton) {
                        xp2 = event.getDamage() * 2;
                    }
                    if (event.getEntity() instanceof Zombie) {
                        xp2 = event.getDamage() * 2;
                    }
                    if (event.getEntity() instanceof PigZombie) {
                        xp2 = event.getDamage() * 3;
                    }
                    if (event.getEntity() instanceof Slime) {
                        xp2 = event.getDamage() * 3;
                    }
                    if (event.getEntity() instanceof Ghast) {
                        xp2 = event.getDamage() * 3;
                    }
                    if (m.isSwords(attacker.getItemInHand()) && mcPermissions.getInstance().swords(attacker)) {
                        PPa.addXP(SkillType.SWORDS, xp2 * 10);
                    }
                    else if (m.isAxes(attacker.getItemInHand()) && mcPermissions.getInstance().axes(attacker)) {
                        PPa.addXP(SkillType.AXES, xp2 * 10);
                    }
                    else if (attacker.getItemInHand().getTypeId() == 0 && mcPermissions.getInstance().unarmed(attacker)) {
                        PPa.addXP(SkillType.UNARMED, xp2 * 10);
                    }
                }
                Skills.XpCheckAll(attacker);
                if (event.getEntity() instanceof Wolf) {
                    final Wolf theWolf = (Wolf)event.getEntity();
                    if (attacker.getItemInHand().getTypeId() == 352 && mcPermissions.getInstance().taming(attacker)) {
                        event.setCancelled(true);
                        if (theWolf.isTamed()) {
                            attacker.sendMessage(String.valueOf(mcLocale.getString("Combat.BeastLore")) + " " + mcLocale.getString("Combat.BeastLoreOwner", new Object[] { Taming.getOwnerName(theWolf) }) + " " + mcLocale.getString("Combat.BeastLoreHealthWolfTamed", new Object[] { theWolf.getHealth() }));
                        }
                        else {
                            attacker.sendMessage(String.valueOf(mcLocale.getString("Combat.BeastLore")) + " " + mcLocale.getString("Combat.BeastLoreHealthWolf", new Object[] { theWolf.getHealth() }));
                        }
                    }
                }
            }
        }
        if (event instanceof EntityDamageByEntityEvent && ((EntityDamageByEntityEvent)event).getDamager() instanceof Wolf) {
            final EntityDamageByEntityEvent eventb = (EntityDamageByEntityEvent)event;
            final Wolf theWolf2 = (Wolf)eventb.getDamager();
            if (theWolf2.isTamed() && Taming.ownerOnline(theWolf2, (Plugin)pluginx)) {
                if (Taming.getOwner((Entity)theWolf2, (Plugin)pluginx) == null) {
                    return;
                }
                final Player master = Taming.getOwner((Entity)theWolf2, (Plugin)pluginx);
                final PlayerProfile PPo = Users.getProfile(master);
                if (mcPermissions.getInstance().taming(master)) {
                    if (PPo.getSkillLevel(SkillType.TAMING) >= 750) {
                        event.setDamage(event.getDamage() + 2);
                    }
                    if (Math.random() * 1000.0 <= PPo.getSkillLevel(SkillType.TAMING)) {
                        event.setDamage(event.getDamage() * 2);
                        if (event.getEntity() instanceof Player) {
                            final Player target = (Player)event.getEntity();
                            target.sendMessage(mcLocale.getString("Combat.StruckByGore"));
                            Users.getProfile(target).setBleedTicks(2);
                        }
                        else {
                            pluginx.misc.addToBleedQue((LivingEntity)event.getEntity());
                        }
                        master.sendMessage(mcLocale.getString("Combat.Gore"));
                    }
                    if (!event.getEntity().isDead() && !pluginx.misc.mobSpawnerList.contains(event.getEntity())) {
                        int xp3 = 0;
                        if (event.getEntity() instanceof Monster) {
                            if (event.getEntity() instanceof Creeper) {
                                xp3 = event.getDamage() * 6;
                            }
                            if (event.getEntity() instanceof Spider) {
                                xp3 = event.getDamage() * 5;
                            }
                            if (event.getEntity() instanceof Skeleton) {
                                xp3 = event.getDamage() * 3;
                            }
                            if (event.getEntity() instanceof Zombie) {
                                xp3 = event.getDamage() * 3;
                            }
                            if (event.getEntity() instanceof PigZombie) {
                                xp3 = event.getDamage() * 4;
                            }
                            if (event.getEntity() instanceof Slime) {
                                xp3 = event.getDamage() * 4;
                            }
                            if (event.getEntity() instanceof Ghast) {
                                xp3 = event.getDamage() * 4;
                            }
                            Users.getProfile(master).addXP(SkillType.TAMING, xp3 * 10);
                        }
                        if (event.getEntity() instanceof Player) {
                            xp3 = event.getDamage() * 2;
                            Users.getProfile(master).addXP(SkillType.TAMING, xp3 * 10);
                        }
                        Skills.XpCheckSkill(SkillType.TAMING, master);
                    }
                }
            }
        }
        if (event instanceof EntityDamageByEntityEvent && event.getCause() == EntityDamageEvent.DamageCause.PROJECTILE && ((EntityDamageByEntityEvent)event).getDamager() instanceof Arrow) {
            archeryCheck((EntityDamageByEntityEvent)event, pluginx);
        }
        if (event instanceof EntityDamageByEntityEvent && event.getEntity() instanceof Player) {
            Swords.counterAttackChecks((EntityDamageByEntityEvent)event);
            Acrobatics.dodgeChecks((EntityDamageByEntityEvent)event);
        }
        if (event.getEntity() instanceof Wolf) {
            final Wolf theWolf3 = (Wolf)event.getEntity();
            if (theWolf3.isTamed() && Taming.ownerOnline(theWolf3, (Plugin)pluginx)) {
                if (Taming.getOwner((Entity)theWolf3, (Plugin)pluginx) == null) {
                    return;
                }
                final Player master2 = Taming.getOwner((Entity)theWolf3, (Plugin)pluginx);
                final PlayerProfile PPo2 = Users.getProfile(master2);
                if (mcPermissions.getInstance().taming(master2)) {
                    if ((event.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION || event.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION) && PPo2.getSkillLevel(SkillType.TAMING) >= 500) {
                        event.setDamage(2);
                    }
                    if (PPo2.getSkillLevel(SkillType.TAMING) >= 250) {
                        event.setDamage(event.getDamage() / 2);
                    }
                }
            }
        }
    }
    
    public static void combatAbilityChecks(final Player attacker, final PlayerProfile PPa, final Plugin pluginx) {
        if (PPa.getAxePreparationMode()) {
            Axes.skullSplitterCheck(attacker);
        }
        if (PPa.getSwordsPreparationMode()) {
            Swords.serratedStrikesActivationCheck(attacker);
        }
        if (PPa.getFistsPreparationMode()) {
            Unarmed.berserkActivationCheck(attacker);
        }
    }
    
    public static void archeryCheck(final EntityDamageByEntityEvent event, final nfcMMO pluginx) {
        final Arrow arrow = (Arrow)event.getDamager();
        final Entity y = (Entity)arrow.getShooter();
        final Entity x = event.getEntity();
        if (x instanceof Player) {
            final Player defender = (Player)x;
            final PlayerProfile PPd = Users.getProfile(defender);
            if (PPd == null) {
                Users.addUser(defender);
            }
            if (mcPermissions.getInstance().unarmed(defender) && defender.getItemInHand().getTypeId() == 0) {
                if (defender != null && PPd.getSkillLevel(SkillType.UNARMED) >= 1000) {
                    if (Math.random() * 1000.0 <= 500.0) {
                        event.setCancelled(true);
                        defender.sendMessage(mcLocale.getString("Combat.ArrowDeflect"));
                        return;
                    }
                }
                else if (defender != null && Math.random() * 1000.0 <= PPd.getSkillLevel(SkillType.UNARMED) / 2) {
                    event.setCancelled(true);
                    defender.sendMessage(mcLocale.getString("Combat.ArrowDeflect"));
                    return;
                }
            }
        }
        if (y instanceof Player) {
            final Player attacker = (Player)y;
            final PlayerProfile PPa = Users.getProfile(attacker);
            if (mcPermissions.getInstance().archery(attacker)) {
                Archery.trackArrows(pluginx, x, event, attacker);
                Archery.ignitionCheck(x, event, attacker);
                if (!pluginx.misc.mobSpawnerList.contains(x) && x instanceof Monster) {
                    if (x instanceof Creeper) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 4 * 10);
                    }
                    if (x instanceof Spider) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 3 * 10);
                    }
                    if (x instanceof Skeleton) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 2 * 10);
                    }
                    if (x instanceof Zombie) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 2 * 10);
                    }
                    if (x instanceof PigZombie) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 3 * 10);
                    }
                    if (x instanceof Slime) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 3 * 10);
                    }
                    if (x instanceof Ghast) {
                        PPa.addXP(SkillType.ARCHERY, event.getDamage() * 3 * 10);
                    }
                }
                if (x instanceof Player) {
                    final Player defender2 = (Player)x;
                    final PlayerProfile PPd2 = Users.getProfile(defender2);
                    if (PPa.inParty() && PPd2.inParty() && Party.getInstance().inSameParty(defender2, attacker)) {
                        event.setCancelled(true);
                        return;
                    }
                    if (Config.pvpxp && !Party.getInstance().inSameParty(attacker, defender2) && (PPd2.getLastLogin() + 5) * 1000 < System.currentTimeMillis() && !attacker.getName().equals(defender2.getName())) {
                        final int xp = event.getDamage() * 2 * 10;
                        PPa.addXP(SkillType.ARCHERY, xp);
                    }
                    Archery.dazeCheck(defender2, attacker);
                }
            }
            Skills.XpCheckSkill(SkillType.ARCHERY, attacker);
        }
    }
    
    public static void dealDamage(final Entity target, final int dmg) {
        if (target instanceof Player) {
            ((Player)target).damage(dmg);
        }
        if (target instanceof Animals) {
            ((Animals)target).damage(dmg);
        }
        if (target instanceof Monster) {
            ((Monster)target).damage(dmg);
        }
    }
    
    public static boolean pvpAllowed(final EntityDamageByEntityEvent event, final World world) {
        return event.getEntity().getWorld().getPVP();
    }
}
