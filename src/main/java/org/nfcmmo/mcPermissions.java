// 
// Decompiled by Procyon v0.5.36
// 

package org.nfcmmo;

import com.nijiko.permissions.PermissionHandler;
import ru.tehkode.permissions.PermissionManager;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import com.nijikokun.bukkit.Permissions.Permissions;
import ru.tehkode.permissions.bukkit.PermissionsEx;
import java.util.logging.Logger;
import org.bukkit.Server;

public class mcPermissions
{
    private static volatile mcPermissions instance;
    private static PermissionType permissionType;
    private static Object PHandle;
    public static boolean permissionsEnabled;
    
    static {
        mcPermissions.permissionsEnabled = false;
    }
    
    public static void initialize(final Server server) {
        final Logger log = Logger.getLogger("Minecraft");
        if (mcPermissions.permissionsEnabled && mcPermissions.permissionType != PermissionType.PERMISSIONS) {
            return;
        }
        final Plugin PEXtest = server.getPluginManager().getPlugin("PermissionsEx");
        final Plugin test = server.getPluginManager().getPlugin("Permissions");
        if (PEXtest != null) {
            mcPermissions.PHandle = PermissionsEx.getPermissionManager();
            mcPermissions.permissionType = PermissionType.PEX;
            mcPermissions.permissionsEnabled = true;
            log.info("[nfcMMO] PermissionsEx found, using PermissionsEx.");
        }
        else if (test != null) {
            mcPermissions.PHandle = ((Permissions)test).getHandler();
            mcPermissions.permissionType = PermissionType.PERMISSIONS;
            mcPermissions.permissionsEnabled = true;
            log.info("[nfcMMO] Permissions version " + test.getDescription().getVersion() + " found, using Permissions.");
        }
        else {
            mcPermissions.permissionType = PermissionType.BUKKIT;
            mcPermissions.permissionsEnabled = true;
            log.info("[nfcMMO] Using Bukkit Permissions.");
        }
    }
    
    public static boolean getEnabled() {
        return mcPermissions.permissionsEnabled;
    }
    
    public static boolean permission(final Player player, final String permission) {
        if (!mcPermissions.permissionsEnabled) {
            return player.isOp();
        }
        switch (mcPermissions.permissionType) {
            case PEX: {
                return ((PermissionManager)mcPermissions.PHandle).has(player, permission);
            }
            case PERMISSIONS: {
                return ((PermissionHandler)mcPermissions.PHandle).has(player, permission);
            }
            case BUKKIT: {
                return player.hasPermission(permission);
            }
            default: {
                return true;
            }
        }
    }
    
    public boolean admin(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.admin");
    }
    
    public boolean nfcrefresh(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.tools.nfcrefresh");
    }
    
    public boolean mmoedit(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.tools.mmoedit");
    }
    
    public boolean herbalismAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.herbalism");
    }
    
    public boolean excavationAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.excavation");
    }
    
    public boolean unarmedAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.unarmed");
    }
    
    public boolean chimaeraWing(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.item.chimaerawing");
    }
    
    public boolean miningAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.mining");
    }
    
    public boolean axesAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.axes");
    }
    
    public boolean swordsAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.swords");
    }
    
    public boolean woodCuttingAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.ability.woodcutting");
    }
    
    public boolean nfcgod(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.tools.nfcgod");
    }
    
    public boolean regeneration(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.regeneration");
    }
    
    public boolean motd(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.motd");
    }
    
    public boolean mcAbility(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.ability");
    }
    
    public boolean mySpawn(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.myspawn");
    }
    
    public boolean setMySpawn(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.setmyspawn");
    }
    
    public boolean partyChat(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.chat.partychat");
    }
    
    public boolean partyLock(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.chat.partylock");
    }
    
    public boolean partyTeleport(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.ptp");
    }
    
    public boolean whois(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.whois");
    }
    
    public boolean party(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.commands.party");
    }
    
    public boolean adminChat(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.chat.adminchat");
    }
    
    public static mcPermissions getInstance() {
        if (mcPermissions.instance == null) {
            mcPermissions.instance = new mcPermissions();
        }
        return mcPermissions.instance;
    }
    
    public boolean taming(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.taming");
    }
    
    public boolean mining(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.mining");
    }
    
    public boolean woodcutting(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.woodcutting");
    }
    
    public boolean repair(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.repair");
    }
    
    public boolean unarmed(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.unarmed");
    }
    
    public boolean archery(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.archery");
    }
    
    public boolean herbalism(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.herbalism");
    }
    
    public boolean excavation(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.excavation");
    }
    
    public boolean swords(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.swords");
    }
    
    public boolean axes(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.axes");
    }
    
    public boolean acrobatics(final Player player) {
        return !mcPermissions.permissionsEnabled || permission(player, "nfcmmo.skills.acrobatics");
    }
    
    private enum PermissionType
    {
        PEX("PEX", 0), 
        PERMISSIONS("PERMISSIONS", 1), 
        BUKKIT("BUKKIT", 2);
        
        private PermissionType(final String name, final int ordinal) {
        }
    }
}
