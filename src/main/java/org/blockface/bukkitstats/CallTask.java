// 
// Decompiled by Procyon v0.5.36
// 

package org.blockface.bukkitstats;

import java.net.URLConnection;
import java.io.Reader;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import org.bukkit.plugin.Plugin;

class CallTask implements Runnable
{
    private Plugin plugin;
    private int pub;
    
    public CallTask(final Plugin plugin, final Boolean pub) {
        this.pub = 1;
        this.plugin = plugin;
        if (!pub) {
            this.pub = 0;
        }
    }
    
    @Override
    public void run() {
        try {
            if (this.postUrl().contains("Success")) {
                return;
            }
        }
        catch (Exception ex) {}
        System.out.println("Could not call home.");
    }
    
    private String postUrl() throws Exception {
        final String url = String.format("http://usage.blockface.org/update.php?name=%s&build=%s&plugin=%s&port=%s&public=%s", this.plugin.getServer().getName(), this.plugin.getDescription().getVersion(), this.plugin.getDescription().getName(), this.plugin.getServer().getPort(), this.pub);
        final URL oracle = new URL(url);
        final URLConnection yc = oracle.openConnection();
        final BufferedReader in = new BufferedReader(new InputStreamReader(yc.getInputStream()));
        String result = "";
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            result = String.valueOf(result) + inputLine;
        }
        return result;
    }
}
