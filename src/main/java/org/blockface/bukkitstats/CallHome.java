// 
// Decompiled by Procyon v0.5.36
// 

package org.blockface.bukkitstats;

import java.io.IOException;
import java.io.File;
import org.bukkit.plugin.Plugin;
import org.bukkit.util.config.Configuration;

public class CallHome
{
    private static Configuration cfg;
    
    static {
        CallHome.cfg = null;
    }
    
    public static void load(final Plugin plugin) {
        if (CallHome.cfg == null && !verifyConfig()) {
            return;
        }
        if (CallHome.cfg.getBoolean("opt-out", false)) {
            return;
        }
        plugin.getServer().getScheduler().scheduleAsyncRepeatingTask(plugin, (Runnable)new CallTask(plugin, CallHome.cfg.getBoolean("list-server", true)), 0L, 72000L);
        System.out.println(String.valueOf(plugin.getDescription().getName()) + " is keeping usage stats an. To opt-out for whatever bizarre reason, check plugins/stats.");
    }
    
    private static Boolean verifyConfig() {
        Boolean ret = true;
        final File config = new File("plugins/stats/config.yml");
        if (!config.getParentFile().exists()) {
            config.getParentFile().mkdir();
        }
        if (!config.exists()) {
            try {
                config.createNewFile();
                ret = false;
                System.out.println("BukkitStats has initialized for the first time. To opt-out check plugins/stats");
            }
            catch (IOException e) {
                return false;
            }
        }
        (CallHome.cfg = new Configuration(config)).load();
        CallHome.cfg.getBoolean("opt-out", false);
        CallHome.cfg.getBoolean("list-server", true);
        CallHome.cfg.save();
        return ret;
    }
}
